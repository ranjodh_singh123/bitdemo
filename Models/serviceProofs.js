const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const providers = require('./providers');
const bookings = require('./bookings');



const  serviceProofs = new Schema({
    providerId :{type:Schema.ObjectId,ref:providers,sparse:true,default:null},
    orderId:{type:Schema.ObjectId,ref:bookings,sparse:true,default:null},
    image:{type: String, default: ""},
    isDeleted:{type:Boolean,default:false},
    isBlocked:{type:Boolean,default:false},
    createdAt:{type: Number, default:+new Date()},
    updatedAt:{type: Number, default:0},
})
module.exports = mongoose.model('serviceProofs', serviceProofs);
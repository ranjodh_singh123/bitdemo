const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');

const  paymentsOrders = new Schema({
    orderId:{type:Schema.ObjectId,ref:"orders",sparse:true,default:null},
    senderId:{type:Schema.ObjectId,ref:"users",sparse:true,default:null},
    recieverId:{type:Schema.ObjectId,ref:"providers",sparse:true,default:null},
    price :{type:Number,default:0},
    currency :{type:String,default:null},
    isDeleted:{type:Boolean,default:false},
    isBlocked:{type:Boolean,default:false},
    createdAt:{type: Number, default:+new Date()},
    updatedAt:{type: Number, default:0},
})
module.exports = mongoose.model('paymentsOrders', paymentsOrders);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const bookings = require('./bookings');
const providers = require('./providers');
const users = require('./users');

const  userRatings = new Schema({
    userId:{type:Schema.ObjectId,ref:users,sparse:true,default:null},
    orderId : {type :Schema.ObjectId,ref:bookings,sparse:true,default:null},
    providerId:{type:Schema.ObjectId,ref:providers,sparse:true,default:null},
    rating:{type:Number,default:0}, 
    message :{type:String,default:null}, 
    isDeleted:{type:Boolean,default:false},
    createdAt:{type: Number, default:+new Date()},
    updatedAt:{type: Number, default:0},
})
module.exports = mongoose.model('userRatings', userRatings);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const products = require('./products');
const users = require('./users');


const  orderProducts = new Schema({

    user_id : {type : Schema.ObjectId,ref : users,sparse : true,default : null},
    
    isDeleted:{type:Boolean,default:false},
    isBlocked:{type:Boolean,default:false},
    createdAt:{type: Number, default:+new Date()},
    updatedAt:{type: Number, default:0},
})
module.exports = mongoose.model('orderProducts', orderProducts);
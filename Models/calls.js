const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const providers = require('./providers');
const users = require('./users');


const  calls = new Schema({
    provider_id:{type:Schema.ObjectId,ref:providers,sparse:true,default:null},
    user_id:{type:Schema.ObjectId,ref:users,sparse:true,default:null},
    call_time:{type:String,default:null},
    isBlocked:{type:Boolean,default:false},
    createdAt:{type: Number, default:+new Date()},
    updatedAt:{type: Number, default:0},
})
module.exports = mongoose.model('calls', calls);
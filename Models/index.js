

module.exports = {
    admins:require('./admins'),
    providers : require('./providers'),
    users :require('./users'),
    categories :require('./categories'),
    subCategories:require('./subCategories'),
    products:require('./products'),
    bookingDetails :require('./bookingDetails'),
    bookings :require('./bookings'),
    messages:require('./messages'),
    calls:require('./calls'),
    notifications:require('./notifications'),
    paymentsOrders:require('./paymentsOrders'),
    providerDocs :require('./providerDocs'),
    dateTimes : require('./dateTimes'),
    userRatings : require('./userRatings'),
    providerRatings : require('./providerRatings'),
    serviceProofs :require('./serviceProofs'),
    policyTermConditions : require('./policyTermConditions'),
    cards : require("./cards"),
    providerCards : require("./providerCards"),
    stripCredentials : require ("./stripCredentials"),
    
};
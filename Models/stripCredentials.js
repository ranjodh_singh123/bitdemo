const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');

const stripCredentials = new Schema({
    publishableKey:{type:String,default:null},
    secretKey:{type: String, default: null},
    mode:{type: String,enum:["LIVE","TEST"],default: "TEST"},
    createdAt:{type: String, default: +new Date()},
  })

module.exports = mongoose.model('stripCredentials', stripCredentials);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const providers = require('./providers');
const users = require('./users');


const  messages = new Schema({

    userId:{type:Schema.ObjectId,ref:"users",sparse:true,default:null},
    bookingId:{type:Schema.ObjectId,ref:"bookings",sparse:true,default:null},
    message:{type:String,default:null},
    time:{type:String,default:null},
    picture:{type: String, default:""},
    isDelete:{type:Boolean,default:false},
    isBlocked:{type:Boolean,default:false},
    createdAt:{type: Number, default:+new Date()},
    updatedAt:{type: Number, default:0},
})
module.exports = mongoose.model('messages', messages);
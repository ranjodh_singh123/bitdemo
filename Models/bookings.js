const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const products = require('./products');
const providers = require('./providers');
const subCategories = require('./subCategories');
const users = require('./users');


const  bookings = new Schema({

    user_id : {type : Schema.ObjectId,ref : users,sparse : true,default : null},
    providerId : {type : Schema.ObjectId,ref : providers,sparse : true,default : null },
    logs : [{
      status : {type : String ,enum : ["Accepted","Rejected","Reached","Started","Canceled","Completed","Pending"],default : "Pending" },
      date : {type : Number,default : null}
    }],
    logsStatus :{type : String ,default : "Pending"},
    booking_id : {type : String,default : null},
    book : {type : Boolean,default : false},
    status : {type : String,enum : ["Pending","Confirmed","Completed"],default : "Pending"},
    specialInstruction : {type : String,default : null},
    date : {type : Number, default : null},
    time : {type : Number,default : null},
    timeZone : {type : String,default : null},
    address : {type : String,default:null},
    servicelocation : {
        type: { type: String, enum: ['Point'], default: "Point" },
        coordinates: { type: [Number], default: [0, -1] }
    },
    confirmed_payment:{type:Boolean,default:false},  
    paymentStaus:{type:String,enum:["Pending","Paid"],default:"Pending"},
    paymentIntentKey : {type:String,default:null},
    transationId :{type:String,default:null},
    paymentMode:{type:String,enum:["Cash","Online"],default:"Cash"},
    isDeleted:{type:Boolean,default:false},
    isBlocked:{type:Boolean,default:false},
    createdAt:{type: Number, default:+new Date()},
    updatedAt:{type: Number, default:0},
})
bookings.index({ servicelocation: "2dsphere" });
module.exports = mongoose.model('bookings', bookings);
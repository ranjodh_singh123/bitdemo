const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');


const  categories = new Schema({
    
    name:{type: String,default: null},
    icon:{type: String, default: ""},
    isDeleted:{type:Boolean,default:false},
    isBlocked:{type:Boolean,default:false},
    createdAt:{type: Number, default:+new Date()},
    updatedAt:{type: Number, default:0},
})
module.exports = mongoose.model('categories', categories);
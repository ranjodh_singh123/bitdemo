const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const bookings = require('./bookings');
const products = require('./products');



const  bookingDetails = new Schema({

    orderId : {type : Schema.ObjectId,ref : bookings,sparse : true,default : null},
    productId : { type : Schema.ObjectId,ref : products,sparse : true,default : null},
    quentity : { type : Number,default:null},

    createdAt:{type: Number, default:+new Date()},
})
module.exports = mongoose.model('bookingDetails', bookingDetails);
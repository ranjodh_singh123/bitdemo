const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');


const  subCategories = new Schema({
    
    categoryId:{type:Schema.ObjectId,ref:"categories",sparse:true,default:null},
    name:{type:String,default:null},
    isDeleted:{type:Boolean,default:false},
    isBlocked:{type:Boolean,default:false},
    createdAt:{type: Number, default:+new Date()},
    updatedAt:{type: Number, default:0},
})
module.exports = mongoose.model('subCategories', subCategories);
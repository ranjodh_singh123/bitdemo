const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');

const  dateTimes = new Schema({
    date:{type:String,default:" "},
    time :[{ 
        days:{type :String,default:" "},
        startTime:{type:Number,default:" "}, 
        endTime:{type:Number,default:" "},
    }],
    isDeleted:{type:Boolean,default:false},
    createdAt:{type: Number, default:+new Date()},
    updatedAt:{type: Number, default:0},
})
module.exports = mongoose.model('dateTimes', dateTimes);
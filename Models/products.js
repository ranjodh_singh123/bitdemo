const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');


const  products = new Schema({
    categoryId:{type:Schema.ObjectId,ref:"categories",sparse:true,default:null},
    subCategoryId:{type:Schema.ObjectId,ref:"subCategories",sparse:true,default:null},
    picture:{
        original:{type: String, default: ""}, 
        thumbnail:{type: String, default: ""},
         },
    description:{type:String,default:null},
    name:{type:String,default:null},
    price:{type:Number,default:null},
    isDeleted:{type:Boolean,default:false},
    isBlocked:{type:Boolean,default:false},
    createdAt:{type: Number, default:+new Date()},
    updatedAt:{type: Number, default:0},
})
module.exports = mongoose.model('products', products);
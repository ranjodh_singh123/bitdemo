const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const providers = require('./providers');


const  providerDocs = new Schema({
    provider_id:{type:Schema.ObjectId,ref:providers,sparse:true,default:null},
addproof:{
      original:{type: String, default: ""},
      thumbnail:{type: String, default: ""},
      number:{type:String,default:null},
      expireDate:{type:Date,default:null},
},
identicard:{
      original:{type: String, default: ""},
      thumbnail:{type: String, default: ""},
      number:{type:String,default:null},
     expireDate:{type:Date,default:null},
},
DocComplete:{type:Boolean,default:false}, 
isDeleted:{type:Boolean,default:false},
isBlocked:{type:Boolean,default:false},
createdAt:{type: Number, default:+new Date()},
updatedAt:{type: Number, default:0},
})
module.exports = mongoose.model('providerDocs', providerDocs);

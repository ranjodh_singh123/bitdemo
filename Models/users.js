const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');


const  users = new Schema({

    accessToken : { type : String, default : null }, 
    socialKey:{ type:String,default:null}, 
    otp : { type : Number, default : null }, 
    fullName : {type:String,default: null},
    phoneNumber : {type: Number,default: null}, 
    countrycode : { type:String,default:null}, 
    email : { type : String,default:null},
    password : { type: String, default: null},
    location : {
        type: { type : String, enum : ['Point'], default : "Point" }, 
        coordinates : { type : [Number], default : [0, -1] } 
    },
    address : { type : String, default : null },
    profilePicture : {
        original : {type: String, default: ""}, 
        thumbnail : {type: String, default: ""}, 
    },
    isOnline : {type:Boolean,default:false},  
    profileComplete : {type:Boolean,default:false},  
    deviceToken :{type : String, default : null},
    deviceType :{type : String,enum:["WEB","ANDROID", "IPHONE"], default : "ANDROID"},
    otpverify : {type:Boolean,default:false}, 
    notification:{ type: Boolean, default: true },
    isDeleted : {type:Boolean,default:false},
    isBlocked : {type:Boolean,default:false},
    isActive : {type:Boolean,default:false},
    createdAt : {type: Number, default:+new Date()},
    updatedAt : {type: Number, default:0}, 
})

users.index({ location: "2dsphere" }); 
module.exports = mongoose.model('users', users);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');


const  policyTermConditions = new Schema({
    policy:{type:String,default:null},
    termsCondition:{type:String,default:null},
    isDeleted :{type:Boolean,default:false},
    createdAt:{type: Number, default:+new Date()},
    updatedAt:{type: Number, default:0},
})
module.exports = mongoose.model('policyTermConditions', policyTermConditions);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');
const providers = require('./providers');
const users = require('./users');


const  notifications = new Schema({
    senderId:{type:Schema.ObjectId,ref:users,sparse:true,default:null},
    recieverId:{type:Schema.ObjectId,ref:providers,sparse:true,default:null},
    pic :{type:String,default:null},
    title:{type:String,default:null},
    message:{type:String,default:null},
    time:{type:String,default:null},  
    type:{type:String,enum:["Status","Message","Rating"],default:"Status"},
    isRead:{type:Boolean, default:false},
    isDeleted:{type:Boolean,default:false},
    isBlocked:{type:Boolean,default:false},
    createdAt:{type:Number,default:+new Date()},
    updatedAt:{type:Number,default:0},
})
module.exports = mongoose.model('notifications', notifications);
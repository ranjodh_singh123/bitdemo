const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Config = require("../Config");
const UniversalFunctions = require("../Utils/UniversalFunctions");

const cards = new Schema({
  userId: { type: Schema.ObjectId, ref: "users", sparse: true },
  customerId: { type: String, default: "" },
  sourceId: { type: String, default: "" },
  brand: { type: String, default: "" },
  exp_month: { type: String, default: "" },
  exp_year: { type: String, default: "" },
  last4: { type: String, default: "" },
  funding: { type: String, default: "" },
  country :{type:String,default:null},
  type:{ type: String, default: "" },
  default: { type: Boolean, default: false },
  selected:{ type: Boolean, default: false },
  isDeleted: { type: Boolean, default: false },
  timeStamp: { type: String, default: +new Date() }
});

module.exports = mongoose.model("cards", cards);
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Config = require('../Config');
const UniversalFunctions = require('../Utils/UniversalFunctions');


const  providers = new Schema({

    accessToken: {type: String, default: null},
    socialKey:{type:String,default:null},
    otp:{type:Number,default:null},
    fullName:{type:String,default: null},
    phoneNumber:{type: Number,default: null},
    countrycode:{type:String,default:null},
    email:{type:String,default:null},
    password:{type: String, default: null},
    location : {
        type: { type: String, enum: ['Point'], default: "Point" },
        coordinates: { type: [Number], default: [0, -1] }
      },
    address : { type : String, default : null },
    profilePicture:{
        original:{type: String, default: ""},
        thumbnail:{type: String, default: ""}
       },
    isOnline:{type:Boolean,default:false},  
    deviceToken :{type : String, default : null},
    deviceType :{type : String,enum:["WEB","ANDROID", "IPHONE"], default : "ANDROID"},
    profileComplete:{type:Boolean,default:false},  
    documentUploaded : { type : Boolean, default : false }, 
    stripeId : {type : String, default : null},
    otpverify:{type:Boolean,default:false},
    isApproval : {type:Boolean,default:false},
    isActive :{type :Boolean,default:false},
    categoryId : {type: Schema.ObjectId, ref: "categories", sparse: true , default : null },
    isDeleted:{type:Boolean,default:false},
    isBlocked:{type:Boolean,default:false},
    createdAt:{type: Number, default:+new Date()},
    updatedAt:{type: Number, default:0},
})
providers.index({ location: "2dsphere" });
module.exports = mongoose.model('providers', providers);
/**
 * Created by Prince
 */

const Jwt = require('jsonwebtoken'),
    Config = require('../Config'),
    DAO = require('../DAOManager').queries,
    Models = require('../Models/'),
    UniversalFunctions = require('../Utils/UniversalFunctions'),
    _ = require('lodash')
ERROR = Config.responseMessages.ERROR;


var FCM = require('fcm-push');


let nodemailer = require('nodemailer');
let sesTransport = require('nodemailer-ses-transport');

let transporter = nodemailer.createTransport(sesTransport({
    accessKeyId : "AKIAVP2SLA6NS6Y2KTWQ",
    secretAccessKey : "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",
    region : 'us-west-2'
}));



var sendMail = async(data) => {
    let obj ={
        from: "Krowd support@krowd.io", // sender address
        to: data.email, // list of receivers
        subject: data.subject, // Subject line
        html: data.content,
    };

    transporter.sendMail(obj,(err,res)=>{
        console.log('send mail',err,res);
    })
    
    return null
}



var sendNotification = async (deviceToken,data) => {

    var fcm = new FCM(Config.APP_CONSTANTS.SERVER.NOTIFICATION_KEY);

    console.log(".......data......send notifiaction..",data);
    let message = {
        registration_ids : deviceToken,
        notification : {
            title : data.title,
            message : data.msg,
            pushType : data.type,
            body : data.msg,
            sound : "default",
            badge : 0,
        },
        data:data,
        priority : 'high'
    };


    console.log("=========================================Notifications===> ",message );

    fcm.send(message, function (err, result) {
        console.log("push android/ios..", err,result);
    });
    
};

var sendSingleNotification = async (deviceToken,data) => {


    var fcm = new FCM(Config.APP_CONSTANTS.SERVER.NOTIFICATION_KEY);

    console.log(".......data......send notifiaction..",data);


    let message = {
        to: deviceToken,
        notification: {
            title: data.title,
            message : data.message,
            pushType : data.type,
            body : data.message,
            sound : "default",
            badge : 0,
        },
        data:data,
        priority: 'high'
    };
    
    console.log("=========================================Notifications===> ",message );

    fcm.send( message, function (err, result) {
        console.log("push android/ios.************.", err,result);

        return null;
    });
};

module.exports={
    sendNotification:sendNotification,
    sendSingleNotification:sendSingleNotification,
    sendMail:sendMail
};
/**
 * Created by Prince
 */

const Jwt = require('jsonwebtoken'),
    Config = require('../Config'),
    DAO = require('../DAOManager').queries,
    Models = require('../Models/'),
    UniversalFunctions = require('../Utils/UniversalFunctions'),
    _ = require('lodash')
    ERROR = Config.responseMessages.ERROR;

var generateToken = function(tokenData,userType) {
    return new Promise((resolve, reject) => {
        try {
           let secretKey;
            switch(userType){
                case Config.APP_CONSTANTS.SCOPE.ADMIN:
                    secretKey = Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY_ADMIN;
                    break;
                case Config.APP_CONSTANTS.SCOPE.USER:
                    secretKey = Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY_USER;
                    break;
                    case Config.APP_CONSTANTS.SCOPE.PROVIDER:
                    secretKey = Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY_PROVIDER;
                    break;
            }
            

            console.log("........tokenData...........",tokenData);
            let token = Jwt.sign(tokenData, secretKey);
             console.log("=======secretKey==========",token,secretKey)

            return resolve(token);
        } catch (err) {
            return reject(err);
        }
    });
};


var verifyToken = async function verifyToken(tokenData,secretKey) {

    
     // console.log("============tokenData================",tokenData,secretKey.headers.authorization);
        
      var token = secretKey.headers.authorization.substr(7,300);
      console.log("============match with this token  =========",secretKey.headers.authorization )
    
     var user;
         if(tokenData.scope === Config.APP_CONSTANTS.SCOPE.ADMIN){
            user = await DAO.getData(Models.admins,{_id: tokenData._id,accessToken : token },{__v : 0},{lean : true});
                console.log("..+++++++++++++++++++.Admin..******************............");
            if(!(user.length)){
                throw UniversalFunctions.sendError('en', ERROR.UNAUTHORIZED);
            }
         }
         
        else if(tokenData.scope === Config.APP_CONSTANTS.SCOPE.PROVIDER){
            user = await DAO.getData(Models.providers,{_id: tokenData._id,accessToken : token},{__v : 0},{lean : true});
                console.log("..+++++++++++++++++++.Provider..******************............");
            if(!(user.length)){
                throw UniversalFunctions.sendError('en', ERROR.UNAUTHORIZED);
            }
        }

        else if(tokenData.scope === Config.APP_CONSTANTS.SCOPE.USER){
            user = await DAO.getData(Models.users,{_id: tokenData._id,accessToken : token},{__v : 0},{lean : true});
                console.log("..+++++++++++++++++++.users..******************............");
            if(!(user.length)){
                throw UniversalFunctions.sendError('en', ERROR.UNAUTHORIZED);
            }
        }

        // console.log("....user.....",user);
      
        
        if(user.length === 0){
            throw UniversalFunctions.sendError('en', ERROR.UNAUTHORIZED);
        } else{
            user[0].scope = tokenData.scope;
            return {
                isValid: true,
                credentials : user[0]
            }
        }
       
};
/*********************************************************************************************** */


module.exports={
    generateToken:generateToken,
    verifyToken:verifyToken
};
const DAO = require("../DAOManager").queries,
  Config = require("../Config"),
  TokenManager = require("../Libs/tokenManager"),
  NotificationsManager = require("../Libs/NotificationsManager"),
  ERROR = Config.responseMessages.ERROR,
  Models = require("../Models"),
  bcrypt = require("bcryptjs"),
  momentTimeZone = require('moment-timezone'),
  UniversalFunctions = require("../Utils/UniversalFunctions");

  async = require("async"),
  winston = require("winston");
  moment = require('moment');
  request = require('request');
  var path =require('path');  


  var rn = require('random-number');
  var randomGenerator = require('random-generator');
  var crypto = require('crypto');
  var {DateTime} =require('luxon');
  var random = require('random-hex-character-generator')
  let Base64 = require('base-64');
  const { SUCCESS } = require("../Config/responseMessages");
  const commonController = require('./commonController');
  const userSubcategory  =require('../Aggregation/userSubcategory');
  const orderlisting = require('../Aggregation/orderListing');
  const orderSublisting = require('../Aggregation/orderSublisting');
  const trackLocationUser = require('../Aggregation/trackLocationUser');
  const searchProducts = require('../Aggregation/searchProducts');
  const orderstatus = require('../Aggregation/accessOrderUser');
  const AWS = require('aws-sdk'); 

  const OTP = 1234;
  const PASSWORD = "qwerty";
 var stripe = require("stripe")("sk_test_51IeNFEC3cNboZ0zDOVljMUqyJFImT3MtStT4ZPIgF1PnbMVt5jxg1sYRfaHUH6UqxdRclGFwpFAyxPNmHk0aQwRV00955G6PTg");


 const StripeListing = async (payloadData) => {
  try {

      let query = { mode : payloadData.type}
      let projection = { __v: 0 }
      let options = { lean: true }
      let fetch_data = await DAO.getData(Models.stripCredentials, query, projection, options)
      return fetch_data

  }
  catch (err) {
      throw err;
  }
};


const imageUpload = async (payloadData) => {
  try {
     
    let name = "IMAGE" + randomstring.generate(7)+ ".png";
    const s3 = new AWS.S3({
      bucket: Config.awsS3Config.s3BucketCredentials.bucket,
      accessKeyId: Config.awsS3Config.s3BucketCredentials.accessKeyId,
      secretAccessKey: Config.awsS3Config.s3BucketCredentials.secretAccessKey,
      s3URL: Config.awsS3Config.s3BucketCredentials.s3URL,
      folder: {
        profilePicture : "profilePicture",
        thumb : "thumb"
      }
    });

    let image = payloadData.image    
    let params = {
      Bucket: Config.awsS3Config.s3BucketCredentials.bucket, 
      Key : name,
      Body: image,
    };
     console.log(params)
     const data = await s3.upload(params).promise()
     const { Location } = data
     return Location
  
  } catch (err) {
    throw err;
  }
}


 const userRegister = async(payloadData) => {

  try {

        // # check email 
        let email = payloadData.email.toLowerCase()
        let check_email = await commonController.check_user_email(email)

        if(check_email.length) { throw ERROR.EMAIL_ALREADY_EXIST }
        else {

            // ## check mobile number
            let country_code = payloadData.countrycode;
            let phoneNumber = payloadData.phoneNumber;
            let check_phone_no = await commonController.check_user_phone_no(country_code, phoneNumber)

            if(check_phone_no.length) { throw ERROR.PHONENO_ALREADY_EXIST }
            else {
                let setData = {
                    profilePicture : payloadData.profilePicture,
                    fullName : payloadData.fullName,
                    phoneNumber : payloadData.phoneNumber,
                    countrycode : payloadData.countrycode,
                    email : payloadData.email,
                    password : payloadData.password,
                    deviceToken : payloadData.deviceType,
                    deviceType : payloadData.deviceType
                    
                }
                // ### save user data
                let create_data = await commonController.save_user_data(setData)

                // #### generate token
                let token_info = {
                      _id : create_data._id,
                      collection : Models.users,
                      scope : Config.APP_CONSTANTS.SCOPE.USER
                }
                let token_data = await commonController.generate_token(token_info)
                let policy_terms_data = await commonController.policy_terms_condtions();
                
                return {
                    _id : token_data._id,
                    fullName : token_data.fullName,
                    email : token_data.email,
                    countrycode : token_data.countrycode,
                    phoneNumber : token_data.phoneNumber,
                    profilePicture : token_data.profilePicture,
                    accessToken : token_data.accessToken,
                    policy : policy_terms_data[0].policy,
                    termsCondition : policy_terms_data[0].termsCondition
                }

            }

        }

  }
  catch(err) {
      throw err;
  }

}

const addAddress = async(payloadData, userData) => {
  try {

      let query = { _id : userData._id }
       let update = { 
       "location.coordinates" : [ payloadData.lng, payloadData.lat ],
           address : payloadData.address
                    }
      let options = { new : true }
      let data_to_update = await DAO.findAndUpdate(Models.users, query, update, options)
      return data_to_update

     }
    catch(err) {
        throw err;
             }
}

const userLogin = async(payloadData) => {

  try {

        // # check email
        
        let query = { 
            email : payloadData.email.toLowerCase(),
            isDeleted : false 
        }
        let projection = { __v : 0 }
        let options = { lean : true }
        let fetch_data = await DAO.getData(Models.users, query, projection, options)

        if(fetch_data.length == 0) { throw ERROR.EMAIL_NOT_FOUND }
        else {

            // ## check password && check blocked
            if(payloadData.password != fetch_data[0].password) { throw ERROR.WRONG_PASSWORD } 
            else if(fetch_data[0].isBlocked == true) { throw ERROR.BLOCKED }
            else {
              
                  // ### generate new access token 
                  let token_info = {
                        _id : fetch_data[0]._id,
                        collection : Models.users,
                        scope : Config.APP_CONSTANTS.SCOPE.USER,
                  }
                  if(payloadData.deviceId){
                    token_info.deviceToken = payloadData.deviceId
                  }

                  if(payloadData.deviceType){
                    token_info.deviceType = payloadData.deviceType
                  }

                  let token_data = await commonController.generate_token(token_info)
                  let policy_terms_data = await commonController.policy_terms_condtions();               
                  return {
                      _id : token_data._id,
                      fullName : token_data.fullName,
                      email : token_data.email,
                      countrycode : token_data.countrycode,
                      phoneNumber : token_data.phoneNumber,
                      profilePicture : token_data.profilePicture,
                      accessToken : token_data.accessToken,
                      policy : policy_terms_data[0].policy,
                      termsCondition : policy_terms_data[0].termsCondition
                  }     
            }
        }
  }
  catch(err) {
      throw err;
  }

}


 ////////////gmail login
 const verify_email = async(email) => {
  try {

        let query = { email : email, isDeleted : false }
        let projection = { __v : 0 }
        let options = { lean : true }
        let fetch_data = await DAO.getData(Models.users, query, projection, options)
        return fetch_data

  }
  catch(err) {
    throw err;
  }
}

const verify_phone_no = async(countrycode, phoneNumber) => {
try {

    let query = { 
      countrycode : countrycode,
      phoneNumber : phoneNumber,
       isDeleted : false 
    }
    let projection = { __v : 0 }
    let options = { lean : true }
    let fetch_data = await DAO.getData(Models.users, query, projection, options)
    return fetch_data

}
catch(err) {
throw err;
}
}


const Login_gamil = async(payloadData) =>{
 try{
    console.log("check data of user-----",payloadData);
    let query = { socialKey : payloadData.socialKey };
    let projection = { __v : 0 };
    let option = { lean : true };  
    let fetch_data = await DAO.getData(Models.users,query,projection,option);   
    console.log("social key match",fetch_data);
    if(fetch_data.length != 0) { 
        let token_info = {
             _id : fetch_data[0]._id,
             collection : Models.users,
             scope : Config.APP_CONSTANTS.SCOPE.USER
        }

        if(payloadData.deviceId){
            token_info.deviceToken = payloadData.deviceId
        }
          
        if(payloadData.deviceType){
            token_info.deviceType = payloadData.deviceType
        }

        token_data = await commonController.generate_token(token_info)
        //###-------get policy terms and conditions --
        let policy_terms_data = await commonController.policy_terms_condtions();
        return {
             _id : token_data._id,
             fullName : token_data.fullName,
             email : token_data.email,
             countrycode : token_data.countrycode,
             phoneNumber : token_data.phoneNumber,
             profilePicture : token_data.profilePicture,
             accessToken : token_data.accessToken,
             policy : policy_terms_data[0].policy,
             termsCondition : policy_terms_data[0].termsCondition
        }
    }   
    else {

          let countrycode = null, phoneNumber =null, email = null;
          if(payloadData.email) { email = payloadData.email.toLowerCase() }
          if(payloadData.countrycode	) { countrycode	 = payloadData.countrycode	 }
          if(payloadData.phoneNumber) { phoneNumber = payloadData.phoneNumber }

          let token_data = null

          if(email != null || countrycode != null && phoneNumber != null) {

              console.log("--------------------case_1---")

                // # check email
                if(email != null) {

                    let check_email = await verify_email(email)
                    if(check_email.length) { 
                        let token_info = {
                             _id : check_email[0]._id,
                             collection : Models.users,
                             scope : Config.APP_CONSTANTS.SCOPE.USER
                        }
                
                        if(payloadData.deviceId){
                            token_info.deviceToken = payloadData.deviceId
                        }
                          
                        if(payloadData.deviceType){
                            token_info.deviceType = payloadData.deviceType
                        }
                
                        token_data = await commonController.generate_token(token_info)
                        //###-------get policy terms and conditions --
                        let policy_terms_data = await commonController.policy_terms_condtions();
                        return {
                             _id : token_data._id,
                             fullName : token_data.fullName,
                             email : token_data.email,
                             countrycode : token_data.countrycode,
                             phoneNumber : token_data.phoneNumber,
                             profilePicture : token_data.profilePicture,
                             accessToken : token_data.accessToken,
                             policy : policy_terms_data[0].policy,
                             termsCondition : policy_terms_data[0].termsCondition
                         }
                      }

                    }
                    // ## check phone number
                    if(countrycode != null && phoneNumber != null) {

                            let check_phone_no = await verify_phone_no(countrycode, phoneNumber)
                            if(check_phone_no.length) { throw ERROR.PHONENO_ALREADY_EXIST_USER }
                    }
                                  let create_user = await data_to_save(payloadData)
                                  let token_info = {
                                        _id : create_user._id,
                                        collection : Models.users,
                                        scope : Config.APP_CONSTANTS.SCOPE.USER
                                  }
                                   
                                  if(payloadData.deviceId){
                                    token_info.deviceToken = payloadData.deviceId
                                  }
                                  
                                  if(payloadData.deviceType){
                                    token_info.deviceType = payloadData.deviceType
                                  } 

                                  token_data = await commonController.generate_token(token_info)
                                  let policy_terms_data = await commonController.policy_terms_condtions();
                                  return {
                                     _id : token_data._id,
                                     fullName : token_data.fullName,
                                     socialKey :token_data.socialKey,
                                     email : token_data.email,
                                     countrycode : token_data.countrycode,
                                     phoneNumber : token_data.phoneNumber,
                                     profilePicture : token_data.profilePicture,
                                     accessToken : token_data.accessToken,
                                     policy : policy_terms_data[0].policy,
                                     termsCondition : policy_terms_data[0].termsCondition
                                  }

          }      
          else if (email == null && countrycode == null && phoneNumber == null) {

                console.log("--------------------case_2---")

                let create_user = await data_to_save(payloadData)
                let token_info = {
                      _id : create_user._id,
                      collection : Models.users,
                      scope : Config.APP_CONSTANTS.SCOPE.USER
                }
                
                if(payloadData.deviceId){
                    token_info.deviceToken = payloadData.deviceId
                }
                  
                if(payloadData.deviceType){
                    token_info.deviceType = payloadData.deviceType
                }

                token_data = await commonController.generate_token(token_info)
                let policy_terms_data = await commonController.policy_terms_condtions();
                return {
                   _id : token_data._id,
                   fullName : token_data.fullName,
                   socialKey :token_data.socialKey,
                   email : token_data.email,
                   countrycode : token_data.countrycode,
                   phoneNumber : token_data.phoneNumber,
                   profilePicture : token_data.profilePicture,
                   accessToken : token_data.accessToken,
                   policy : policy_terms_data[0].policy,
                   termsCondition : policy_terms_data[0].termsCondition
                }
         }
    }
 }
 
 catch(err){
    throw err;
 }
} 


const data_to_save = async(data) => {
  try {

        let data_to_save = {}
        if(data.socialKey){data_to_save.socialKey = data.socialKey}
        if(data.email) { data_to_save.email = data.email.toLowerCase() }
        if(data.countrycode) { data_to_save.countrycode = data.countrycode }
        if(data.phoneNumber) { data_to_save.phoneNumber = data.phoneNumber }
        if(data.profilePicture) { data_to_save.profilePicture = data.profilePicture }
        if(data.fullName) { data_to_save.fullName = data.fullName }
        if(data.password) { data_to_save.password = data.password }

        console.log("----------------------------data_to_save---",data_to_save)

        let create_user = await DAO.saveData(Models.users, data_to_save)
        return create_user

   }
   catch(err) {
      throw err;
   }
 }


 const logout = async(userData) => {

    try {

          let query = { _id : userData._id }
          let update = { accessToken : null ,isOnline : false }
          let options = { new : true }
          let data_to_update = await DAO.findAndUpdate(Models.users, query, update, options)

          return {}
        
    }
    catch(err) {
      throw err;
    }

 }


const forgotPassword = async(payloadData) => {

  try {
 
        let query = { email : payloadData.email.toLowerCase() };
        let projection = { _id : 1 }
        let options = { lean : true }  
        let fetch_data = await DAO.getData(Models.users,query,projection,options);

        if(fetch_data.length == 0) { throw ERROR.NO_DATA_FOUND }
        else { 

              let conditions = { _id : fetch_data[0]._id }
              let update = { $set : { password : PASSWORD } }
              let options = { new : true };
              let data_to_update = await DAO.findAndUpdate(Models.users,conditions,update,options); 
              return data_to_update   

        }
  }
  catch(err){
      throw err;
  }
};


const verifyOtp = async(payloadData, userData) => {

    try {  

            if(userData.otp != payloadData.otp) { throw ERROR.INVALID_OTP }
            else {

                  let query = { _id : userData._id }
                  let update = { otpverify : true }
                  let options = { new : true }
                  let data_to_update = await DAO.findAndUpdate(Models.users, query, update, options)
                  return data_to_update
            }

      }
      catch(err) {
            throw err;
      }
}


const reSendOtp = async(payloadData) => {

  try {

          let query = { 
              phoneNumber : payloadData.phoneNumber,
              countrycode : payloadData.countrycode,
              isBlocked : false,
              isDeleted : false,
          }

          let projection = { __v : 0 }
          let options = { lean : true }

          let fetch_data = await DAO.getData(Models.users, query, projection, options)

          if(fetch_data.length == 0) { throw ERROR.NO_DATA_FOUND }
          else {

                let query = { _id : fetch_data[0]._id }
                let update = { $set : { otp : OTP } } 
                let options= { new : true };
                let updateNewOtp = await DAO.findAndUpdate(Models.users, query, update, options); 
                return updateNewOtp;  
          }

      }
      catch(err) {
              throw err;
      }
}


const changePasword = async(payloadData, userData) => {
    try {

          let query = { _id : userData._id , password : payloadData.old_password};
          let update = { password : payloadData.New_password };
          let options= { new : true };
          let updatePassword = await DAO.findAndUpdate(Models.users, query, update, options);
          if(updatePassword){
               return  updatePassword;
          }
          else{
              throw ERROR.OLD_PASSWORD_MISMATCH;
          }

    }
    catch(err){
        throw err;
    }
};



const profileUpdate  = async(userData,payloadData) =>{
    try{
           
          console.log("------------user_data----------------",payloadData);

          let query = { _id : userData._id };
          let options = { new : true };
          let setData = {};  
 
          if(payloadData.fullName){  setData.fullName = payloadData.fullName }
              //####---check email 
          if(payloadData.email){

              setData.email = payloadData.email.toLowerCase()

              email = payloadData.email.toLowerCase()
              let check_email = await commonController.check_user_email(email)

              if(check_email.length) { throw ERROR.EMAIL_ALREADY_EXIST }
          }
              //####---check phoneNumber
          if( payloadData.countrycode != null && payloadData.phoneNumber != null) {

              setData.phoneNumber = payloadData.phoneNumber;
              setData.countrycode = payloadData.countrycode; 
 
              countrycode = payloadData.countrycode;
              phoneNumber = payloadData.phoneNumber;

              let check_phone_no = await commonController.check_user_phone_no(countrycode, phoneNumber)

          if(check_phone_no.length) { throw ERROR.PHONENO_ALREADY_EXIST }
          }

          if(payloadData.lng && payloadData.lat){

                   setData.location = {
                      type: "Point",
                      coordinates: [payloadData.lng, payloadData.lat]  
                   }

          }
          if(payloadData.profilePicture){

                   setData.profilePicture = {
                                             thumbnail : payloadData.profilePicture,  
                                             original : payloadData.profilePicture
                   }

          }                                  
          if(payloadData.delete){  setData.isDeleted = payloadData.delete  }
          if(payloadData.address){  setData.address = payloadData.address }                           
        
              let updateProfile = await DAO.findAndUpdate(Models.users,query,setData,options)
              return updateProfile ; 

         } 
         catch(err){
         throw err;
         }
      };


 const deviceDataUpdate  = async(userData,payloadData) =>{
        try{
               
              console.log("------------user_data----------------",payloadData);
    
              let query = { _id : userData._id };
              let options = { new : true };
              let setData = {};  
     
              if(payloadData.deviceId){
                  setData.deviceToken = payloadData.deviceId
              }
              
              if(payloadData.deviceType){
                  setData.deviceType = payloadData.deviceType
              }                           
              let updateProfile = await DAO.findAndUpdate(Models.users,query,setData,options)
              return updateProfile ; 
    
        } 
        catch(err){
             throw err;
        }
 };


   const Home_Page = async(payloadData,userData)=>{
         try{

                    let query = {isDeleted : false, isBlocked : false};
                    let projections = {_id : 1, name : 1,icon:1};
                    let option = { lean : true };
                    let checkCategory = await DAO.getData( Models.categories , query , projections , option );
                    return checkCategory;
  
         }
         catch(err){
                 throw err;
         }
    };
    
const search_products = async(payloadData,userData)=>{
        try{

            let fetch_data = await searchProducts.search_data(payloadData.categoryId,payloadData.searchProduct);
             return fetch_data;

        }catch(err){
         throw err;
        }
   };


   const Sub_Category_Product_list = async(payloadData,userData)=>{
         try{

              let categoryId = payloadData.categoryId;
              let fetch_data = await userSubcategory.list_sub_category_product(categoryId);
              return fetch_data;

         }catch(err){
          throw err;
         }
    };

    const scheduleDateTime = async(userData)=>{
     try{  
            
              let query = {isDeleted: false};
              let projection = {__v : 0 ,isDeleted :0,createdAt:0,updatedAt:0};
              let option = {lean : true};
              let fetch_data = await DAO.getData(Models.dateTimes,query,projection,option)

              return fetch_data;

      }
      catch(err){
              throw err;
      }
    };


    const dateTimeGet = async(userData)=>{
        try{  
               
                 let query = {isDeleted : false};
                 let projection = {__v : 0 ,isDeleted :0,createdAt:0,updatedAt:0};
                 let option = {lean : true};
                 let fetch_data = await DAO.getData(Models.dateTimes,query,projection,option)
                
                 if(fetch_data.length == 0){throw ERROR.NO_SLOT_AVAILABLE}
                 return fetch_data;   
         }
         catch(err){
                 throw err;
         }
       };
   

    const createBooking = async(payloadData,userData)=>{
        try{
              let fetch_data = await managed_booking(payloadData);
              let setData = {

                      user_id : userData._id,
                      booking_id : fetch_data.bookingId,
                      date : fetch_data.date,
                      time : fetch_data.time,
                      address : payloadData.address,
                      paymentMode : payloadData.paymentMode,
                    
                      servicelocation :  {
                                type : "Point",
                                coordinates : [payloadData.lng, payloadData.lat]
                      }
              }

              if(payloadData.specialInstruction) {  setData.specialInstruction = payloadData.specialInstruction }
              if(payloadData.timeZone) {  setData.timeZone =  payloadData.timeZone }
              if(payloadData.paymentIntentKey){setData.paymentIntentKey = payloadData.paymentIntentKey}
              let create_data = await DAO.createData(Models.bookings, setData);
              
              //###-------save products----------
              let save_booking_details = await booking_details(create_data._id,payloadData.products,) 

              if(payloadData.cardId && payloadData.totalPrice)
              {
                let payment_hold =  await bookingPayment(payloadData.cardId,payloadData.totalPrice)
                let query = {_id : create_data._id };
                let setData = {paymentIntentKey : payment_hold};
                let option = {new : true};
                let save_key = await DAO.findAndUpdate(Models.bookings,query,setData,option)
                console.log(save_key)
                return create_data;  
              }
              return create_data;
      }
      catch(err){
              throw err;
      }
  };


  const booking_details = async(orderid,products)=>{
    try{

        let savedata = { } 

        for(let i = 0; i < products.length ; i++){
            
               let  orderId = orderid;
               let  productId = products[i].id;
               let  quentity = products[i].qty;
               let setData = {orderId : orderId,productId : productId,quentity : quentity};
               savedata = DAO.createData(Models.bookingDetails,setData);


        }
         return await savedata;
    }
    catch(err){
            throw err;
    }
};

  const managed_booking = async(payloadData)=>{
    try{
      
        console.log(random.RandomHax(8));
            //###-------genrate booking_id
           let bookingId = random.RandomHax(8);

           //###-------convert date in milliseconds
           let date =payloadData.date;
           let startdate = new Date(date).getTime();
         
           //###--------get mintues from time
           let tempTime1 = moment.duration(payloadData.time);
           let minutes =  tempTime1.minutes();

           //###--------get hours from time and convert in minutes
           let tempTime = moment.duration(payloadData.time);
           let hours1 =  tempTime.hours();
           let hours = hours1 * 60;     
     
           let setData = {}
               setData = {
                        bookingId : bookingId,
                        date : startdate,
                        time : hours+minutes
                }
           return setData;
     }
     catch(err){
           throw err; 
     } 
};


const orderListing = async(userData)=>{
    try{
       
          let userId =userData._id;
          let Service = await orderlisting.list_order(userId); 
          return Service ;
    }
    catch(err){
         throw err; 
    } 
};

   const OrderDetails = async(payloadData,userData)=>{    
        try{

           let orderId = payloadData.orderId;
           let userId = userData._id;
           let fetch_data = await orderSublisting.list_sub_order(userId,orderId);
           console.log(fetch_data)
           return fetch_data;

        }
        catch(err){
              throw err; 
        } 
   };
   
  

const giveRating = async(payloadData,userData)=>{
    try{
             let data = {}

             var userName = await commonController.get_user_name(userData._id)
             //### ---------get device token ---------
             let fetch_data = await commonController.get_provider_device_token(payloadData.providerId);

             let setData = { 

                 userId :  userData._id,
                 providerId : payloadData.providerId,
                 orderId : payloadData.orderId,
                 rating : payloadData.rating

              }
              
              if (payloadData.message) {
                setData.message = payloadData.message;
             
                data = {
                   title: "you got rating ",
                   message: payloadData.message + " " +payloadData.rating+" is rated by "+userName[0].fullName,
                   type: "Rating",
                }
              }
              else{  
                data = {
                   title: "you got rating ",
                   message:payloadData.rating +"is rated by "+userName[0].fullName,
                   type: "Rating",
                }
              }
              if(fetch_data.length != 0){
              let push_notification = await commonController.push_notification_status(fetch_data[0].deviceToken, data)
           
              //###-------save notification ------
              let ids = {
                 recieverId: payloadData.providerId,
                 senderId: userData._id,
              }
              let pic = {}
                if(userName[0].profilePicture.original != undefined){
                    pic = userName[0].profilePicture.original
                }
              let save_notification = await commonController.save_notification(ids, data,pic);
              console.log("save notifucation ", save_notification);
            }
              //### ---------save rating ---------
              let save_rating  = await DAO.saveData(Models.providerRatings,setData)
              return save_rating;                       
      
    } 
    catch(err){
        throw err;
    }
};


const getUserDetils = async(payloadData,userData)=>{
    try{
        
         let query = { _id : payloadData.userId };
         let projections = { __v : 0 };
         let option = { lean : true };
         let check_user_details = await DAO.getData(Models.users, query, projections, option );
         //###-------check user id 
         if(check_user_details.length == 0){ throw ERROR.INVALID_USER_ID }
         else{
             return {
                profilePicture : check_user_details[0].profilePicture,
                fullName : check_user_details[0].fullName,
                email : check_user_details[0].email,
                phoneNumber : check_user_details[0].phoneNumber,
                location : check_user_details[0].location.coordinates,
                address : check_user_details[0].address
             }
                                   
         }
    }  
    catch(err){
        throw err;
    }
};


const sendMessages = async(payloadData,userData)=>{
    try{
        
         let query = { _id : payloadData.bookingId };
         let projections = { __v : 0 };
         let option = { lean : true };
         let check_provider_Id = await DAO.getData(Models.bookings, query, projections, option );
         //###-------check provider id 
         if(check_provider_Id.length == 0){ throw ERROR.INVALID_BOOKING_ID }
         else{
           //###--------save messages ---------


           let data = {}

           //### ------------get user name-------
           var userName = await commonController.get_user_name(userData._id)
             //### ---------get device token ---------
             let fetch_data = await commonController.get_provider_device_token(check_provider_Id[0].providerId);
          
              if(payloadData.message){
                 data = {
                    title: userName[0].fullName,
                    message: payloadData.message,
                    type: "Message",
                 }
              }
              if(payloadData.picture){
                data = {
                    title: userName[0].fullName,
                    message:payloadData.picture,
                    type: "Message",
                }
             }
             if(fetch_data.length != 0){ 
           let push_notification = await commonController.push_notification_status(fetch_data[0].deviceToken, data)
           

            //###-------save notification ------
            let ids = {
                recieverId: check_provider_Id[0].providerId,
                senderId: userData._id,
            }
            let pic = {}
                if(userName[0].profilePicture.original != undefined){
                    pic = userName[0].profilePicture.original
                }
            let save_notification = await commonController.save_notification(ids, data,pic);
            console.log("save notifucation ", save_notification);
          }
           let save_data  = await commonController.save_messages_user(payloadData,userData._id)
           return save_data;                       
         }
    }  
    catch(err){
        throw err;
    }
};


const messagesListing = async(payloadData,userData)=>{
    try{

         let query = { bookingId : payloadData.bookingId };
         let projections = {_id : 1,userId : 1,bookingId : 1, message : 1, picture : 1, time : 1  };
         let option = { lean : true };
         let send_messages = await DAO.getmessage(Models.messages, query, projections, option );

         return send_messages;
        
   }  
   catch(err){
        throw err;
   }
};

const getTermCondition = async()=>{
    try{
         
        let policy_terms_data = await commonController.policy_terms_condtions();
         return {
            policy : policy_terms_data[0].policy,
            termsCondition : policy_terms_data[0].termsCondition
         }
        
   }  
   catch(err){
        throw err;
   }
};

const accessTokenLogin = async(userData)=>{
    try{


          let userId =userData._id;
          let query = {_id  : userId  ,isDeleted : false}
          let projection = {accessToken : 1, 
             socialKey:1,  
             fullName : 1,
             phoneNumber : 1, 
             countrycode : 1, 
             email : 1,
             location : 1, 
             address : 1,
             profilePicture : 1,
             isOnline : 1,    
             deviceToken : 1,
             deviceType : 1,
             otpverify : 1 , 
             notification:1 
          }
          let options = {lean : true}
          let get_data  = await DAO.getdata(Models.users,query ,projection,options)
          return get_data ;
        
   }  
   catch(err){
        throw err;
   }
};

const notificationListing = async(userData)=>{
    try{
     let query = { recieverId : userData._id,isDeleted : false};
     let projection = {__v : 0 }
     let option = {lean : true };    
     let fetch_data = await DAO.getdata(Models.notifications,query,projection,option );
     return fetch_data;
         
   }  
   catch(err){
        throw err;
   }
};


const notificationDelete = async(userData,payloadData)=>{
  try{
   let query = { recieverId : userData._id};
   let update = {isDeleted : payloadData.isDeleted }
   let option = {new : true };    
   let deleteNotification = await DAO.updateMany(Models.notifications,query,update,option );
   return deleteNotification;
       
 }  
 catch(err){
      throw err;
 }
};



const addCard = async (payloadData, userData) => {
  try {
    let createCustomer = await createCustomers(userData._id, payloadData.token);

    console.log("...createCustomer......", createCustomer);
    let createSources = await createSource(createCustomer.customerId, createCustomer.sourceId);

    console.log("...createSources......", createSources);

    if (!(payloadData.type)) {
      payloadData.type = ""
    }

    let saveData = await DAO.saveData(Models.cards, {
      userId: userData._id,
      customerId: createCustomer.customerId,
      sourceId: createCustomer.sourceId,
      brand: createSources.brand,
      exp_month: createSources.exp_month,
      exp_year: createSources.exp_year,
      last4: createSources.last4,
      name : createSources.name,
      funding: createSources.funding,
      isDeleted: false,
      type: payloadData.type
    })

    let user = await DAO.findAndUpdate(Models.users, { _id: userData._id}, { isActive: true}, { new : true });
    return  saveData;

  } catch (err) {
    throw err;
  }
}


const createCustomers = async(userId,token) => {
  try {

        let get_user_info = await commonController.check_user(userId)
        let mobileNumber = null ,userName = null, email = null;
        if(get_user_info.length =! 0){
          mobileNumber = get_user_info[0].phoneNumber,
          userName = get_user_info[0].fullName,
          email = get_user_info[0].email
        }

        let customer = await stripe.customers.create({
              description : 'Customer for ' + userId,
              source : token,
              email : email,
              name : userName,
              phone : mobileNumber
        });

        let  customerId = customer.id;
        let  sourceId = customer.default_source;
        console.log("customer data ",customer)
        return { customerId : customerId, sourceId : sourceId }

  }
  catch(err) {
        throw err;
        
  }
}


const createSource = async (customerId, sourceId) => {
  try {
         let card =  await  stripe.customers.retrieveSource(customerId,sourceId)
         console.log("card data ",card)
         return { brand: card.brand, exp_month: card.exp_month, exp_year: card.exp_year, last4: card.last4, funding: card.funding,name : card.name }
        
        }
        catch(err) {
              throw err;
            
        }
}



const listCard = async (payloadData , userData) => {
  try {
    let query = { isDeleted: false, userId: userData._id};

    if(payloadData._id) {
       query._id = payloadData._id
    }
    let options = {lean : true};
    let projections = {};
    let populate =[
      { 
        path : 'userId', 
        select : '_id fullName ',
      },
    ]
    let listCard = await DAO.populateData(Models.cards, query, projections, options,populate);
    return listCard

  } catch (err) {
    throw err;
  }
}


const deleteCard = async (payloadData, userData) => {
  try {
    let query = { _id: payloadData._id , userId : userData._id };
    let setData = { isDeleted: payloadData.isDeleted }
    let options = { new : true };
    let deletSet = await DAO.findAndUpdate(Models.cards, query, setData, options);
    return null;
  } catch (err) {
    throw err;
  }
}


  
 const bookingPayment = async (cardId,totalPrice) => {
    try {
   
        var cardInfo = await  commonController.get_card_info(cardId)
        console.log("Card info data ",cardInfo)

        let create_payment_intent =await hold_payment_intent(cardInfo, totalPrice)
        console.log("payment intent data ",create_payment_intent)
        if(create_payment_intent)
        {
          return create_payment_intent
        }
        else{
          throw ERROR.TRANSATION_FAILD;
        }
    } catch (err) {
      throw err;
    }
  }

  
  const hold_payment_intent = async(cardInfo,price)=>{
    try{
            let paymentIntent = await stripe.paymentIntents.create({
               amount: Math.round(price * 100),
               currency: 'USD',
               payment_method_types: ['card'],
               customer : cardInfo[0].customerId,
               capture_method : "manual"
            });
            return paymentIntent.id 
  }
  catch(err){
        throw err; 
  } 
  };

module.exports = {
  userLogin:userLogin,
  Login_gamil:Login_gamil,
  userRegister:userRegister,
  profileUpdate:profileUpdate,
  verifyOtp:verifyOtp,
  reSendOtp:reSendOtp,
  changePasword:changePasword,
  forgotPassword:forgotPassword,  
  logout:logout,
  addCard :addCard,
  messagesListing: messagesListing,
  deviceDataUpdate:deviceDataUpdate,
  Home_Page:Home_Page,
  scheduleDateTime:scheduleDateTime,
  dateTimeGet:dateTimeGet,
  Sub_Category_Product_list:Sub_Category_Product_list,
  managed_booking:managed_booking,
  createBooking:createBooking,
  orderListing:orderListing,
  OrderDetails:OrderDetails,
  addAddress : addAddress,
  search_products:search_products,
  sendMessages:sendMessages,
  giveRating:giveRating,
  getUserDetils:getUserDetils,
  accessTokenLogin:accessTokenLogin,
  booking_details:booking_details,
  getTermCondition:getTermCondition,
  notificationListing:notificationListing,
  listCard :  listCard,
  notificationDelete :notificationDelete,
  deleteCard : deleteCard,
  hold_payment_intent :hold_payment_intent,
  bookingPayment : bookingPayment,
  StripeListing : StripeListing,
  imageUpload : imageUpload,
};

const DAO = require("../DAOManager").queries;
const Config = require("../Config");
const TokenManager = require("../Libs/tokenManager");
const NotificationsManager = require("../Libs/NotificationsManager");
const ERROR = Config.responseMessages.ERROR;
const Models = require("../Models");
const bcrypt = require("bcryptjs");
const momentTimeZone = require('moment-timezone');
const UniversalFunctions = require("../Utils/UniversalFunctions");
const winston = require("winston");
const commonController = require('./commonController');
const categoryList = require('../Aggregation/categoryList');
const bookingListing = require('../Aggregation/bookingListing');
const { saveData } = require("../DAOManager/queries");
var stripe = require("stripe")("sk_test_51IeNFEC3cNboZ0zDOVljMUqyJFImT3MtStT4ZPIgF1PnbMVt5jxg1sYRfaHUH6UqxdRclGFwpFAyxPNmHk0aQwRV00955G6PTg");



const adminLogin = async (payloadData, callback) => {
  try {
    //###--------check email ---------- 
    let email = payloadData.email.toLowerCase()
    let query = { email: email };
    let projection = { __v: 0 };
    let option = { lean: true };
    let fetch_data = await DAO.getData(Models.admins, query, projection, option);

    if (fetch_data.length == 0) { throw ERROR.EMAIL_NOT_FOUND }
    else {
      //###-------check password----------
      if (payloadData.password != fetch_data[0].password) { throw ERROR.WRONG_PASSWORD }

      //###------ generate token--------
      let token_info = {
        _id: fetch_data[0]._id,
        collection: Models.admins,
        scope: Config.APP_CONSTANTS.SCOPE.ADMIN

      }
      let token_data = await commonController.generate_token(token_info)

      return {
        _id: token_data._id,
        email: token_data.email,
        accessToken: token_data.accessToken
      }
    }
  }
  catch (err) {
    throw err;
  }
};



const changePassword = async (payloadData, adminData) => {
  try {

    let conditions = { _id: adminData._id };
    let update = { $set: { password: payloadData.New_password } };
    let options = { new: true };
    let updatePassword = await DAO.findAndUpdate(Models.admins, conditions, update, options);
    return updatePassword;

  }
  catch (err) {
    throw err;
  }
};



const addEditCategory = async (payloadData, adminData) => {
  try {

    //## ------- check category_id
    if (payloadData.categoryId) {

      categoryId = payloadData.categoryId;
      let check_category_id = await commonController.check_category_id(categoryId)

      if (check_category_id.length == 0) { throw ERROR.INVALID_CATEGORY_ID }
      let setData = {};
      //### ------edit category -----               

      if (payloadData.category) { setData.name = payloadData.category }
      if (payloadData.icon) { setData.icon = payloadData.icon }

      let condition = { _id: payloadData.categoryId }
      let options = { new: true };
      let edit_category = await DAO.findAndUpdate(Models.categories, condition, setData, options);
      return edit_category;
    }
    else {

      //### --- save category-----
      let savedata = {
        name: payloadData.category,
        icon: payloadData.icon
      }

      let save_category = await DAO.saveData(Models.categories, savedata);
      return save_category;

    }
  }
  catch (err) {
    throw err;
  }
};


const listCategory = async (payloadData, adminData) => {
  try {
    let query = { isDeleted: false };
    let Numbers = 0;
    if (payloadData.pageNumber) {
      Numbers = payloadData.pageNumber
    }

    if (payloadData.categoryId) { query._id = payloadData.categoryId }

    let projections = {};
    let option = { lean: true };
    let check_category = await DAO.getDataPages(Models.categories, query, projections, option, Numbers);
    return check_category;

  }
  catch (err) {
    throw err;
  }
};


const deleteBlockCategory = async (payloadData, adminData) => {
  try {

    let setData = {};

    if (payloadData.deleted || payloadData.deleted == false) { setData.isDeleted = payloadData.deleted }
    if (payloadData.blocked || payloadData.blocked == false) { setData.isBlocked = payloadData.blocked }
    //### ------delete and block category -----                        
    let condition = { _id: payloadData.categoryId }
    let options = { new: true };
    let edit_category = await DAO.findAndUpdate(Models.categories, condition, setData, options);
    return edit_category;
  }
  catch (err) {
    throw err;
  }
};


const addEditSubCategory = async (payloadData, adminData) => {
  try {


    //## ------- check sub_category_id
    if (payloadData.subCategoryId) {

      subCategoryId = payloadData.subCategoryId;
      let check_sub_category_id = await commonController.check_sub_category_id(subCategoryId)

      if (check_sub_category_id.length == 0) { throw ERROR.INVALID_SUBCATEGORY_ID }
      let setData = {};

      if (payloadData.subCategory) { setData.name = payloadData.subCategory }
      if (payloadData.categoryId) { setData.categoryId = payloadData.categoryId }

      //### ------edit sub_category -----                        
      let condition = { _id: payloadData.subCategoryId }
      let options = { new: true };
      let edit_category = await DAO.findAndUpdate(Models.subCategories, condition, setData, options);
      return edit_category;
    }
    else {
      //###------save subcategory--------
      let savedata = {
        name: payloadData.subCategory,
        categoryId: payloadData.categoryId
      }

      let save_category = await DAO.saveData(Models.subCategories, savedata);
      return save_category;
    }
  }
  catch (err) {
    throw err;
  }
};


const listSubCategory = async (payloadData, adminData) => {
  try {

    let query = { isDeleted: false, isBlocked: false };
    let Numbers = 0;
    if (payloadData.pageNumber) {
      Numbers = payloadData.pageNumber
    }

    if (payloadData.subCategoryId) { query._id = payloadData.subCategoryId }

      let populate = [
        {
          path: "categoryId",
          select: "_id name"
        }
      ];
      let projection = { __v: 0 }
      let option = {};
      let get_data =await DAO.populateDataAdmin(Models.subCategories, query, projection, option,Numbers, populate)
      return get_data
  }
  catch (err) {
    throw err;
  }
};


const deleteBlockSubCategory = async (payloadData, adminData) => {
  try {

    let setData = {};

    if (payloadData.deleted || payloadData.deleted == false) { setData.isDeleted = payloadData.deleted }
    if (payloadData.blocked || payloadData.blocked == false) { setData.isBlocked = payloadData.blocked }
    //### ------delete and block sub category -----                        
    let condition = { _id: payloadData.subCategoryId }
    let options = { new: true };
    let edit_sub_category = await DAO.findAndUpdate(Models.subCategories, condition, setData, options);
    return edit_sub_category;
  }
  catch (err) {
    throw err;
  }
};

const addEditProducts = async (payloadData, adminData) => {
  try {

    //###------------case1------------------
    if (payloadData.productId) {
      productId = payloadData.productId;
      let check_productId = await commonController.check_productId(productId)

      if (check_productId.length == 0) { throw ERROR.INVALID_PRODUCT_ID }
      //###------edit----product                
      else {
        let setData = {}
        if (payloadData.subCategoryId) { setData.subCategoryId = payloadData.subCategoryId }

        if (payloadData.categoryId) { setData.categoryId = payloadData.categoryId }

        if (payloadData.description) { setData.description = payloadData.description }

        if (payloadData.price) { setData.price = payloadData.price }

        if (payloadData.product) { setData.name = payloadData.product }

        if (payloadData.productPicture) {
          setData.picture = {
            thumbnail: payloadData.productPicture,
            original: payloadData.productPicture,
          }
        }
        //###----------edit product -----------   
        let query = { _id: payloadData.productId };
        let options = { new: true };
        let edit_product = await DAO.findAndUpdate(Models.products, query, setData, options)
        return edit_product;
      }
    } //###---------------------case2---------------------
    else {
      let setdata = {
        picture: {
          thumbnail: payloadData.productPicture,
          original: payloadData.productPicture,
        },
        categoryId: payloadData.categoryId,
        description: payloadData.description,
        subCategoryId: payloadData.subCategoryId,
        name: payloadData.product,
        price: payloadData.price,
      }
      let save = await DAO.saveData(Models.products, setdata);
      return save;
    }
  }
  catch (err) {
    throw err;
  }
};


const listProduct = async (payloadData, adminData) => {
  try {

    let query = { isDeleted: false, isBlocked: false };

    let Numbers = 0;
    if (payloadData.pageNumber) {
      Numbers = payloadData.pageNumber
    }
    if (payloadData.productId) {
      query._id = payloadData.productId
    }

      let populate = [
        {
          path: "categoryId",
          select: "_id name",
        },
        {
          path: "subCategoryId",
          select: "_id name",
        }
      ];
      let projection = {
          _id:1,
          categoryId:1,
          subCategoryId:1,
          picture:1,
          description:1,
          name:1,
          price:1,
          isDeleted:1,
          isBlocked:1 
      }
      let option = {};
      let get_data = await DAO.populateDataAdmin(Models.products, query, projection, option,Numbers, populate) 
      return get_data

  }
  catch (err) {
    throw err;
  }
};

const deleteBlockProduct = async (payloadData, adminData) => {
  try {

    let setData = {};

    if (payloadData.deleted || payloadData.deleted == false) { setData.isDeleted = payloadData.deleted }
    if (payloadData.blocked || payloadData.blocked == false) { setData.isBlocked = payloadData.blocked }
    //### ------delete and block sub product -----                        
    let condition = { _id: payloadData.productId }
    let options = { new: true };
    let edit_product = await DAO.findAndUpdate(Models.products, condition, setData, options);
    return edit_product;
  }
  catch (err) {
    throw err;
  }
};


const categoriesList = async (payloadData, adminData) => {
  try {

    let fetch_data = await categoryList.categories_List();
    return fetch_data;
  }
  catch (err) {
    throw err;
  }
};



const listUser = async (payloadData, adminData) => {
  try {
    let query = { isDeleted: false, isBlocked: false };

    let Numbers = 0;
    if (payloadData.pageNumber) {
      Numbers = payloadData.pageNumber
    }
    if (payloadData.userId) {
      query._id = payloadData.userId
    }


    let projections = {};
    let option = { lean: true };
    let check_user = await DAO.getDataPages(Models.users, query, projections, option, Numbers);
    return check_user;

  }
  catch (err) {
    throw err;
  }
};



const listProvider = async (payloadData, adminData) => {
  try {
    let query = { isDeleted: false, isBlocked: false };

    let Numbers = 0;
    if (payloadData.pageNumber) {
      Numbers = payloadData.pageNumber
    }

    if (payloadData.providerId) {
      query._id = payloadData.providerId
    }
    let projections = {};
    let option = { lean: true };
    let check_provider = await DAO.getDataPages(Models.providers, query, projections, option, Numbers);
    return check_provider;
  }
  catch (err) {
    throw err;
  }
};


const deleteBlockUser = async (payloadData, adminData) => {
  try {

    let setData = {};

    if (payloadData.deleted || payloadData.deleted == false) { setData.isDeleted = payloadData.deleted }

    if (payloadData.blocked || payloadData.blocked == false) { setData.isBlocked = payloadData.blocked }
    let query = { _id: payloadData.userId };
    let options = { new: true };
    let update = await DAO.findAndUpdate(Models.users, query, setData, options);
    return update

  }
  catch (err) {
    throw err;
  }
};



const deleteBlockProvider = async (payloadData, adminData) => {
  try {

    let setData = {};

    if (payloadData.deleted || payloadData.deleted == false) { setData.isDeleted = payloadData.deleted }

    if (payloadData.blocked || payloadData.blocked == false) { setData.isBlocked = payloadData.blocked }

    let query = { _id: payloadData.providerId };
    let options = { new: true };
    let update = await DAO.findAndUpdate(Models.providers, query, setData, options)
    return update

  }
  catch (err) {
    throw err;
  }
};

const createDateTime = async (payloadData, adminData) => {
  try {
 
    if(payloadData._id){

      
      let setData = {
            time: payloadData.time,
      };
      let query = {_id : payloadData._id };
      let option = { new: true }
      let saveDateTime = await DAO.findAndUpdate(Models.dateTimes, query, setData, option)
      return saveDateTime;
    }
else{
  
        let setData = {          
            time : payloadData.time
        }  
        let saveDateTime = await DAO.createData(Models.dateTimes,setData)
        return saveDateTime  
    }
  }
  catch (err) {
    throw err;
  }
};


const dateTimeListing = async (payloadData, adminData) => {
  try {
    let query = {};
    if (payloadData._id) {
      query._id = payloadData._id
    }

    let projections = {__v : 0};
    let option = { lean: true };
    let check_provider = await DAO.getData(Models.dateTimes, query, projections, option);
    return check_provider;
  }
  catch (err) {
    throw err;
  }
};

const bookingList = async (adminData, payloadData) => {
  try {
    let date = payloadData.startDate;
    let startdate = new Date(date).getTime();
    let date1 = payloadData.endDate;
    let enddate = new Date(date1).getTime();
    let fetch_data = await bookingListing.list_booking(startdate, enddate);
    return fetch_data;
  }
  catch (err) {
    throw err;
  }
};


const bookingManaged = async (payloadData) => {
  try {
     payloadData.status;
     let date = new Date(Date.now()).getTime();
     // ### ---- save status in logs    
     let condtion1 = { _id: payloadData.bookingId };
     let option = { new: true };
     let updateData = { $push: { logs: { status: payloadData.status, date: date } } }
     let save_data = await DAO.findAndUpdate(Models.bookings, condtion1, updateData, option)

     let update = {logsStatus : payloadData.status,book : false }
     let update_booking = await DAO.findAndUpdate(Models.bookings,condtion1,update,option)
      
     if(update_booking.paymentIntentKey != null)
     {
      let id  = update_booking.paymentIntentKey;
      let paymentIntent = await stripe.paymentIntents.cancel(id);
      console.log(paymentIntent)
     }
     
     return fetch_data;
  }
  catch (err) {
    throw err;
  }
};


const providerApproval = async (adminData, payloadData) => {
  try {


    let query = { _id : payloadData.providerId };
    let setData = { isApproval: payloadData.approve };
    let options = { new: true };
    let update = await DAO.findAndUpdate(Models.providers, query, setData, options)
    return update;


  }
  catch (err) {
    throw err;
  }
};

const createPolicyAndTerm = async (payloadData, adminData) => {
  try {

    if (payloadData.policyId) {

      let setdata = {};
      if (payloadData.policy) { setdata.policy = payloadData.policy }
      if (payloadData.terms) { setdata.termsCondition = payloadData.terms }
      if (payloadData.isDeleted) { setdata.isDeleted = payloadData.isDeleted }

      let query = { _id: payloadData.policyId };
      let option = { new: true };
      let update_policy = await DAO.findAndUpdate(Models.policyTermConditions, query, setdata, option)
      return update_policy;
    }
    let setData = {};

    if (payloadData.policy) { setData.policy = payloadData.policy }
    if (payloadData.terms) { setData.termsCondition = payloadData.terms }


    let policy = await DAO.createData(Models.policyTermConditions, setData)
    return policy;

  }
  catch (err) {
    throw err;
  }
};


const policyTermListing = async (payloadData, adminData) => {
  try {

    let query = {isDeleted : false}
    if(payloadData._id){
      query._id = payloadData._id
    }
     
    let projection = {__v : 0}
    let options = {lean : true};
    let policy = await DAO.getData(Models.policyTermConditions,query,projection,options)
    return policy;

  }
  catch (err) {
    throw err;
  }
};



const imageUpload = async (payloadData, adminData) => {
  try {



  }
  catch (err) {
    throw err;
  }
};


const addEditStrip = async(userData,payloadData) =>{
  try{
     //## ------- check stripeId
             if(payloadData.id){

                    id = payloadData.id;
                   let check_email_id = await commonController.check_stripe_id(id)
             
                   if(check_email_id.length == 0){ throw ERROR.INVALID_ID }
                   let setData = {};
                     //### ------edit strip -----               
                     
                   if(payloadData.publishableKey){ setData.publishableKey = payloadData.publishableKey }
                   if(payloadData.secretKey){ setData.secretKey = payloadData.secretKey }
                   if(payloadData.mode){ setData.mode = payloadData.mode }
                   let condition = { _id : payloadData.id}
                   let options={new : true};
                   let edit_stripe = await DAO.findAndUpdate(Models.stripCredentials,condition, setData, options);
                   return edit_stripe;
             }
             else{
             
               //### --- save stripe-----
               let setData = {}
               if(payloadData.publishableKey){ setData.publishableKey = payloadData.publishableKey }
               if(payloadData.secretKey){ setData.secretKey = payloadData.secretKey }
               if(payloadData.mode){ setData.mode = payloadData.mode }

              let save_stripe = await DAO.saveData( Models.stripCredentials ,setData );
              return save_stripe;
            }

  }catch(err){
    throw err;
  }
}

const deleteStripCredentails = async(userData,paylaodData) =>{
  try{
      
    let getEmails = await DAO.remove1(Models.stripCredentials,{_id : paylaodData.id });
    return getEmails;
  }catch(err){
    throw err;
  }
}

const StripeListing = async(userData,paylaodData) =>{
  try{
     
    let query = { };
    if(paylaodData.id){
       query = {
          _id : paylaodData.id,
        };
    }
    let projection = { __v : 0};
    let options = {lean : true};
    let getStripe = await DAO.getData(Models.stripCredentials,query,projection,options);
    return getStripe;
  }catch(err){
    throw err;
  }
}


module.exports = {
  adminLogin: adminLogin,
  policyTermListing :policyTermListing,
  changePassword: changePassword,
  addEditCategory: addEditCategory,
  bookingList: bookingList,
  createDateTime: createDateTime,
  deleteBlockCategory: deleteBlockCategory,
  listCategory: listCategory,
  imageUpload: imageUpload,
  addEditSubCategory: addEditSubCategory,
  listSubCategory: listSubCategory,
  addEditProducts: addEditProducts,
  listProduct: listProduct,
  categoriesList: categoriesList,
  listUser: listUser,
  createPolicyAndTerm: createPolicyAndTerm,
  listProvider: listProvider,
  dateTimeListing : dateTimeListing,
  deleteBlockUser: deleteBlockUser,
  deleteBlockProvider: deleteBlockProvider,
  deleteBlockSubCategory: deleteBlockSubCategory,
  deleteBlockProduct: deleteBlockProduct,
  providerApproval: providerApproval,
  bookingManaged :bookingManaged,
  addEditStrip : addEditStrip,
  deleteStripCredentails : deleteStripCredentails,
  StripeListing : StripeListing,
};

const DAO = require("../DAOManager").queries;
const Config = require("../Config");
const TokenManager = require("../Libs/tokenManager");
const NotificationsManager = require("../Libs/NotificationsManager");
const ERROR = Config.responseMessages.ERROR;
const Models = require("../Models");
const UniversalFunctions = require("../Utils/UniversalFunctions");
const async = require("async");
const winston = require("winston");
const moment = require("moment");
const request = require('request');
const path = require('path');
const rn = require('random-number');
const Base64 = require('base-64');
const { SUCCESS } = require("../Config/responseMessages");
const { Mongoose } = require("mongoose");
const commonController = require('./commonController');
const listService = require('../Aggregation/listService');
const providerOderList = require('../Aggregation/providerOderList');
const providerOrderlisting = require('../Aggregation/providerOrderlisting');
const accessOrder = require('../Aggregation/accessOrderProvider');

var stripe = require("stripe")("sk_test_51IeNFEC3cNboZ0zDOVljMUqyJFImT3MtStT4ZPIgF1PnbMVt5jxg1sYRfaHUH6UqxdRclGFwpFAyxPNmHk0aQwRV00955G6PTg");
const OTP = 1234;
const PASSWORD = "qwerty";



const StripeListing = async (payloadData) => {
    try {

        let query = { mode : payloadData.type}
        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data = await DAO.getData(Models.stripCredentials, query, projection, options)
        return fetch_data

    }
    catch (err) {
        throw err;
    }
};

const listCategories = async () => {
    try {

        let query = { isDeleted: false, isBlocked: false }
        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data = await DAO.getData(Models.categories, query, projection, options)
        return fetch_data

    }
    catch (err) {
        throw err;
    }
};

const providerRegister = async (payloadData) => {

    try {

        // # check email 
        let email = payloadData.email.toLowerCase()
        let check_email = await commonController.check_provider_email(email)

        if (check_email.length) { throw ERROR.EMAIL_ALREADY_EXIST }
        else {

            // ## check mobile number
            let country_code = payloadData.countrycode;
            let phoneNumber = payloadData.phoneNumber;
            let check_phone_no = await commonController.check_provider_phone_no(country_code, phoneNumber)

            if (check_phone_no.length) { throw ERROR.PHONENO_ALREADY_EXIST }
            else {
                let setData = {
                    profilePicture: payloadData.profilePicture,
                    fullName: payloadData.fullName,
                    phoneNumber: payloadData.phoneNumber,
                    countrycode: payloadData.countrycode,
                    email: payloadData.email,
                    password: payloadData.password,
                    categoryId: payloadData.categoryId,
                    deviceToken: payloadData.deviceType,
                    deviceType: payloadData.deviceType

                }
                // ### save provider data
                let create_provider = await commonController.save_provider_data(setData)

                // #### generate token
                let token_info = {
                    _id: create_provider._id,
                    collection: Models.providers,
                    scope: Config.APP_CONSTANTS.SCOPE.PROVIDER
                }
                let token_data = await commonController.generate_token(token_info)
                let policy_terms_data = await commonController.policy_terms_condtions();

                let populate = [
                    {
                        path: "categoryId",
                        select: "_id name"
                    },
                ];
                let query = { _id: token_data._id }
                let projection = {
                    _id: 1,
                    fullName: 1,
                    email: 1,
                    countrycode: 1,
                    phoneNumber: 1,
                    profilePicture: 1,
                    accessToken: 1,
                    documentUploaded: 1,
                    categoryId: 1,
                    location: 1,
                    address: 1,
                    stripeId : 1,
                    policy: policy_terms_data[0].policy,
                    termsCondition: policy_terms_data[0].termsCondition
                }
                let option = {};
                let get_data = await DAO.populateData(Models.providers, query, projection, option, populate)
                return  get_data[0]
            }
        }
    }
    catch (err) {
        throw err;
    }
}


const addAddress = async (payloadData,userData) => {
    try {
        console.log(userData)
        let query = { _id: userData._id }
        let update = {
            "location.coordinates": [payloadData.lng, payloadData.lat],
            address: payloadData.address
        }
        let options = { new : true }
        let data_to_update = await DAO.findAndUpdate(Models.providers, query, update, options)
        console.log(data_to_update)
        return data_to_update;
  
    }
    catch (err) {
        throw err;
    }
};


const providerDocument = async (payloadData,providerData) => {

    try {
        console.log(providerData,"  ",providerData._id);
        let data_to_save = {
            provider_id: providerData._id,
            addproof: {
                thumbnail: payloadData.addressProofPicture,
                original: payloadData.addressProofPicture,
                number: payloadData.addressProofNumber,
                expireDate: payloadData.addressExpiryDate
            },
            identicard: {
                thumbnail: payloadData.identyPicture,
                original: payloadData.identyPicture,
                number: payloadData.identyNumber,
                expireDate: payloadData.identyExpiryDate
            },
            DocComplete: true
        }
        let save_docs = await DAO.createData(Models.providerDocs, data_to_save);
         console.log("save document",save_docs._id)
        // update in provider
        if (save_docs._id) {
            console.log(providerData._id)
            let query = { _id: providerData._id }
            let update = { documentUploaded: true }
            let options = { new: true }
            let save_docs1 = await DAO.findAndUpdate(Models.providers, query, update, options)
            return save_docs1 ;

        } else {
            throw ERROR.SOMETHING_WENT_WRONG;
        }

    }
    catch (err) {
        throw err;
    }

}


const providerLogin = async (payloadData) => {

    try {
        // # check email
        let query = {
            email: payloadData.email.toLowerCase(),
            isDeleted: false
        }
        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data = await DAO.getData(Models.providers, query, projection, options)

        if (fetch_data.length == 0) { throw ERROR.EMAIL_NOT_FOUND }
        else {

            // ## check password && check blocked
            if (payloadData.password != fetch_data[0].password) { throw ERROR.WRONG_PASSWORD }
            else if (fetch_data[0].isBlocked == true) { throw ERROR.BLOCKED }
            else {

                // ### generate new access token 
                let token_info = {
                    _id: fetch_data[0]._id,
                    collection: Models.providers,
                    scope: Config.APP_CONSTANTS.SCOPE.PROVIDER
                }

                if (payloadData.deviceId) {
                    token_info.deviceToken = payloadData.deviceId
                }

                if (payloadData.deviceType) {
                    token_info.deviceType = payloadData.deviceType
                }

                let token_data = await commonController.generate_token(token_info)
                let policy_terms_data = await commonController.policy_terms_condtions();


                console.log(token_data.categoryId)

                let populate = [
                    {
                        path: "categoryId",
                        select: "_id name"
                    }
                ];
                let query1 = { _id: token_data._id }
                let projection = {
                    _id: 1,
                    fullName: 1,
                    email: 1,
                    countrycode: 1,
                    phoneNumber: 1,
                    profilePicture: 1,
                    accessToken: 1,
                    documentUploaded: 1,
                    categoryId: 1,
                    location: 1,
                    address: 1,
                    stripeId : 1,
                    policy: policy_terms_data[0].policy,
                    termsCondition: policy_terms_data[0].termsCondition
                }
                let option = {};
                let get_data = await DAO.populateData(Models.providers, query1, projection, option, populate)
                return get_data[0] 
            }
        }
    }
    catch (err) {
        throw err;
    }

}

const verify_email = async (email) => {
    try {

        let query = { email: email, isDeleted: false }
        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data = await DAO.getData(Models.providers, query, projection, options)
        return fetch_data

    }
    catch (err) {
        throw err;
    }
}

const verify_phone_no = async (countrycode, phoneNumber) => {
    try {

        let query = {
            countrycode: countrycode,
            phoneNumber: phoneNumber,
            isDeleted: false
        }
        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data = await DAO.getData(Models.providers, query, projection, options)
        return fetch_data

    }
    catch (err) {
        throw err;
    }
}


const Login_gamil = async (payloadData) => {

    try {
        console.log("check data of provider-----", payloadData);
        let query = { socialKey: payloadData.socialKey };
        let projection = { __v: 0 };
        let option = { lean: true };
        let fetch_data = await DAO.getData(Models.providers, query, projection, option);

        console.log("social key data", fetch_data);
        if (fetch_data.length != 0) {

            let token_info = {
                _id: fetch_data[0]._id,
                collection: Models.providers,
                scope: Config.APP_CONSTANTS.SCOPE.PROVIDER
            }

            if (payloadData.deviceId) {
                token_info.deviceToken = payloadData.deviceId
            }

            if (payloadData.deviceType) {
                token_info.deviceType = payloadData.deviceType
            }
            token_data = await commonController.generate_token(token_info)
            let policy_terms_data = await commonController.policy_terms_condtions();
            let populate = [
                {
                    path: "categoryId",
                    select: "_id name"
                }
            ];
            let query1 = { _id: token_data._id }
            let projection = {
                _id: 1,
                fullName: 1,
                email: 1,
                countrycode: 1,
                phoneNumber: 1,
                profilePicture: 1,
                accessToken: 1,
                documentUploaded: 1,
                categoryId: 1,
                location: 1,
                address: 1,
                policy: policy_terms_data[0].policy,
                termsCondition: policy_terms_data[0].termsCondition
            }
            let option = {};
            let get_data = await DAO.populateData(Models.providers, query1, projection, option, populate)
            return get_data[0];
        }//###---- if social key not matched --
        else {

            let countrycode = null, phoneNumber = null, email = null;
            if (payloadData.email) { email = payloadData.email.toLowerCase() }
            if (payloadData.countrycode) { countrycode = payloadData.countrycode }
            if (payloadData.phoneNumber) { phoneNumber = payloadData.phoneNumber }
            

            let token_data = null

            if (email != null || countrycode != null && phoneNumber != null) {

                console.log("--------------------case_1---")

                // # check email
                if (email != null) {

                    let check_email = await verify_email(email)
                    if (check_email.length) {
                        let token_info = {
                            _id: check_email[0]._id,
                            collection: Models.providers,
                            scope: Config.APP_CONSTANTS.SCOPE.PROVIDER
                        }

                        if (payloadData.deviceId) {
                            token_info.deviceToken = payloadData.deviceId
                        }

                        if (payloadData.deviceType) {
                            token_info.deviceType = payloadData.deviceType
                        }

                        token_data = await commonController.generate_token(token_info)
                        //###-------get policy terms and conditions --
                        let policy_terms_data = await commonController.policy_terms_condtions();

                        let populate = [
                            {
                                path: "categoryId",
                                select: "_id name"
                            }
                        ];
                        let query1 = { _id: token_data._id }
                        let projection = {
                            _id: 1,
                            fullName: 1,
                            email: 1,
                            countrycode: 1,
                            phoneNumber: 1,
                            profilePicture: 1,
                            accessToken: 1,
                            documentUploaded: 1,
                            categoryId: 1,
                            location: 1,
                            address: 1,
                            policy: policy_terms_data[0].policy,
                            termsCondition: policy_terms_data[0].termsCondition
                        }
                        let option = {};
                        let get_data = await DAO.populateData(Models.providers, query1, projection, option, populate)
                        return get_data[0];
                          
                    }

                }
                // ## check phone number
                if (countrycode != null && phoneNumber != null) {

                    let check_phone_no = await verify_phone_no(countrycode, phoneNumber)
                    if (check_phone_no.length) { throw ERROR.PHONENO_ALREADY_EXIST }
                }
                let create_provider = await data_to_save(payloadData)
                let token_info = {
                    _id: create_provider._id,
                    collection: Models.providers,
                    scope: Config.APP_CONSTANTS.SCOPE.PROVIDER
                }

                if (payloadData.deviceId) {
                    token_info.deviceToken = payloadData.deviceId
                }

                if (payloadData.deviceType) {
                    token_info.deviceType = payloadData.deviceType
                }
                token_data = await commonController.generate_token(token_info)
                let policy_terms_data = await commonController.policy_terms_condtions();

                let populate = [
                    {
                        path: "categoryId",
                        select: "_id name"
                    }
                ];
                let query1 = { _id: token_data._id }
                let projection = {
                    _id: 1,
                    fullName: 1,
                    email: 1,
                    countrycode: 1,
                    phoneNumber: 1,
                    profilePicture: 1,
                    accessToken: 1,
                    documentUploaded: 1,
                    categoryId: 1,
                    location: 1,
                    address: 1,
                    policy: policy_terms_data[0].policy,
                    termsCondition: policy_terms_data[0].termsCondition
                }
                let option = {};
                let get_data = await DAO.populateData(Models.providers, query1, projection, option, populate)
                return get_data[0];

            }
            else if (email == null && countrycode == null && phoneNumber == null) {

                console.log("--------------------case_2---")
                let create_provider = await data_to_save(payloadData)
                let token_info = {
                    _id: create_provider._id,
                    collection: Models.providers,
                    scope: Config.APP_CONSTANTS.SCOPE.PROVIDER
                }

                if (payloadData.deviceId) {
                    token_info.deviceToken = payloadData.deviceId
                }

                if (payloadData.deviceType) {
                    token_info.deviceType = payloadData.deviceType
                }

                token_data = await commonController.generate_token(token_info)
                let policy_terms_data = await commonController.policy_terms_condtions();
                
                let populate = [
                    {
                        path: "categoryId",
                        select: "_id name"
                    }
                ];
                let query1 = { _id: token_data._id }
                let projection = {
                    _id: 1,
                    fullName: 1,
                    email: 1,
                    countrycode: 1,
                    phoneNumber: 1,
                    profilePicture: 1,
                    accessToken: 1,
                    documentUploaded: 1,
                    categoryId: 1,
                    location: 1,
                    address: 1,
                    policy: policy_terms_data[0].policy,
                    termsCondition: policy_terms_data[0].termsCondition
                }
                let option = {};
                let get_data = await DAO.populateData(Models.providers, query1, projection, option, populate)
                return  get_data[0];
            }
        }
    }

    catch (err) {
        throw err;
    }
}



const addCategoryId = async (ProviderData,payloadData) => {
    try {

        let update = {}
        if (payloadData.categoryId) { update.categoryId = payloadData.categoryId }
        let query = {_id : ProviderData._id }; 
        let option = { new : true};   
        let create_category_id = await DAO.findAndUpdate(Models.providers,query,update,option)
        return create_category_id

    }
    catch (err) {
        throw err;
    }

}

const data_to_save = async (data) => {
    try {

        let data_to_save = {}

        if (data.socialKey) { data_to_save.socialKey = data.socialKey }
        if (data.email) { data_to_save.email = data.email.toLowerCase() }
        if (data.countrycode) { data_to_save.countrycode = data.countrycode }
        if (data.phoneNumber) { data_to_save.phoneNumber = data.phoneNumber }
        if (data.profilePicture) { data_to_save.profilePicture = data.profilePicture }
        if (data.fullName) { data_to_save.fullName = data.fullName }
        if (data.password) { data_to_save.password = data.password }

        console.log("----------------------------data_to_save---", data_to_save)

        let create_provider = await DAO.saveData(Models.providers, data_to_save)
        return create_provider

    }
    catch (err) {
        throw err;
    }

}



const logout = async (ProviderData) => {
    try {

        let query = { _id: ProviderData._id }
        let update = { accessToken: null, isOnline: false }
        let options = { new: true }
        let data_to_update = await DAO.findAndUpdate(Models.providers, query, update, options)
        console.log(data_to_update)
        return {}

    }
    catch (err) {
       throw err;
    }

}


const forgotPassword = async (payloadData) => {
    try {

        let query = { email: payloadData.email.toLowerCase() }
        let projection = { __v: 0 }
        let option = { lean: true }
        let checkMail = await DAO.getData(Models.providers, query, projection, option);

        if (checkMail.length == 0) { throw ERROR.INVALID_EMAIL }
        else {

            let conditions = { _id: checkMail[0]._id }
            let update = { $set: { password: PASSWORD } }
            let options = { new: true }
            let data_to_update = await DAO.findAndUpdate(Models.providers, conditions, update, options)
            return data_to_update

        }
    }
    catch (err) {
        throw err;
    }
};


const verifyOtp = async (payloadData, userData) => {
    try {

        if (userData.otp != payloadData.otp) { throw ERROR.INVALID_OTP }
        else {

            let query = { _id: userData._id }
            let update = { otpverify: true }
            let options = { new: true }
            let data_to_update = await DAO.findAndUpdate(Models.providers, query, update, options)
            return data_to_update
        }

    }
    catch (err) {
        throw err;
    }

}

const reSendOtp = async (payloadData) => {
    try {

        let query = {
            countrycode: payloadData.countrycode,
            phoneNumber: payloadData.phoneNumber,
            isBlocked: false,
            isDeleted: false
        };

        let projection = { __v: 0 }
        let options = { lean: true }
        let fetch_data = await DAO.getData(Models.providers, query, projection, options)

        if (fetch_data.length == 0) { throw ERROR.NO_DATA_FOUND }
        else {

            let query = { _id: fetch_data[0]._id }
            let update = { otp: OTP }
            let options = { new: true };
            let updateOtp = await DAO.findAndUpdate(Models.providers, query, update, options);
            return updateOtp;

        }

    } catch (err) {
        throw err;
    }
};


const changePasword = async (payloadData, userData) => {
    try {

        let query = { _id: userData._id, password: payloadData.old_password };
        let update = { password: payloadData.New_password };
        let options = { new: true };
        let updatePassword = await DAO.findAndUpdate(Models.providers, query, update, options);
        if (updatePassword) {
            return updatePassword;
        }
        else {
            throw ERROR.OLD_PASSWORD_MISMATCH;
        }

    }
    catch (err) {
        throw err;
    }
}


const providerprofileUpdate = async (userData, payloadData) => {
    try {

        let query = { _id: userData._id };
        let options = { new: true };
        let setData = {};

        if (payloadData.fullName) { setData.fullName = payloadData.fullName }
        //####---check email 
        if (payloadData.email) {

            setData.email = payloadData.email.toLowerCase()

            email = payloadData.email.toLowerCase()

            let check_email = await commonController.check_provider_email(email)

            if (check_email.length) { throw ERROR.EMAIL_ALREADY_EXIST }
        }
        //####---check phoneNumber
        if (payloadData.countrycode != null && payloadData.phoneNumber != null) {

            setData.phoneNumber = payloadData.phoneNumber;
            setData.countrycode = payloadData.countrycode;

            countrycode = payloadData.countrycode;
            phoneNumber = payloadData.phoneNumber;
            let check_phone_no = await commonController.check_provider_phone_no(countrycode, phoneNumber)

            if (check_phone_no.length) { throw ERROR.PHONENO_ALREADY_EXIST }
        }

        if (payloadData.lng && payloadData.lat) {

            setData.location = {
                type: "Point",
                coordinates: [payloadData.lng, payloadData.lat]
            }

        }
        if (payloadData.profilePicture) {

            setData.profilePicture = {
                thumbnail: payloadData.profilePicture,
                original: payloadData.profilePicture
            }

        }
        if (payloadData.delete) { setData.isDeleted = payloadData.delete }

        if (payloadData.categoryId) { setData.categoryId = payloadData.categoryId }

        if (payloadData.address) { setData.address = payloadData.address }

        if(payloadData.stripeId) {setData.stripeId = payloadData.stripeId}
        let updateProfile = await DAO.findAndUpdate(Models.providers, query, setData, options)

        let populate = [
            {
                path: "categoryId",
                select: "_id name"
            }
        ];
        let query1 = { _id: updateProfile._id }
        let projection = {
            _id: 1,
            fullName: 1,
            email: 1,
            countrycode: 1,
            phoneNumber: 1,
            profilePicture: 1,
            accessToken: 1,
            documentUploaded: 1,
            categoryId: 1,
            location: 1,
            address: 1,
            stripeId : 1
        }
        let option = {};
        let get_data = await DAO.populateData(Models.providers, query1, projection, option, populate)
        return  get_data[0];

    }
    catch (err) {
        throw err;
    }
};



const documentUpdate = async (userData, payloadData) => {
    try {

        let query = { provider_id: userData._id };
        let options = { new: true };
        let setData = {};

        if (payloadData.addressExpiryDate) { setData = { "addproof.expireDate": payloadData.addressExpiryDate } }

        if (payloadData.addressProofNumber) { setData = { "addproof.number": payloadData.addressProofNumber } }

        if (payloadData.addressProofPicture) {
            setData = {
                addproof: {
                    thumbnail: payloadData.addressProofPicture,
                    original: payloadData.addressProofPicture
                }
            }
        }

        if (payloadData.identyExpiryDate) { setData = { "identicard.expireDate": payloadData.identyExpiryDate } }

        if (payloadData.identyNumber) { setData = { "identicard.number": payloadData.identyNumber } }

        if (payloadData.identyPicture) {
            setData = {
                identicard: {
                    thumbnail: payloadData.identyPicture,
                    original: payloadData.identyPicture
                }
            }
        }

        if (payloadData.delete) { setData.isDeleted = payloadData.delete }

        let updatedoc = await DAO.findAndUpdate(Models.providerDocs, query, { $set: setData }, options)
        return updatedoc;

    }
    catch (err) {
        throw err;
    }
};

const deviceDataUpdate = async (userData, payloadData) => {
    try {

        console.log("------------user_data----------------", payloadData);

        let query = { _id: userData._id };
        let options = { new: true };
        let setData = {};

        if (payloadData.deviceId) {
            setData.deviceToken = payloadData.deviceId
        }

        if (payloadData.deviceType) {
            setData.deviceType = payloadData.deviceType
        }
        let updateProfile = await DAO.findAndUpdate(Models.providers, query, setData, options)
        return updateProfile;

    }
    catch (err) {
        throw err;
    }
};


const OnlineOffline = async (userData, payloadData) => {
    try {

        let query = { _id: userData._id };
        let projections = {};
        let option = { lean: true };
        let fetch_data = await DAO.getData(Models.providers, query, projections, option);

        if (payloadData.online) {

            let setData = { isOnline: payloadData.online }
            let fetch_data = await DAO.findAndUpdate(Models.providers, { _id: userData._id }, setData, { new: true });

            return fetch_data;
        }
        return fetch_data;

    }
    catch (err) {
        throw err;
    }
};


const HomePage = async (ProviderData) => {
    try {

        //###--------get provide location----------
        let query1 = { _id: ProviderData._id };
        let projections = { _id: 0, address: 1, location: { coordinates: 1 }, categoryId: 1 };
        let option = { lean: true };
        let checkdata1 = await DAO.getData(Models.providers, query1, projections, option);
        if (checkdata1[0].address == null) { throw ERROR.NO_LOCATION }
        else {
            //###-------------get orders by near location---------
            let location = checkdata1[0].location.coordinates;
            let categoryId = checkdata1[0].categoryId;
            console.log("provider location", location, "category_id", categoryId)
            let fetch_data = await listService.list_services(location, categoryId);
            return fetch_data;
        }
        
    }
    catch (err) {
        throw err;
    }
};

const bookingManaged = async (providerData, payloadData) => {
    try {
    
        let providerid = providerData._id
        let orderId = payloadData.orderId;


        //## ---check provider id 
        let check_providerId = await commonController.check_provider_id(orderId, providerid);
        if (check_providerId.length != 0) {
            //###-----------Booking services--------
            let date = new Date(Date.now()).getTime();
            // ### ---- save status in logs    
            let condtion1 = { _id: payloadData.orderId };
            let option1 = { new: true };
            let update = { $push: { logs: { status: payloadData.status, date: date } } }
            let save_data = await DAO.findAndUpdate(Models.bookings, condtion1, update, option1)

            let setData = {
                providerId: providerid,
                logsStatus: payloadData.status
            }
             //### ------------get user name-------
             var userName = await commonController.get_provider_name(providerData._id)
            //### ---------get device token ---------
            var fetch_data = await commonController.get_user_device_token(save_data.user_id);
            let data = {}


            //-------------------- push when canceled service -------###
            if (payloadData.status == "Canceled" || payloadData.status == "Rejected") {
                setData.book = false;
                setData.status = "Pending";

                data = {
                    title:"Order Status Rejected",
                    message:  userName[0].fullName + " Rejected your Order ",
                    type: "Status",
                }
                if(save_data.paymentIntentKey){
                    //##---------------cancel payment ---------------##
                    let paymentIntent = await stripe.paymentIntents.cancel(save_data.paymentIntentKey);
                }
                if(fetch_data.length != 0){
               let push_notification = await commonController.push_notification_status(fetch_data[0].deviceToken, data)
                //###-------save notification ------
                let ids = {
                    recieverId: save_data.user_id,
                    senderId: providerData._id,
                }
                let pic = {}
                if(userName[0].profilePicture.original != undefined){
                    pic = userName[0].profilePicture.original
                }
                let save_notification = await commonController.save_notification(ids, data,pic);
                console.log("save notifucation ", save_notification);
            }
            }



             //-------------------- push when Reached  service -------###
            if (payloadData.status == "Reached" ) {
                console.log("status is :",payloadData.status )
                data = {
                    title:"Order Status Reached",
                    message: userName[0].fullName + " Reached your Order  ",
                    type: "Status",
                }
                  
                
                if(fetch_data.length != 0){
                 let push_notification = await commonController.push_notification_status(fetch_data[0].deviceToken, data)
                 //###-------save notification ------
                 let ids = {
                    recieverId: save_data.user_id,
                    senderId: providerData._id,
                 }
                 let pic = {}
                 if(userName[0].profilePicture.original != undefined){
                    pic = userName[0].profilePicture.original
                 }
                 let save_notification = await commonController.save_notification(ids, pic);
                 console.log("save notifucation ", save_notification);
            }
            

             //-------------------- push when start  service -------###
             if (payloadData.status == "Started" ) {
                console.log("status is :",payloadData.status )
                data = {
                    title:"Order Status Started",
                    message: userName[0].fullName + " Started your Order ",
                    type: "Status",
                }
                if(fetch_data.length != 0){
               let push_notification = await commonController.push_notification_status(fetch_data[0].deviceToken, data)
                //###-------save notification ------
                let ids = {
                    recieverId: save_data.user_id,
                    senderId: providerData._id,
                }
                let pic = {}
                if(userName[0].profilePicture.original != undefined){
                    pic = userName[0].profilePicture.original
                }
                let save_notification = await commonController.save_notification(ids, data,pic);
                console.log("save notifucation ", save_notification);
            }
            }
               


            //-------------------- push when Completed service -------###
            if (payloadData.status == "Completed") {
                setData.status = "Completed";

                console.log("status is :",payloadData.status )
                data = {
                    title: "Order Status Completed " ,
                    message: userName[0].fullName + " Completed Your Order ",
                    type: "Status",
                }
                //--------------Capture price -------------###
                console.log( "payment intent id : ",save_data.paymentIntentKey) 
                if(save_data.paymentIntentKey != null){


                if(fetch_data.length != 0){
               let push_notification = await commonController.push_notification_status(fetch_data[0].deviceToken, data)
                //###-------save notification ------
                let ids = {
                    recieverId: save_data.user_id,
                    senderId: providerData._id
                }
                let pic = {}
                if(userName[0].profilePicture.original != undefined){
                    pic = userName[0].profilePicture.original
                }
                let save_notification = await commonController.save_notification(ids, data,pic);
                console.log("save notifucation ", save_notification);
             }    

              let userId = null ;
              let  userfullName = null;
             if(fetch_data.length != 0){
                userId = fetch_data[0]._id
                userfullName =  fetch_data[0].fullName
                let push_notification = await commonController.push_notification_status(fetch_data[0].deviceToken, data)
                 //###-------save notification ------
                 let ids = {
                     recieverId: save_data.user_id,
                     senderId: providerData._id
                 }
                 let pic = {}
                 if(userName[0].profilePicture.original != undefined){
                     pic = userName[0].profilePicture.original
                 }
                 let save_notification = await commonController.save_notification(ids, data,pic);
                 console.log("save notifucation ", save_notification);
              }
                
                //----------------------capture payment -------------##
                if(save_data.paymentIntentKey){
                    let transationId = await confirmedPayment(save_data.paymentIntentKey)
                    console.log(transationId)
                    let save_tarnsationId = await commonController.save_tarnsationId(save_data._id,transationId)
                                    }
                 }
                
                return save_tarnsationId;
              }
               
            
            }

            let condition = { _id: payloadData.orderId };
            let option = { new: true };
            let createOrder = await DAO.findAndUpdate(Models.bookings, condition, setData, option);
            return createOrder;
        }
        else {
            console.log(payloadData)

            let date = new Date(Date.now()).getTime();
            // ### ---- save status in logs    
            let condtion1 = { _id: payloadData.orderId};

            let option1 = { new : true };
            let update = { $push: { logs: { status: payloadData.status, date: date } } }
            let save_data = await DAO.findAndUpdate(Models.bookings, condtion1, update, option1)

            console.log("booking data  ....",save_data.user_id)
            let setData = {
                providerId: providerid,
                logsStatus: payloadData.status
            }

             //### ------------get user name------- 
            var userName = await commonController.get_provider_name(providerData._id)
            //### ---------get device token ---------
            let fetch_data = await commonController.get_user_device_token(save_data.user_id);
            let user_data = await commonController.get_user(save_data.user_id)
            
            let data = {}

            //-------------------- push when Accepted service -------###
            if (payloadData.status == "Accepted") {
                setData.book = true;
                setData.status = "Confirmed";

                 data = {
                    title:"Order Status Accepted",
                    message:  userName[0].fullName + " Accepted your Order ",
                    type: "Status",
                }

                if(save_data.paymentIntentKey){
                let get_order_data = await getOrderData(payloadData.orderId)    
                let transationId =  await updatePayment(save_data.paymentIntentKey,payloadData.orderId,providerid,userName[0].fullName,save_data.user_id,user_data[0].fullName,get_order_data,userName[0].stripeId)
                }

                if(fetch_data.length != 0){
                let push_notification = await commonController.push_notification_status(fetch_data[0].deviceToken, data)
               

                //###-------save notification ------
                let ids = {
                    recieverId: save_data.user_id,
                    senderId: providerData._id,
                }
                let pic = {}
                if(userName[0].profilePicture.original != undefined){
                    pic = userName[0].profilePicture.original
                }
                let save_notification = await commonController.save_notification(ids, data,pic);
                console.log("save notifucation ", save_notification);
            }
            }
        

            //-------------------- push when Canceled service -------###
            if (payloadData.status == "Canceled" || payloadData.status == "Rejected") {
                setData.book = false;
                setData.status = "Pending";

                data = {
                    title: "Order status  Rejected ",
                    message:userName[0].fullName + " Rejected your Order ",
                    type: "Status",
                }
                if(save_data.paymentIntentKey){
                //##---------------cancel payment ---------------##
                let paymentIntent = await stripe.paymentIntents.cancel(save_data.paymentIntentKey);
                }
                if(fetch_data.length != 0){
               let push_notification = await commonController.push_notification_status(fetch_data[0].deviceToken, data)
               
                //###-------save notification ------
                let ids = {
                    recieverId: save_data.user_id,
                    senderId: providerData._id,
                }
                let pic = {}
                if(userName[0].profilePicture.original != undefined){
                    pic = userName[0].profilePicture.original
                }
                let save_notification = await commonController.save_notification(ids, data,pic);
                console.log("save notification ", save_notification);
            }
            }

            let condition = {
                _id: payloadData.orderId
            };
            let option = { new: true };
            let createOrder = await DAO.findAndUpdate(Models.bookings, condition, setData, option);
            return createOrder;
        }
    }
    catch (err) {
        throw err;
    }
};

const getOrderData = async (orderId) => {
    try {

      let productId = [];
      let productName = [];
      let price = [];
      let quentity = [];
      let products = await commonController.get_product_by_orderId(orderId);
      for (let i = 0; i < products.length; i++) {
       
         let get_product_name = await commonController.check_productId_1(products[i].productId) 
         productId.push(products[i].productId ) 
         productName.push(get_product_name.name )
         price.push(get_product_name.price )
         quentity.push(products[i].quentity )
      }
      return {
        productId : productId,
        productName : productName,
        price : price ,
        quentity : quentity
  
      }
    }
    catch (err) {
      throw err;
    }
  };

  const confirmedPayment = async (paymentIntentKey) => {
    try {
   
          let paymentIntent = await stripe.paymentIntents.confirm(
            paymentIntentKey,
            {payment_method: 'pm_card_visa'}
          );                
          console.log(" paument data is here ",paymentIntent)
          return  paymentIntents.charges.data[0].balance_transaction
      
    } catch (err) {
      throw err;
    }
  }


  
  const updatePayment = async (paymentIntentKey,orderId,providerid,providerName,userId,userName,get_order_data,stripeId) => {
    try {
        console.log(paymentIntentKey,orderId,providerid,providerName,userId,userName,get_order_data.productId,get_order_data.productName,get_order_data.price,get_order_data.quentity,stripeId)
           let paymentIntents =  await stripe.paymentIntents.update(
                      paymentIntentKey,
                      {description: 
                        " { username :"+  userName+
                        " },{ userId : " +userId+
                        "},{ orderId : " +orderId+
                        "},{ productid : " +get_order_data.productId+
                        "},{ productname :" + get_order_data.productName+
                        "},{ providername : " +providerName+
                        "},{ providerid : " +providerid+
                        "},{ stripeId : " +stripeId+
                        "},{ amount : " +get_order_data.price+
                        "},{quentity : " +get_order_data.quentity+ " } " 
                        
                    }
          );
          console.log(" paument data is here ",paymentIntents)
          return paymentIntents
      
    } catch (err) {
      throw err;
    }
  }



const capturePayment = async (paymentIntentKey) => {
    try {
   
           let paymentIntents = await stripe.paymentIntents.capture(paymentIntentKey);
                
            console.log(" paument data is here ",paymentIntents)
            return paymentIntents.charges.data[0].balance_transaction
      
    } catch (err) {
      throw err;
    }
  }

const OrderList = async (providerData) => {
    try {

        let providerId = providerData._id;
        let data = await providerOrderlisting.provider_list_order(providerId);
        return data;

    }
    catch (err) {
        throw err;
    }
};

const OrderDetails = async (providerData, payloadData) => {
    try {

        let providerId = providerData._id
        let orderId = payloadData.orderId;
        let fetch_data = await providerOderList.provider_order_sub_list(orderId, providerId);
        console.log(fetch_data)
        return fetch_data;
    }
    catch (err) {
        throw err;
    }
};


const uploadProofService = async (payloadData, providerData) => {
    try {

        let setData = {
            providerId: providerData._id,
            orderId: payloadData.orderId,
            image: payloadData.image
        }
        let save_data = await DAO.createData(Models.serviceProofs, setData)
        return save_data;
    }
    catch (err) {
        throw err;
    }
};

const giveRating = async (payloadData, providerData) => {
    try {   

        let data = {}

        //### ------------get user name-------
        var providerName = await commonController.get_provider_name(providerData._id)
        //### ---------get device token ---------
        let fetch_data = await commonController.get_user_device_token(payloadData.userId);
        let setData = {     
            providerId: providerData._id,
            userId: payloadData.userId,
            orderId: payloadData.orderId,
            rating: payloadData.rating
        }
              
        if (payloadData.message) {
             setData.message = payloadData.message;
             
             data = {
                title: "you got rating ",
                message: payloadData.message + " " +payloadData.rating +"is rated by "+providerName[0].fullName ,
                type: "Rating",
             }
        }
        else{  
            data = {
                title: "you got rating",
                message: payloadData.rating + " is rated by "+providerName[0].fullName ,
                type: "Rating",
            }
        }
        

        if(fetch_data.length != 0){
          let push_notification = await commonController.push_notification_status(fetch_data[0].deviceToken, data)

          //###-------save notification ------
          let ids = {
              recieverId: payloadData.userId,
              senderId: providerData._id,
          }
          let pic = {}
                if(providerName[0].profilePicture.original != undefined){
                    pic = providerName[0].profilePicture.original
                }
          let save_notification = await commonController.save_notification(ids, data,pic);
          console.log("save notifucation ", save_notification);
        }
        //### ---------save rating ---------
        let save_data = await DAO.createData(Models.userRatings, setData)
        return save_data;
    }
    catch (err) {
        throw err;
    }
};



const getProviderDetails = async (payloadData, providerData) => {
    try {
        //###------get provider personal details
        let query = { _id: payloadData.providerId };
        let projections = { __v: 0 };
        let option = { lean: true };
        var check_provider_details = await DAO.getData(Models.providers, query, projections, option);
        
        //###-------check porvider id 
        if (check_provider_details.length == 0) { throw ERROR.INVALID_PROVIDER_ID }
        else {

            //###------get provider document details
            let query = { provider_id: payloadData.providerId };
            let projections = { __v: 0 };
            let option = { lean: true };
            let check_provider_doc = await DAO.getData(Models.providerDocs, query, projections, option);
            if(check_provider_doc.length != 0 ){
                data ={
                    _id : check_provider_details[0]._id,
                    fullName : check_provider_details[0].fullName,
                    email : check_provider_details[0].email,
                    countrycode : check_provider_details[0].countrycode,
                    phoneNumber : check_provider_details[0].phoneNumber,
                    profilePicture : check_provider_details[0].profilePicture,
                    accessToken : check_provider_details[0].accessToken,
                    documentUploaded : check_provider_details[0].documentUploaded,
                    categoryId : check_provider_details[0].categoryId,
                    location : check_provider_details[0].location,
                    address : check_provider_details[0].address,
                    addressproof : check_provider_doc[0].addproof,
                    identicard : check_provider_doc[0].identicard
                 }
                console.log( "documents of provider",data)
            return {
                _id : check_provider_details[0]._id,
                fullName : check_provider_details[0].fullName,
                email : check_provider_details[0].email,
                countrycode : check_provider_details[0].countrycode,
                phoneNumber : check_provider_details[0].phoneNumber,
                profilePicture : check_provider_details[0].profilePicture,
                accessToken : check_provider_details[0].accessToken,
                documentUploaded : check_provider_details[0].documentUploaded,
                categoryId : check_provider_details[0].categoryId,
                location : check_provider_details[0].location,
                address : check_provider_details[0].address,
                addproof : check_provider_doc[0].addproof,
                identicard : check_provider_doc[0].identicard
             }
            }
            else{
                console.log("provider details",check_provider_details)
               return check_provider_details[0]
            }
                
        }
    }
    catch (err) {
        throw err;
    }
};


const sendMessages = async (payloadData, providerData) => {
    try {

        let query = { _id: payloadData.bookingId };
        let projections = { __v: 0 };
        let option = { lean: true };
        let check_user_Id = await DAO.getData(Models.bookings, query, projections, option);
        //###-------check user id 
        if (check_user_Id.length == 0) { throw ERROR.INVALID_BOOKING_ID }
        else {

            let data = {}

            //### ------------get user name-------
            var providerName = await commonController.get_provider_name(providerData._id)

             //### ---------get device token ---------
             let fetch_data = await commonController.get_user_device_token(check_user_Id[0].user_id);

             if(payloadData.message){
                 data = {
                    title:providerName[0].fullName ,
                    message: payloadData.message,
                    type: "Message",
                 }
              }
              if(payloadData.picture){
                data = {
                    title:providerName[0].fullName ,
                    message: payloadData.picture,
                    type: "Message",
                }
             }

             if(fetch_data.length != 0){
               let push_notification = await commonController.push_notification_status(fetch_data[0].deviceToken, data)
           

            //###-------save notification ------
            let ids = {
                recieverId: check_user_Id[0].user_id,
                senderId: providerData._id,
            }
            let pic = {}
                if(providerName[0].profilePicture.original != undefined){
                    pic = providerName[0].profilePicture.original
                }
            let save_notification = await commonController.save_notification(ids, data,pic);
            console.log("save notifucation ", save_notification);
            }
            //###--------save messages ---------
            let save_data = await commonController.save_messages_provider(payloadData, providerData._id)
            return save_data;
        }
    }
    catch (err) {
        throw err;
    }
};

const messagesListing = async (payloadData,providerData) => {
    try {

        let query = {bookingId : payloadData.bookingId  };
        let projections = { _id: 1, bookingId: 1, userId: 1, message: 1, picture: 1, time: 1 };
        let option = { lean: true };
        let send_messages = await DAO.getmessage(Models.messages, query, projections, option);

        return send_messages;

    }
    catch (err) {
        throw err;
    }
};

const getTermCondition = async () => {
    try {

        let policy_terms_data = await commonController.policy_terms_condtions();
        return {
            policy: policy_terms_data[0].policy,
            termsCondition: policy_terms_data[0].termsCondition
        }

    }
    catch (err) {
        throw err;
    }
};


const accessTokenLogin = async (providerData) => {
    try {

        let query = { _id :  providerData._id}
        let projection = {__v : 0,password : 0 }
        let option = {lean :  true }
        let get_data = await DAO.getdata(Models.providers,query,projection,option)
        return get_data

    }
    catch (err) {
        throw err;
    }
};


const notificationListing = async (providerData) => {
    try {
        let query = { recieverId: providerData._id };
        let projection = { __v: 0 }
        let option = { lean: true };
        let fetch_data = await DAO.getdata(Models.notifications, query, projection, option);
        return fetch_data;

    }
    catch (err) {
        throw err;
    }
};

const notificationDelete = async(userData,payloadData)=>{
    try{
     let query = { recieverId : userData._id};
     let update = {isDeleted : payloadData.isDeleted }
     let option = {new : true };    
     let deleteNotification = await DAO.findAndUpdate(Models.notifications,query,update,option );
     return deleteNotification;
         
   }  
   catch(err){
        throw err;
   }
  };


  /*
const addCard = async (payloadData, providerData) => {
    try {
  
      let createCustomer = await createCustomers(providerData._id, payloadData.sourceId);
  
  // if(createCustomer.sourceId == undefined){

      //  throw Error.INVALID_CARD_NUMBER

    //}
      console.log("...createCustomer......", createCustomer);
      let createSources = await createSource(createCustomer.customerId, createCustomer.sourceId);
  
      console.log("...createSources......", createSources);
  
      if (!(payloadData.type)) {
        payloadData.type = ""
      }
  
      let saveData = await DAO.saveData(Models.providerCards, {
        providerId: providerData._id,
        customerId: createCustomer.customerId,
        sourceId: createCustomer.sourceId,
        brand: createSources.brand,
        exp_month: createSources.exp_month,
        exp_year: createSources.exp_year,
        last4: createSources.last4,
        funding: createSources.funding,
        country: payloadData.country,
        isDeleted: false,
        type: payloadData.type
      })
  
      let user = await DAO.findAndUpdate(Models.providers, {
        _id: userData._id,
      }, {
        isActive: true

      }, { new : true });
  
  
      return  saveData;
  
    } catch (err) {
      throw err;
    }
  }
  
  
  const createCustomers = async (providerID, token) => {
    try {
      userDetails = await DAO.getData(Models.providers, { _id: providerID }, {}, { lean: true });
      return new Promise((resolve, reject) => {
        let create = stripe.customers.create({
          description: 'Customer for ' + providerID,
          source: token // obtained with Stripe.js
        }, function (err, customer) {
          if (err) {
            reject(err);
          } else {
            customerId = customer.id;
            sourceId = customer.default_source;
  
            resolve({ customerId: customerId, sourceId: sourceId })
          }
        });
      });
  
    } catch (err) {
      throw err;
    }
  }
  
  const createSource = async (customerId, sourceId) => {
  
    return new Promise((resolve, reject) => {
  
      stripe.customers.retrieveSource(
        customerId,
        sourceId,
        function (err, card) {
  
          console.log(".......err....card....createSource...", err, card);
          if (err) {
            reject(err)
          } else {
            resolve({ brand: card.brand, exp_month: card.exp_month, exp_year: card.exp_year, last4: card.last4, funding: card.funding })
          }
        }
      )
    });
  }
  


const listCard = async (payloadData,userData) => {
    try {
        let query = {
            userId: userData._id,
            isDeleted: false
          };
          if(payloadData._id){ query._id = payloadData._id}
          let options = {lean :true};
          let projections = {__v : 0};
      
          let listCard = await DAO.getData(Models.cards, query, projections, options);
          return listCard
  
    } catch (err) {
      throw err;
    }
  }
  
  const deleteCard = async (payloadData, userData) => {
    try {

        let query = { _id: payloadData._id, userId : userData._id };
        let options = { new: true };
        let setData = { }
        if(payloadData.isDeleted || payloadData.isDeleted == false){
          setData.isDeleted = payloadData.isDeleted
        }
        let deletSet = await DAO.findAndUpdate(Models.cards, query, setData, options);
        return null;
    } catch (err) {
      throw err;
    }
  }

*/

module.exports = {
    providerLogin: providerLogin,
    Login_gamil: Login_gamil,
    providerRegister: providerRegister,
    providerDocument: providerDocument,
    documentUpdate: documentUpdate,
    providerprofileUpdate: providerprofileUpdate,
    verifyOtp: verifyOtp,
    reSendOtp: reSendOtp,
    changePasword: changePasword,
    forgotPassword: forgotPassword,
    logout: logout,
    uploadProofService: uploadProofService,
    getTermCondition: getTermCondition,
    OnlineOffline: OnlineOffline,
    HomePage: HomePage,
    OrderDetails, OrderDetails,
    OrderList: OrderList,
    addCategoryId:addCategoryId,
    deviceDataUpdate: deviceDataUpdate,
    bookingManaged: bookingManaged,
    messagesListing: messagesListing,
    listCategories: listCategories,
    getProviderDetails: getProviderDetails,
    sendMessages: sendMessages,
    addAddress: addAddress,
    accessTokenLogin: accessTokenLogin,
    notificationDelete :notificationDelete,
    giveRating: giveRating,
    notificationListing: notificationListing,
    confirmedPayment : confirmedPayment,
    StripeListing : StripeListing,

    /////////////////
    // addCard :addCard,
    // listCard :listCard,
    // deleteCard :deleteCard,

};
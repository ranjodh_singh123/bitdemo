

const DAO = require("../DAOManager").queries;
const Models = require("../Models");
const TokenManager = require("../Libs/tokenManager");
const NotificationsManager = require("../Libs/NotificationsManager");



const OTP = 1234;

const save_tarnsationId = async(orderId,transationId) => {
      try {

            
         let condtion1 = { _id : orderId }; 
         let option1 = { new : true };
         let update = { transationId : transationId }
         let save_data = await DAO.findAndUpdate(Models.bookings, condtion1, update, option1)
         return save_data

      }
      catch(err) {
            throw err;
      }
}

         

const save_intent_key = async(bookingId,key) => {
      try {

            let query = { _id : bookingId,isDeleted : false}
            let update = { paymentIntentKey : key }
            let options = { new : true }
            let fetch_data = await DAO.findAndUpdate(Models.bookings, query, update, options)
            console.log(fetch_data)
            return fetch_data

      }
      catch(err) {
            throw err;
      }
}

const check_provider_email = async(email) => {
      try {

            let query = { email : email , isDeleted : false }
            let projection = { _id : 1 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.providers, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}

const get_provider_name = async(id) => {
      try {

            let query = { _id : id , isDeleted : false,isBlocked :  false }
            let projection = {__v :0 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.providers, query, projection, options)
            return fetch_data

      }
      catch(err) {
            throw err;
      }
}
const get_user_name = async(id) => {
      try {

            let query = { _id : id , isDeleted : false,isBlocked :  false }
            let projection = { __v :0}
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.users, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}

const check_provider_phone_no = async(country_code, phoneNumber) => {
      try {

            let query = { 
                  countrycode : country_code, 
                  phoneNumber : phoneNumber, 
                  isDeleted : false 
            }
            let projection = { _id : 1 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.providers, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}

const save_provider_data = async(data) => {

      try {

            let data_to_save = {
                  fullName : data.fullName,
                  email : data.email.toLowerCase(),
                  password : data.password,
                  countrycode : data.countrycode,
                  phoneNumber : data.phoneNumber,
                  categoryId : data.categoryId,        
                  otp : OTP     
            }
            if(data.profilePicture) {
                  data_to_save.profilePicture = {
                        original : data.profilePicture,
                        thumbnail : data.profilePicture
                  }
            }

            let create_provider = await DAO.saveData(Models.providers, data_to_save)
            return create_provider

      }
      catch(err) {
            throw err;
      }

}


const generate_token = async(token_info) => {

      try {

              let token_data = {
                    _id : token_info._id,
                    scope : token_info.scope
              }
              let gen_token = await TokenManager.generateToken(token_data, token_info.scope)

              if(gen_token == null) { throw ERROR.SOMETHING_WENT_WRONG }
              else {

                    // update token in db
                    let query = { _id : token_info._id }
                    let update = { 
                          accessToken : gen_token ,
                          isOnline : true
                        }
                    if(token_info.deviceToken){ update.deviceToken = token_info.deviceToken}
                    if(token_info.deviceType){update.deviceType  = token_info.deviceType}    
                    let options = { new : true }
                    let data_to_update = await DAO.findAndUpdate(token_info.collection, query, update, options)

                    return data_to_update

              }
      }
      catch(err) {
            throw err;
      }

}


const check_user_email = async(email) => {
      try {

            let query = { email : email , isDeleted : false }
            let projection = { _id : 1 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.users, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}


const check_user_phone_no = async(country_code, phoneNumber) => {
      try {

            let query = { 
                  countrycode : country_code, 
                  phoneNumber : phoneNumber, 
                  isDeleted : false 
            }
            let projection = { _id : 1 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.users, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}

const save_user_data = async(data) => {
      try {

            let data_to_save = {
                  fullName : data.fullName,
                  email : data.email.toLowerCase(),
                  password : data.password,
                  countrycode : data.countrycode,
                  phoneNumber : data.phoneNumber,
                  otp : OTP     
            }
            if(data.profilePicture) {
                  data_to_save.profilePicture = {
                        original : data.profilePicture,
                        thumbnail : data.profilePicture
                  }
            }

            let create_provider = await DAO.saveData(Models.users, data_to_save)
            return create_provider

      }
      catch(err) {
            throw err;
      }
}



const update_status_user_service = async(orderId,booked) => {
      try {

            let query = {_id : orderId};
            let option = { new : true };
            let update ={};
            //###-------case1 -----
             if(booked == true){
                  update = {$set : {status : "Confirmed"}};
             }
            //###-------case2 -------  
             if(booked == false){
                   update = {$set : {status : "Pending"}};
             }
             
            let update_status = await DAO.findAndUpdate(Models.bookindDeatails,query,update,option)
            return update_status

      }
      catch(err) {
            throw err;
      }
}

const check_provider_id = async(orderId,providerid) => {
      try {

            let query = { 
                  _id :  orderId ,
                  providerId : providerid,
                  isDeleted : false
            }
            let projection = {  }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.bookings, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}

const checkbooking = async(userid) => {
      try {

            let query = { 
                  user_id :  userid,
                  isDeleted : false 
            }
            let projection = {  }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.bookindDeatails, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}

const check_booking_id = async(orderId) => {
      try {

            let query = { 
                  _id :  orderId,
                  isDeleted : false 
            }
            let projection = {  }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.bookindDeatails, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}

const check_category_id = async(categoryId) => {
      try {

            let query = { 
                  _id : categoryId, 
            }
            let projection = { _id : 1 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.categories, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}


const check_document = async(providerId) => {
      try {

            let query = { 
                  _id : providerId,
                  documentUploaded : true 
            }
            let projection = { }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.providers, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}

const check_category = async(category) => {
      try {

            let query = { 
                  name: category, 
            }
            let projection = { _id : 1 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.categories, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}



const check_sub_category = async(subCategory) => {
      try {

            let query = { 
                  name : subCategory, 
            }
            let projection = { _id : 1 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.subCategories, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}


const check_product = async(product) => {
      try {

            let query = { 
                  name : product, 
            }
            let projection = { _id : 1 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.products, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}

const get_product_by_orderId = async(orderId) => {
      try {

            let query = { 
                  orderId : orderId, 
            }
            let projection = { __V : 0 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.bookingDetails, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}
const check_sub_category_id = async(subCategoryId) => {
      try {

            let query = { 
                  _id : subCategoryId, 
            }
            let projection = { _id : 1 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.subCategories, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}

const check_productId = async(productId) => {
      try {

            let query = {  _id : productId}
            let projection = { __v : 0 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.products, query, projection, options)

            return fetch_data ;

      }
      catch(err) {
            throw err;
      }
}

const check_productId_1 = async(productId) => {
      try {

            let query = {  _id : productId}
            let projection = { __v : 0 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.products, query, projection, options)

            return {
                  price : fetch_data[0].price,
                  name :fetch_data[0].name 
            }

      }
      catch(err) {
            throw err;
      }
}

const user_approval = async(userId) => {
      try {

            let query = { 
                  _id : userId,
                  isApproval : true 
            }
            let projection = { _id : 1 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.users, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}

const provider_approval = async(providerId) => {
      try {

            let query = { 
                  _id : providerId,
                  isApproval : true 
            }
            let projection = { _id : 1 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.providers, query, projection, options)

            return fetch_data

      }
      catch(err) {
            throw err;
      }
}

const policy_terms_condtions = async() => {
      try {

            let query = { 
                  isDeleted : false 
            }
            let projection = { _id : 0, policy : 1,termsCondition : 1 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.policyTermConditions, query, projection, options)
            return fetch_data
      }
      catch(err) {
            throw err;
      }
}

const get_user_device_token = async(userId) => {

      try { 
            
            let query = {_id : userId ,isDeleted : false ,deviceToken :{$ne : null}}
            let projection = {__V : 0 }
            let options = { lean : true }
            let fetch_token = await DAO.getData(Models.users, query, projection, options)
            return fetch_token;

      }
      catch(err) {
            throw err;
      }
}

const get_user = async(userId) => {

      try { 
            
            let query = {_id : userId ,isDeleted : false }
            let projection = {__V : 0 }
            let options = { lean : true }
            let fetch_token = await DAO.getData(Models.users, query, projection, options)
            return fetch_token;

      }
      catch(err) {
            throw err;
      }
}


const get_provider_device_token = async(providerId) => {

      try { 
            
            let query = {_id : providerId ,isDeleted : false ,deviceToken :{$ne : null} }
            let projection = {_id : 1, deviceToken : 1 }
            let options = { lean : true }
            let fetch_token = await DAO.getData(Models.providers, query, projection, options)
            return token = fetch_token;

      }
      catch(err) {
            throw err;
      }
}
const save_notification = async(ids,data,pic) => {
      try { //###----------current time -------
            time =  new  Date(Date.now()).getTime()
            let setData = { 
                    time : time,
                    recieverId : ids.recieverId,
                    senderId : ids.senderId,
                    message : data.message,
                    title : data.title,
                    type : data.type
                }
                if(pic){
                      setData.pic = pic
                }
            let fetch_data = await DAO.createData(Models.notifications,setData)
            return fetch_data

      }
      catch(err) {
            throw err;
      }
}

const save_messages_user = async(data,userid) => {
      try {
            let save_data = {
                  bookingId   : data.bookingId,
                  userId :  userid,
                  time : new Date(Date.now()).getTime()
                }

         if(data.message){ save_data.message = data.message }
         if(data.picture){ save_data.picture = data.picture }
                                console.log(save_data);
            let create_message = await DAO.saveData(Models.messages, save_data)
            return create_message

      }
      catch(err) {
            throw err;
      }
}

const save_messages_provider = async(data,providerid) => {
      try {
            let save_data = {
                  userId : providerid,
                  bookingId : data.bookingId,
                  time : new Date(Date.now()).getTime()
                }

         if(data.message){ save_data.message = data.message }
         if(data.picture){ save_data.picture = data.picture}

            let create_message = await DAO.saveData(Models.messages, save_data)
            return create_message

      }
      catch(err) {
            throw err;
      }
}


const push_notification_status = async(deviceToken,data) => {
      try {

             //###-----push notification -------
             let push_notification = await NotificationsManager.sendSingleNotification(deviceToken, data);
             console.log("push notification ", push_notification);

      }
      catch(err) {
            throw err;
      }
}


const check_user = async(userid) => {
      try {
    
            let query = { _id : userid , isDeleted : false }
            let projection = {__V : 0 }
            let options = { lean : true }
            let fetch_data = await DAO.getData(Models.users, query, projection, options)
    
            return fetch_data
    
      }
      catch(err) {
            throw err;
      }
    }


    const get_card_info = async(cardId) => {
      try { 
            
        let query = { isDeleted: false, _id: cardId};
        let options = {lean : true};
        let projections = {__V : 0};
        let listCard = await DAO.getData(Models.cards, query, projections, options);
        return listCard
      }
      catch(err) {
            throw err;
      }
}

module.exports = {

      check_provider_email : check_provider_email,
      check_provider_phone_no : check_provider_phone_no,
      save_provider_data : save_provider_data,
      generate_token : generate_token,
      check_document:check_document,
      check_user_email : check_user_email,
      check_user_phone_no : check_user_phone_no,
      save_user_data : save_user_data,
      check_provider_id:check_provider_id,
      check_booking_id:check_booking_id,
      update_status_user_service:update_status_user_service,
      check_category_id:check_category_id,
      check_category:check_category,
      check_sub_category:check_sub_category,
      check_sub_category_id:check_sub_category_id,
      check_product:check_product,
      check_productId:check_productId,
      save_messages_user:save_messages_user,
      checkbooking:checkbooking,
      push_notification_status :push_notification_status,
      user_approval:user_approval,
      provider_approval:provider_approval,
      save_notification:save_notification,
      get_provider_device_token :get_provider_device_token,
      get_user_device_token:get_user_device_token,
      save_messages_provider:save_messages_provider,
      policy_terms_condtions:policy_terms_condtions,
      get_provider_name :get_provider_name,
      get_user_name :get_user_name,
      get_user :get_user,
      save_intent_key :save_intent_key,
      save_tarnsationId : save_tarnsationId,
      check_user :check_user,
      get_product_by_orderId :get_product_by_orderId,
      check_productId_1 : check_productId_1,
      get_card_info : get_card_info,
      

}
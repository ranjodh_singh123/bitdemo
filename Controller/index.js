/**
 * Created by Prince 
 */
module.exports = {
  
    adminController : require('./adminController'),
    providerController : require('./providerController') ,
    userController : require('./userController'),  
    commonController : require('./commonController')
};
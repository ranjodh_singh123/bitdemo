
var Controller = require('../Controller');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../Config');
const { min } = require('underscore');
var SUCCESS = Config.responseMessages.SUCCESS;
var ERROR = Config.responseMessages.ERROR;

//USER REGESTRATION API

module.exports = [
  {
    method: "GET",
    path: "/Provider/stripeListing",
    options: {
      description: "Provider stripe listing",
      auth: false,
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.providerController.StripeListing(request.query)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
            type :Joi.string().optional().valid("LIVE","TEST")
        },
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'POST',
    path: '/Provider/SingUp',
    options: {
      description: 'Provider Singup',
      auth: false,
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.providerRegister(request.payload)
        .then(response => {
          return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
        })
        .catch(error => {
          winston.error("=====error=============", error);
          return UniversalFunctions.sendError("en", error, reply);
        });
    },
    validate: {
      payload: {
          profilePicture:Joi.string().optional().description("choose profile picture"),
          fullName:Joi.string().required().description("Enter full name"),
          phoneNumber:Joi.number().integer().required().description("Enter your phone number"),
          countrycode:Joi.string().required(),
          email: Joi.string().email().required().description("Enter your Email Here"),
          password: Joi.string().required().description("Enter your password"),
          categoryId:Joi.string().required().description("Enter your category"),
          deviceType: Joi.string().optional().valid('WEB','ANDROID', 'IPHONE'),
          deviceId: Joi.string().optional(),
        },

        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'POST',
    path: '/Provider/DocumentsUpload',
    options: {
      description: 'Provider document upload ',
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.providerDocument(request.payload,request.auth.credentials)
        .then(response => {
          return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
        })
        .catch(error => {
          winston.error("=====error=============", error);
          return UniversalFunctions.sendError("en", error, reply);
        });
    },
    validate: {
      payload: {      
          addressProofPicture:Joi.string().required().description("Enter address proof picture"),
          addressProofNumber:Joi.string().required().description("enter address proof  number"),
          addressExpiryDate:Joi.date().required().description("Enter address proof expiry date"),
          identyPicture:Joi.string().required().description("Enter identy picture"),
          identyNumber:Joi.string().required().description("Enter identy number"),
          identyExpiryDate:Joi.date().required().description("Enter identy expiry date"),
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  //provider LOGIN API

  {
    method: 'GET',
    path: '/Provider/Login',
    options: {
      description: 'Provider Login Api',
      auth: false,
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.providerLogin(request.query)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
          email:Joi.string().required().description("Enter your email "),
          password: Joi.string().required().description("Enter your Password here"),
          deviceType: Joi.string().optional().valid('ANDROID', 'IPHONE','WEB'),
          deviceId: Joi.string().optional()
        },
        // headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'POST',
    path: '/Provider/LoginGmail',
    options: {
      description: 'Provider Login Api',
      auth: false,
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.Login_gamil(request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          
              socialKey : Joi.string().required().description("Enter social key"),
              profilePicture : Joi.string().optional(), 
              fullName : Joi.string().optional(),
              email : Joi.string().email().optional(),
              countrycode : Joi.string().optional(),
              phoneNumber : Joi.number().optional(),
              password : Joi.string().optional(),
              deviceType: Joi.string().optional().valid('ANDROID', 'IPHONE','WEB'),
              deviceId: Joi.string().optional()

        },
        // headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/Provider/FogotPassword',
    options: {
      description: 'Provider forgot password Api',
      auth: false,
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.forgotPassword(request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
        email: Joi.string().required().description("Enter your email address"),
        },
        // headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/Provider/VerifyOtp',
    options: {
      description: 'Provider verify otp Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.verifyOtp(request.payload, request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
            otp : Joi.number().integer().required()
        },
         headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/Provider/ReSendOtp',
    options: {
      description: 'Provider re send otp Api',
      auth: false,
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.reSendOtp(request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          countrycode : Joi.string().required(),
          phoneNumber : Joi.number().required(),
        },
        //  headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/Provider/ChangePassword',
    options: {
      description: 'Provider new password Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.changePasword(request.payload, request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
         old_password:Joi.string().required().description("Enter your old password here"), 
         New_password: Joi.string().required().description("Enter your password here ")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/Provider/ProfileUpdate',
    options: {
      description: 'Provider profile Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.providerprofileUpdate(request.auth.credentials,request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          profilePicture:Joi.string().optional().description("choose profile picture"),
          fullName:Joi.string().optional().description("Enter full name"),
          phoneNumber:Joi.number().integer().optional().description("Enter your phone number"),
          countrycode:Joi.string().optional(),
          email: Joi.string().email().optional().description("Enter your Email Here"),
          address:Joi.string().optional().description("Enter address"),
          categoryId:Joi.string().optional().description("Enter your categoryId"),
          lat:Joi.string().optional().description("Enter latitude"),
          lng:Joi.string().optional().description("Enter logntitude"),
          delete:Joi.bool().optional().valid("false","true"),
          stripeId : Joi.string().optional()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/Provider/selectCategoryId',
    options: {
      description: 'Provider add category id',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.addCategoryId(request.auth.credentials,request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          categoryId:Joi.string().required().description("enter category id"),
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/Provider/UpdateDocument',
    options: {
      description: 'Provider document Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.documentUpdate(request.auth.credentials,request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          addressProofPicture:Joi.string().optional().description("enter address proof picture"),
          addressProofNumber:Joi.number().integer().optional().description("enter address proof  number"),
          addressExpiryDate:Joi.date().optional().description("Enter address proof expiry date"),
          identyPicture:Joi.string().optional().description("enter identy picture"),
          identyNumber:Joi.number().integer().optional().description("enter identy number"),
          identyExpiryDate:Joi.date().optional().description("Enter identy expiry date"),
          delete:Joi.bool().optional().valid("false","true")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/Provider/DeviceIdAndTypeUpdate',
    options: {
      description: 'Provider update device id and type  Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.deviceDataUpdate(request.auth.credentials,request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          deviceType: Joi.string().optional().valid('WEB','ANDROID', 'IPHONE'),
          deviceId: Joi.string().optional()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/Provider/logout',
    options: {
      description: 'Provider logout Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.logout(request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/Provider/OnlineOffline',
    options: {
      description: 'Provider OnlineOffline Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.OnlineOffline(request.auth.credentials,request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          online:Joi.bool().optional().valid("false","true"),
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/Provider/HomePage',
    options: {
      description: 'Provider home api Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.HomePage(request.auth.credentials,request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
      
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/Provider/BookingManaged',
    options: {
      description: 'Provider booking managed Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.bookingManaged(request.auth.credentials,request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          orderId:Joi.string().required().description("Enter order id"),
          status:Joi.string().required().valid("Accepted","Rejected","Reached","Started","Canceled","Completed")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  
  {
    method: 'GET',
    path: '/Provider/OrderList',
    options: {
      description: 'Provider orderList Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.OrderList(request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
  
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/Provider/OrderDetails',
    options: {
      description: 'Provider order list Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.OrderDetails(request.auth.credentials,request.query)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
          orderId:Joi.string().required().description("Enter order id")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'POST',
    path: '/Provider/SendMessages',
    options: {
      description: 'Provider send messages to user Api',
      auth:{ strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.sendMessages(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          bookingId:Joi.string().required().description("Enter booking id"),
          message:Joi.string().optional(),
          picture:Joi.string().optional()
        
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/Provider/MessagesListing',
    options: {
      description: ' messages listing Api',
      auth:{ strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.messagesListing(request.query,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query :{

          bookingId:Joi.string().required().description("Enter booking id"),
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/Provider/ListCategories',
    options: {
      description: 'Provider list Categories Api',
      auth:false,
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.listCategories()
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            // winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },

  {
    method: 'PUT',
    path: '/Provider/AddAddress',
    options: {
      description: 'Provider addAddress',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.addAddress(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload : { 
          lat : Joi.number().required(),
          lng : Joi.number().required(),
          address : Joi.string().required()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'POST',
    path: '/Provider/UploadServiceProof',
    options: {
      description: 'Provider upload image  Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.uploadProofService(request.auth.credentials,request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          orderId:Joi.string().required().description("Enter order id"),
          image:Joi.string().required(),
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/Provider/GiveRating',
    options: {
      description: 'Provider give rating to customer',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.giveRating(request.payload, request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload : { 
          userId:Joi.string().required().description("Enter user id"),
          orderId:Joi.string().required().description("Enter order id"),
          rating:Joi.string().required(),
          message:Joi.string().optional()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/Provider/GetProviderDetails',
    options: {
      description: 'get provider details ',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.getProviderDetails(request.query, request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query : { 
          providerId:Joi.string().required().description("Enter provider id"),
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "GET",
    path: "/Provider/PolicyTermsCondtions",
    options: {
      description: "get privacy policy  and term conditions Api",
      auth: false,
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.providerController.getTermCondition()
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/Provider/AccessTokenLogin',
    options: {
      description: 'Provider access token login Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.accessTokenLogin(request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
  
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/Provider/NotificationListing',
    options: {
      description: 'Provider notification listing Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.notificationListing(request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
  
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/Provider/deleteNotification',
    options: {
      description: 'provider notification delete Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.providerController.notificationDelete(request.auth.credentials,request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload:{
             isDeleted:Joi.boolean().required()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  /*
  {
    method: "POST",
    path: "/Provider/addCard",
    options: {
      description: "Provider add cards ",
      auth: {
        strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER]
      },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.providerController.addCard(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en",SUCCESS.DEFAULT,response,reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload:{
          sourceId: Joi.string().required(),
          country:Joi.string().required(),
          exp_month:Joi.string().required(),
          exp_year:Joi.string().required(),
          last4:Joi.string().required() ,
          type:Joi.string().optional()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "POST",
    path: "/Provider/listCard",
    options: {
      description: "listing cards ",
      auth: {
        strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER]
      },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.providerController.listCard(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en",SUCCESS.DEFAULT,response,reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload:{
          _id:Joi.string().optional()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "POST",
    path: "/Provider/deleteCard",
    options: {
      description: "add delete cards ",
      auth: {
        strategies: [Config.APP_CONSTANTS.SCOPE.PROVIDER]
      },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.providerController.deleteCard(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en",SUCCESS.DEFAULT,response,reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload:{
          _id:Joi.string().required(),
          isDeleted:Joi.boolean().optional(),
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },*/
];
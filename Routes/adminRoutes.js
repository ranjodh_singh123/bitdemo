var Controller = require("../Controller");
var UniversalFunctions = require("../Utils/UniversalFunctions");
var Joi = require("joi");
var Config = require("../Config");
var SUCCESS = Config.responseMessages.SUCCESS;
var ERROR = Config.responseMessages.ERROR;

module.exports = [
  {
    method: "POST",
    path: "/Admin/Login",
    options: {
      description: "Admin Login Api",
      auth: false,
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.adminLogin(request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          email: Joi.string().email().required().description("Enter your Email Address"),
          password: Joi.string().required().description("Enter your Password")
        },

        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "PUT",
    path: "/Admin/ForgotPassword",
    options: {
      description: "Admin Login Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.changePassword(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          New_password: Joi.string().optional().description("Enter New password here ")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "POST",
    path: "/Admin/AddEditCategory",
    options: {
      description: "Admin add edit category Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.addEditCategory(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          categoryId:Joi.string().optional().description("Enter category id"),
          category:Joi.string().optional().description("Enter category "),
          icon :Joi.string().optional().description("Enter category icon")
        },

        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "PUT",
    path: "/Admin/DeleteBlockedCategory",
    options: {
      description: "Admin delete block category Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.deleteBlockCategory(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          categoryId:Joi.string().required().description("Enter category id"),
          deleted:Joi.bool().optional().valid("false","true"),
          blocked:Joi.bool().optional().valid("false","true"),
        },

        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "GET",
    path: "/Admin/CategoryList",
    options: {
      description: "Admin category listing Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.listCategory(request.query,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
          categoryId:Joi.string().optional().description("Enter category id"),
          pageNumber:Joi.number().integer().optional().description("Enter skip numbers")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "POST",
    path: "/Admin/AddEditSubcategory",
    options: {
      description: "Admin add edit category Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.addEditSubCategory(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
        subCategoryId:Joi.string().optional().description("Enter sub Category_id"),
        categoryId:Joi.string().optional().description("Enter category id"),
        subCategory:Joi.string().optional().description("Enter sub category ")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "GET",
    path: "/Admin/SubCategoryList",
    options: {
      description: "Admin category listing Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.listSubCategory(request.query,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
          subCategoryId:Joi.string().optional().description("Enter subcategory id"),
          pageNumber:Joi.number().integer().optional().description("Enter skip numbers")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  
  {
    method: "PUT",
    path: "/Admin/DeleteBlockedSubCategory",
    options: {
      description: "Admin delete block sub category Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.deleteBlockSubCategory(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          subCategoryId:Joi.string().required().description("Enter subcategory id"),
          deleted:Joi.bool().optional().valid("false","true"),
          blocked:Joi.bool().optional().valid("false","true"),
        },

        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  
  {
    method: "POST",
    path: "/Admin/AddEditProducts",
    options: {
      description: "Admin add edit products Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.addEditProducts(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          productId:Joi.string().optional().description("Enter product id"),
          subCategoryId:Joi.string().optional().description("Enter sub Category_id"),
          categoryId:Joi.string().optional().description("Enter category id"),
          product:Joi.string().optional().description("Enter product name "),
          price:Joi.number().integer().optional().description("Enter product price"),
          productPicture:Joi.string().optional().description("Enter product picture"),
          description:Joi.string().optional().description("Enter product discription")
        },

        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "GET",
    path: "/Admin/ProductList",
    options: {
      description: "Admin product list Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.listProduct(request.query,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
          productId:Joi.string().optional().description("Enter product id"),
          pageNumber:Joi.number().integer().optional().description("Enter skip numbers")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "PUT",
    path: "/Admin/DeleteBlockedProduct",
    options: {
      description: "Admin delete block product Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.deleteBlockProduct(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          productId:Joi.string().required().description("Enter product id"),
          deleted:Joi.bool().optional().valid("false","true"),
          blocked:Joi.bool().optional().valid("false","true"),
        },

        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  
  {
    method: "GET",
    path: "/Admin/CategorySubCategoryProductList",
    options: {
      description: "Admin product list Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.categoriesList(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "GET",
    path: "/Admin/UserListing",
    options: {
      description: "Admin user list Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.listUser(request.query,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
           userId:Joi.string().optional().description("Enter user id"),
           pageNumber:Joi.number().optional().description("Enter skip numbers")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "GET",
    path: "/Admin/ProviderListing",
    options: {
      description: "Admin Provider list Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.listProvider(request.query,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
          providerId:Joi.string().optional().description("Enter provider id"),
          pageNumber:Joi.number().integer().optional().description("Enter skip numbers")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "PUT",
    path: "/Admin/DeleteBlockUser",
    options: {
      description: "Admin edit user Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.deleteBlockUser(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          userId:Joi.string().required().description("Enter user id"),
          deleted:Joi.bool().optional().valid("false","true"),
          blocked:Joi.bool().optional().valid("false","true")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "PUT",
    path: "/Admin/DeleteBlockProvider",
    options: {
      description: "Admin edit user Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.deleteBlockProvider(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          providerId:Joi.string().required().description("Enter provider id"),
          deleted:Joi.bool().optional().valid("false","true"),
          blocked:Joi.bool().optional().valid("false","true")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "PUT",
    path: "/Admin/ProviderApproval",
    options: {
      description: "Admin approval to provider Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.providerApproval(request.auth.credentials,request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          providerId:Joi.string().required().description("Enter provider id"),
          approve :Joi.bool().required().valid("true","false")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "GET",
    path: "/Admin/BookingList",
    options: {
      description: "Admin booking list Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.bookingList(request.auth.credentials,request.query)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
          startDate:Joi.date().required().description("YYY-MM-DD"),
          endDate:Joi.date().required().description("YYY-MM-DD")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  
  {
    method: "PUT",
    path: "/Admin/managedBooking",
    options: {
      description: "Admin booking list Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.bookingManaged(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          bookingId:Joi.string().required(),
          status :Joi.string().valid("Canceled").required()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "POST",
    path: "/Admin/CreateDateTime",
    options: {
      description: "Admin booking date and time Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.createDateTime(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
           _id : Joi.string().optional(),
           time:Joi.array().required().description("[{'days':'','startTime':minutes,'endTime':mintues}]")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "GET",
    path: "/Admin/dateTimeListing",
    options: {
      description: "Admin booking date and time Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.dateTimeListing(request.query,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
          _id : Joi.string().optional(),
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "GET",
    path: "/Admin/policyTermsListing",
    options: {
      description: "Admin policy terms Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.policyTermListing(request.query,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
          _id : Joi.string().optional(),
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "POST",
    path: "/Admin/PolicyAndTermsCondtions",
    options: {
      description: "Admin add privacy policy  and term conditions Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.createPolicyAndTerm(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          policyId :Joi.string().optional().description("policy id"),
          policy:Joi.string().optional().description("add policy"),
          terms:Joi.string().optional().description("add terms"),
          isDeleted : Joi.boolean()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "POST",
    path: "/Admin/UploadImages",
    options: {
      description: "Admin upload image  Api",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN] },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.imageUpload(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          image : Joi.string().optional().description("image upload")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "POST",
    path: "/Admin/addEditStripeCredentials",
    options: {
      description: "strip data  add edit",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN]},
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.addEditStrip(request.auth.credentials,request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
           id:Joi.string().optional(),
           publishableKey:Joi.string().optional(),
           secretKey:Joi.string().optional(),
           mode:Joi.string().optional().valid("TEST","LIVE")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  
  {
    method: "DELETE",
    path: "/Admin/deleteStripCredentails",
    options: {
      description: "delete   stripe data ",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN]},
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.deleteStripCredentails(request.auth.credentials,request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
            id:Joi.string().required(),
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "GET",
    path: "/Admin/stripeDetails",
    options: {
      description: "stripe listing",
      auth:  { strategies: [Config.APP_CONSTANTS.SCOPE.ADMIN]},
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.adminController.StripeListing(request.auth.credentials,request.query)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
            id:Joi.string().optional()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
];

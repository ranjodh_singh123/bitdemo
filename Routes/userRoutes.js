var Controller = require('../Controller');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');
var Config = require('../Config');
const { min } = require('underscore');
var SUCCESS = Config.responseMessages.SUCCESS;
var ERROR = Config.responseMessages.ERROR;

//USER REGESTRATION API

module.exports = [
  {
    method: "GET",
    path: "/User/stripeListing",
    options: {
      description: "User stripe listing",
      auth: false,
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.userController.StripeListing(request.query)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
            type :Joi.string().optional().valid("LIVE","TEST")
        },
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },

  {
    method: 'POST',
    path: '/User/imageUpload',
    options: {
      description: 'User image upload  Api',
      auth: false,
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.imageUpload(request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess(SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError(error, reply);
          });
      },
      validate: {
        payload: {
                image : Joi.any().meta({ swaggerType : 'file' }).required().description('Image File'),
    
        },
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'POST',                      
    path: '/User/SingUp',                
    options: {                           
      description: 'User SingUp',        
      auth: false,                      
      tags: ['api'],                   
      handler: (request, reply) => {     
        return Controller.userController.userRegister(request.payload)  
          .then(response => {            
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply); 
          })
          .catch(error => {           
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply); 
          });
      },
      validate: {                       
        payload: {                     
          profilePicture:Joi.string().optional().description("Enter your pofile picture"), 
          fullName:Joi.string().required().description("Enter full name"),                
          phoneNumber:Joi.number().integer().positive().required().description("Enter your phone number"), 
          countrycode:Joi.string().required(),                       
          email: Joi.string().email().required().description("Enter your Email Here"),    
          password: Joi.string().required().description("Enter your password"),
          deviceType: Joi.string().optional().valid('WEB','ANDROID', 'IPHONE'),
          deviceId: Joi.string().optional(),           
        },

        failAction: UniversalFunctions.failActionFunction                                   
      },
        plugins: {                                                      
        'hapi-swagger': {                                              
         payloadType: 'form',                                            
         responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages 
        }
      }
    }
  },
  //User LOGIN API

  {
    method: 'GET',
    path: '/User/Login',
    options: {
      description: 'User Login Api',
      auth: false,
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.userLogin(request.query)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
          email:Joi.string().email().required().description("Enter your email here "),
          password: Joi.string().required().description("Enter your Password here"),
          deviceType: Joi.string().optional().valid('ANDROID', 'IPHONE','WEB'),
          deviceId: Joi.string().optional()

        },
        // headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'POST',
    path: '/User/LoginGmail',
    options: {
      description: 'User Login Api',
      auth: false,
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.Login_gamil(request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
              socialKey : Joi.string().required().description("Enter social key"),
              profilePicture : Joi.string().optional(),
              fullName : Joi.string().optional(),
              email : Joi.string().optional().email(),
              countrycode : Joi.string().optional(),
              phoneNumber : Joi.number().optional(),
              password : Joi.string().optional(),
              deviceType: Joi.string().optional().valid('WEB','ANDROID', 'IPHONE'),
              deviceId: Joi.string().optional(),
        },
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/User/FogotPassword',
    options: {
      description: 'User forgot password Api',
      auth: false,
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.forgotPassword(request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
              email: Joi.string().required().description("Enter your email "),
        },

        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/User/VerifyOtp',
    options: {
      description: 'User verify otp Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.verifyOtp(request.payload, request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          otp:Joi.number().integer().required()
        },
         headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },

  {
    method: 'PUT',
    path: '/User/ReSendOtp',
    options: {
      description: 'User resend Api',
      auth: false,
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.reSendOtp(request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
            countrycode : Joi.string().required(),
            phoneNumber : Joi.number().required(),
         },
        //  headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/User/ChangePassword',
    options: {
      description: 'User new password Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.changePasword(request.payload, request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
            old_password:Joi.string().required().description("Enter your old password here"), 
            New_password: Joi.string().required().description("Enter your password here ")
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/User/ProfileUpdate',
    options: {
      description: 'User profile Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {

        console.log("........request.auth.credentials....",request.auth.credentials);
        return Controller.userController.profileUpdate(request.auth.credentials,request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          profilePicture:Joi.string().optional().description("Enter your pofile picture"),
          fullName:Joi.string().optional().description("Enter full name"),
          phoneNumber:Joi.number().integer().optional().description("Enter your phone number"),
          countrycode:Joi.string().optional(),
          address:Joi.string().optional().description("Enter address"),
          email: Joi.string().email().optional().description("Enter your Email Here"),
          lat:Joi.string().optional().description("Enter latitude"),
          lng:Joi.string().optional().description("Enter logntitude"),
          delete:Joi.bool().optional().valid("false","true"),
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/User/DeviceIdAndTypeUpdate',
    options: {
      description: 'User update device id and type  Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.deviceDataUpdate(request.auth.credentials,request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
          deviceType: Joi.string().optional().valid('WEB','ANDROID', 'IPHONE'),
          deviceId: Joi.string().optional()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/User/Logout',
    options: {
      description: 'User logout',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.logout(request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/User/HomePage',
    options: {
      description: 'User home page Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.Home_Page(request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
       
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/User/SerachProducts',
    options: {
      description: 'User search products Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.search_products(request.query,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
         categoryId:Joi.string().required().description("Enter category id"),
         searchProduct: Joi.string().required()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/User/GetSubCategoryProduct',
    options: {
      description: 'User get sub categories and product Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.Sub_Category_Product_list(request.query,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
         categoryId:Joi.string().required().description("Enter category id"),
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/User/scheduleDateTime',
    options: {
      description: 'User get scheduleDateTime Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.scheduleDateTime(request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/User/GetDateTime',
    options: {
      description: 'User get scheduleDateTime Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.dateTimeGet(request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'POST',
    path: '/User/BookService',
    options: {
      description: 'User book services Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.createBooking(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
         products:Joi.array().required().description("Enter (id) and quentity(qty)"),
         address:Joi.string().required().description("Enter your address"),
         lat:Joi.string().required().description("Enter latitude"),
         lng:Joi.string().required().description("Enter logntitude"),
         date:Joi.date().required().description("YYY-MM-DD"),
         time:Joi.string().required().description("hours:mintues"),
         paymentMode:Joi.string().required().valid("Cash","Online"),
         timeZone:Joi.string().optional().description("timeZone"),
         specialInstruction:Joi.string().optional().description("About service"),
         cardId :Joi.string().optional(),
         totalPrice : Joi.number().integer().optional()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/User/OrderList',
    options: {
      description: 'User  OrderList Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.orderListing(request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
       
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/User/OrderDetails',
    options: {
      description: 'User OrderDetails Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.OrderDetails(request.query,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: {
          orderId :Joi.string().required() 
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/User/addAddress',
    options: {
      description: 'User add_address Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.addAddress(request.payload, request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: {
                lat : Joi.number().required(),
                lng : Joi.number().required(),
                address : Joi.string().required()
        },
         headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'POST',
    path: '/User/SendMessages',
    options: {
      description: 'User send message to provider Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.sendMessages(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: { 
          bookingId:Joi.string().required().description("Enter booking id"),
          message:Joi.string().optional(),
          picture:Joi.string().optional()

        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/User/MessagesListing',
    options: {
      description: 'message  listing  Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.messagesListing(request.query,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query :{
          bookingId :Joi.string().required()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'POST',
    path: '/User/GiveRating',
    options: {
      description: 'give rating to Provider Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.giveRating(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload: { 
          providerId:Joi.string().required().description("Enter provider id"),
          orderId:Joi.string().required().description("Enter order id"),
          rating:Joi.string().required(),
          message:Joi.string().optional()

        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  
  {
    method: 'GET',
    path: '/User/GetUserDetils',
    options: {
      description: 'get user details Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.getUserDetils(request.query,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query: { 
          userId:Joi.string().required().description("Enter user id"),
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "GET",
    path: "/User/PolicyTermsCondtions",
    options: {
      description: "get privacy policy  and term conditions Api",
      auth: false,
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.userController.getTermCondition()
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/User/AccessTokenLogin',
    options: {
      description: 'User access token login Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.accessTokenLogin(request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
       
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'GET',
    path: '/User/NotificationListing',
    options: {
      description: 'User notification listing Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.notificationListing(request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
       
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: 'PUT',
    path: '/User/deleteNotification',
    options: {
      description: 'User notification delete Api',
      auth: { strategies: [Config.APP_CONSTANTS.SCOPE.USER] },
      tags: ['api'],
      handler: (request, reply) => {
        return Controller.userController.notificationDelete(request.auth.credentials,request.payload)
          .then(response => {
            return UniversalFunctions.sendSuccess("en", SUCCESS.DEFAULT, response, reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload:{
             isDeleted:Joi.boolean().required()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        'hapi-swagger': {
          payloadType: 'form',
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "POST",
    path: "/User/addCard",
    options: {
      description: "user add cards ",
      auth: {
        strategies: [Config.APP_CONSTANTS.SCOPE.USER]
      },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.userController.addCard(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en",SUCCESS.DEFAULT,response,reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload:{
          token: Joi.string().required(),
          type:Joi.string().optional()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "GET",
    path: "/User/listCard",
    options: {
      description: "listing cards ",
      auth: {
        strategies: [Config.APP_CONSTANTS.SCOPE.USER]
      },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.userController.listCard(request.query,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en",SUCCESS.DEFAULT,response,reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        query:{

          _id:Joi.string().optional()
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
  {
    method: "POST",
    path: "/User/deleteCard",
    options: {
      description: "add delete cards ",
      auth: {
        strategies: [Config.APP_CONSTANTS.SCOPE.USER]
      },
      tags: ["api"],
      handler: (request, reply) => {
        return Controller.userController.deleteCard(request.payload,request.auth.credentials)
          .then(response => {
            return UniversalFunctions.sendSuccess("en",SUCCESS.DEFAULT,response,reply);
          })
          .catch(error => {
            winston.error("=====error=============", error);
            return UniversalFunctions.sendError("en", error, reply);
          });
      },
      validate: {
        payload:{
          _id:Joi.string().required(),
          isDeleted:Joi.boolean().optional(),
        },
        headers: UniversalFunctions.authorizationHeaderObj,
        failAction: UniversalFunctions.failActionFunction
      },
      plugins: {
        "hapi-swagger": {
          payloadType: "form",
          responseMessages: Config.APP_CONSTANTS.swaggerDefaultResponseMessages
        }
      }
    }
  },
];
/**
 */
'use strict';

if (process.env.NODE_ENV == 'dev') {
    exports.config = {
        PORT : 8001,
        dbURI : 'mongodb://localhost/CarWash'
    }
}
else if (process.env.NODE_ENV == 'test') {
    exports.config = {
        PORT : 8001,
        dbURI : ''
    }
}

else {
    exports.config = {
        PORT : 8001,
        dbURI : 'mongodb://localhost/CarWash'
    };
}
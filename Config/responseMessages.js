/**
 */
exports.ERROR = {


    TRANSATION_FAILD :{
        statusCode: 400,
        customMessage: {
            en : 'transation faild please try again',
        },
        type: 'TRANSATION_FAILD'
    },

    INVALID_CARD_NUMBER :{
        statusCode: 400,
        customMessage: {
            en : 'please enter valid card number',
        },
        type: 'INVALID_CARD_NUMBER'
    },

    UNAUTHORIZED :{
        statusCode: 400,
        customMessage: {
            en : 'UNAUTHORIZED token is invalid',
        },
        type: 'UNAUTHORIZED'
    },


    SOMETHING_WENT_WRONG : {
        statusCode: 400,
        customMessage: {
            en : 'Something went wrong',
        },
        type: 'SOMETHING_WENT_WRONG'
    },

    
    ALREADY_ORDER : {
        statusCode: 400,
        customMessage: {
            en : 'Oops! you already order book this order.',
        },
        type: 'ALREADY_ORDER'
    },


    PRODUCT_ID_ALREADY_EXSIST : {
        statusCode: 400,
        customMessage: {
            en : 'Oops! this product id already exsist.',
        },
        type: 'PRODUCT_ID_ALREADY_EXSIST'
    },
    
    OFFLINE : {
        statusCode: 400,
        customMessage: 'You are offline now.',
        type: 'OFFLINE'
    },

    BLOCKED_DELETED : {
        statusCode: 400,
        customMessage: 'Admin delete and Block your account please connect to Admin.',
        type: 'BLOCKED_DELETED'
    },
    
    DELETED : {
        statusCode:400,
        customMessage:'Admin delete your account please connect to Admin.',
        type: 'DELETED'
    },
   
    BLOCKED : {
        statusCode:400,
        customMessage: ' Admin Blocked your Account.',
        type: 'BLOCKED'
    },

    DUPLICATE_PRODUCT_NAME : {
        statusCode: 400,
        customMessage: {
            en : 'Oops! Duplicate product name.',
        },
        type: 'DUPLICATE_PRODUCT_NAME'
    },
    
    INVALID_PRODUCT_ID : {
        statusCode: 400,
        customMessage: {
            en : 'Oops! this product id not exsist.',
        },
        type: 'INVALID_PRODUCT_ID'
    },
    INVALID_CATEGORY_ID : {
        statusCode: 400,
        customMessage: {
            en : 'Oops! this category id not exsist.',
        },
        type: 'INVALID_CATEGORY_ID'
    },
    INVALID_SUBCATEGORY_ID : {
        statusCode: 400,
        customMessage: {
            en : 'Oops! this  subcategory id not exsist.',
        },
        type: 'INVALID_SUBCATEGORY_ID'
    },
    DUPLICATE_CATEGORY_NAME : {
        statusCode: 400,
        customMessage: {
            en : 'Oops! Duplicate category name.',
        },
        type: 'DUPLICATE_CATEGORY_NAME'
    },
    
    DUPLICATE_SUBCATEGORY_NAME : {
        statusCode: 400,
        customMessage: {
            en : 'Oops! Duplicate subcategory name.',
        },
        type: 'DUPLICATE_SUBCATEGORY_NAME'
    },
    INVALID_OTP : {
        statusCode: 400,
        customMessage: {
            en : 'Oops! incorrect otp.',
        },
        type: 'INVALID_OTP'
    },
    INVALID_GMAIL : {
        statusCode: 400,
        customMessage: {
            en : 'Oops! This gmail already exsist pleace check again.',
        },
        type: 'INVALID_GMAIL'
    },
    GMAIL_NOT_EXSIST : {
        statusCode: 400,
        customMessage: {
            en : 'Oops! This gmail not exsist please check again.',
        },
        type: 'GMAIL_NOT_EXSIST'
    },
    
    INVALID_EMAIL : {
        statusCode: 400,
        customMessage: {
            en : 'Oops! The Email is incorrect.',
        },
        type: 'INVALID_EMAIL'
    },

    DUPLICATE_EMAIL:{
        statusCode :400,
        customMessage:{
            en:'Duplicate email'
        },
        type :'DUPLICATE_EMAIL'
    },
    
    INVALID_MOBILE_NUMBER: {
        statusCode: 400,
        customMessage: {
            en : 'Invalid Mobile Number',
        },
        type: 'INVALID_MOBILE_NUMBER'
    } ,

    PHONE_NUMBER_ALREADY_EXSIST: {
        statusCode: 400,
        customMessage: {
            en : 'phone number aleady exsist',
        },
        type: 'PHONE_NUMBER_ALREADY_EXSIST'
    } ,

    QUESTION_ALREADY_EXSIST: {
        statusCode: 400,
        customMessage: {
            en : 'Question aleady exsist',
        },
        type: 'QUESTION_ALREADY_EXSIST'
    } ,
        
    ANSWER_NUMBER_ALREADY_EXSIST: {
        statusCode: 400,
        customMessage: {
            en : 'Answer number aleady exsist',
        },
        type: 'ANSWER_NUMBER_ALREADY_EXSIST'
    } ,
    ANSWER_ALREADY_EXSIST: {
        statusCode: 400,
        customMessage: {
            en : 'Answer aleady exsist',
        },
        type: 'ANSWER_ALREADY_EXSIST'
    } ,
    OLD_PASSWORD_MISMATCH: {
        statusCode: 400,
        customMessage: {
            en : 'Old Password Mismatch',
        },
        type: 'OLD_PASSWORD_MISMATCH'
    },
    SAME_PLAYER: {
        statusCode: 400,
        customMessage: {
            en : 'Both player same',
        },
        type: 'SAME_PLAYER'
    },
    INVALID_OBJECT_ID : {
        statusCode:400,
        customMessage : {
            en : 'Invalid Game Id provided.',
        },
        type : 'INVALID_OBJECT_ID'
    },
    CODE_EXPIRE : {
        statusCode:400,
        customMessage : {
            en : 'Game code expire.',
        },
        type : 'CODE_EXPIRE'
    },
    NAME_ALREADY_EXISTS : {
        statusCode:400,
        customMessage : {
            en : 'Name Already Exists Please Change Name',
        },
        type : 'NAME_ALREADY_EXISTS'
    },

    WRONG_EMAIL_ADDRESS : {
        statusCode:400,
        customMessage : {
            en : 'The email you entered does not match any accounts.Please check your email.',
        },
        type : 'WRONG_EMAIL_ADDRESS'
    },
    
    
    NOT_BOOKING_PROVIDER : {
        statusCode:400,
        customMessage : {
            en : 'Opps  provider not booking after u can change status .',
        },
        type : 'NOT_BOOKING_PROVIDER'
    },
    
    NOT_ALLOWED : {
        statusCode:400,
        customMessage : {
            en : 'Opps  this service is already booked .',
        },
        type : 'NOT_ALLOWED'
    },


    DOCUMENT_NOT_UPLOADED : {
        statusCode:400,
        customMessage : {
            en : 'Opps  you are not upload your document .',
        },
        type : 'DOCUMENT_NOT_UPLOADED'
    },



    STILL_PROVIDER_NOT_BOOK : {
        statusCode:400,
        customMessage : {
            en : 'provider still not book your order.',
        },
        type : 'STILL_PROVIDER_NOT_BOOK'
    },
    
    INVALID_PROVIDER_ID : {
        statusCode:400,
        customMessage : {
            en : 'Invalid INVALID PROVIDER ID.',
        },
        type : 'INVALID_PROVIDER_ID'
    },
    INVALID_BOOKING_ID : {
        statusCode:400,
        customMessage : {
            en : 'Invalid Booking provided.',
        },
        type : 'INVALID_BOOKING_ID'
    },

    INVALID_USER_ID : {
        statusCode:400,
        customMessage : {
            en : 'Invalid UserId provided.',
        },
        type : 'INVALID_USER_ID'
    },
    
    COLLECTION_EMPTY : {
        statusCode:400,
        customMessage : {
            en : 'Collection is Empty',
        },
        type : 'COLLECTION_EMPTY'
    },
    INVALID_STORY_ID : {
        statusCode:400,
        customMessage : {
            en : 'Invalid Story Id provided.',
        },
        type : 'INVALID_STORY_ID'
    },
    
    NO_DATA_FOUND : {
        statusCode:400,
        customMessage : {
            en : 'No Data FOund',
        },
        type : ' NO_DATA_FOUND'
    },

    INVALID_OPERATION : {
        statusCode:400,
        customMessage : {
            en : 'Invalid operation.',
        },
        type : 'INVALID_OPERATION'
    },
    DB_ERROR: {
        statusCode: 400,
        customMessage: {
            en : 'DB Error : ',
        },
        type: 'DB_ERROR'
    },
    ALREADY_CONNECTED: {
        statusCode: 400,
        customMessage: {
            en : 'Application already connected with hardware device',
        },
        type: 'ALREADY_CONNECTED'
    },

    APP_ERROR: {
        statusCode: 400,
        customMessage: {
            en : 'Application Error ',
        },
        type: 'APP_ERROR'
    },
    DUPLICATE: {
        statusCode: 400,
        customMessage: {
            en : 'Duplicate Entry',
        },
        type: 'DUPLICATE'
    },
    EXPIRE: {
        statusCode: 400,
        customMessage: {
            en : "Sorry, we’re unable to process your review. You're too far away. ",
        },
        type: 'EXPIRE'
    },
    DEFAULT: {
        statusCode: 400,
        customMessage: {
            en : 'Something went wrong.',
        },
        type: 'DEFAULT'
    },
        
    NO_LOCATION: {
        statusCode:401,
        customMessage : {
            en : 'There is not any service request near by your location',
        },
        type : 'NO_LOCATION'
    },

    NOT_APPROVAL: {
        statusCode:401,
        customMessage : {
            en : 'You are not appoved to perform this action',
        },
        type : 'NOT_APPROVAL'
    },

    UNAUTHORIZED: {
        statusCode:401,
        customMessage : {
            en : 'You are not authorized to perform this action',
        },
        type : 'UNAUTHORIZED'
    },

    INVALID_CREDENTIALS : {
        statusCode: 400,
        customMessage: {
            en : 'Oops! The Email or password is incorrect.',
        },
        type: 'INVALID_CREDENTIALS'
    },

    INVALID_EMAIL : {
        statusCode: 400,
        customMessage: {
            en : 'Oops! The Email is incorrect.',
        },
        type: 'INVALID_EMAIL'
    },
    INVALID_USERNAME : {
        statusCode: 400,
        customMessage: {
            en : 'Oops! The Username is incorrect.',
        },
        type: 'INVALID_USERNAME'
    },
    WRONG_PASSWORD: {
        statusCode: 400,
        customMessage: {
            en : 'Password is Incorrect.',
        },
        type: 'WRONG_PASSWORD'
    } ,
    WRONG_OTP: {
        statusCode: 400,
        customMessage: {
            en : 'OTP is Incorrect.',
        },
        type: 'WRONG_OTP'
    } ,
    USER_BLOCKED: {
        statusCode: 401,
        customMessage: {
            en : 'User Is Blocked By Admin.',
        },
        type: 'USER_BLOCKED'
    } ,
    VENDOR_BLOCKED: {
        statusCode: 401,
        customMessage: {
            en : 'Vendor Is Blocked By Admin.',
        },
        type: 'VENDOR_BLOCKED'
    } ,
    OTP_NOT_VERIFIED: {
        statusCode: 400,
        customMessage: {
            en : 'OTP Not Verified',
        },
        type: 'OTP_NOT_VERIFIED'
    } ,
     
    WRONG_ACCESS_TOKEN: {
        statusCode: 400,
        customMessage: {
            en : 'Access Token is Incorrect',
        },
        type: 'WRONG_ACCESS_TOKEN'
    },
    ALREADY_CONNECTED: {
        statusCode: 400,
        customMessage: {
            en : 'Already connected',
        },
        type: 'ALREADY_CONNECTED'
    } ,

    OLD_WRONG_PASSWORD: {
        statusCode: 400,
        customMessage: {
            en : 'Old Password is Incorrect.',
        },
        type: 'OLD_WRONG_PASSWORD'
    } ,

    PHONE_NO_NOT_FOUND: {
        statusCode:400,
        customMessage : {
            en : 'Phone Number not found',
        },
        type : 'PHONE_NO_NOT_FOUND'
    },
    EMAIL_ALREADY_EXIST_USER: {
        statusCode:400,
        customMessage : {
            en : 'The email address user has already been used. Please user another email address',
        },
        type : 'EMAIL_ALREADY_EXIST_USER'
    },


    EMAIL_ALREADY_EXIST: {
        statusCode:400,
        customMessage : {
            en : 'The email address provided has already been used. Please provide another email address',
        },
        type : 'EMAIL_ALREADY_EXIST'
    },
    PHONENO_ALREADY_EXIST: {
        statusCode:400,
        customMessage : {
            en : 'The phone number provided has already been used.',
        },
        type : 'PHONENO_ALREADY_EXIST'
    },

    
    VALID_TIME: {
        statusCode:400,
        customMessage : {
            en : 'Please enter future time.',
        },
        type : 'VALID_TIME'
    },
    
    NO_SLOT_AVAILABLE: {
        statusCode:400,
        customMessage : {
            en : 'we can not create no slot available.',
        },
        type : 'NO_SLOT_AVAILABLE'
    },

    PHONENO_ALREADY_EXIST_USER: {
        statusCode:400,
        customMessage : {
            en : 'The phone number user has already been used.',
        },
        type : 'PHONENO_ALREADY_EXIST_USER'
    },

    EMAIL_OR_PASSWORD_ALREADY_EXIST: {
        statusCode:400,
        customMessage : {
            en : 'The email address or Mobile Number provided has already been used. Please provide another ',
        },
        type : 'EMAIL_OR_PASSWORD_ALREADY_EXIST'
    },


    USER_NOT_FOUND: {
        statusCode: 404,
        customMessage: {
            en:  'Customer not found'
        },
        type : 'CUSTOMER_NOT_FOUND'
    },
    EMAIL_NOT_FOUND: {
        statusCode: 400,
        customMessage: {
            en:  'Email not found'
        },
        type : 'EMAIL_NOT_FOUND'
    },
    NOT_CONNECTED_DEVICE_FOUND: {
        statusCode: 400,
        customMessage: {
            en:  'not connected device found'
        },
        type : 'NOT_CONNECTED_DEVICE_FOUND'
    },
    GAME_START_ERROR: {
        statusCode: 400,
        customMessage: {
            en:  "you cann't start the game" 
        },
        type : 'GAME_START_ERROR'
    },
    OPPPNENT_WAIT: {
        statusCode: 400,
        customMessage: {
            en:  "waiting for the oppnent" 
        },
        type : 'OPPPNENT_WAIT'
    },
    OPPPNENT_CANCEL: {
        statusCode: 400,
        customMessage: {
            en:  "oppnent cancel the game" 
        },
        type : 'OPPPNENT_CANCEL'
    },
    IN_PROCESS: {
        statusCode: 400,
        customMessage: {
            en:  "game already started" 
        },
        type : 'IN_PROCESS'
    },
    
    WAITING_RESULT: {
        statusCode: 400,
        customMessage: {
            en:  "waiting for both players results" 
        },
        type : 'WAITING_RESULT'
    },
    

};
exports.SUCCESS = {
    
    DEFAULT: {
        statusCode: 200,
        customMessage: {
            en : 'Success',
        },
        type: 'DEFAULT'
    },
    
    ONLINE : {
        statusCode: 200,
        customMessage: 'You are Online now.',
        type: 'ONLINE'
    },
    
    ADDED : {
        statusCode:200,
        customMessage: {
            en : 'Added successfully.',
        },
        type: 'ADDED'
    },
    FORGOT_PASSWORD: {
        statusCode:200,
        customMessage: {
            en: "A reset password link is sent to your registered email address."
        },
        type: 'FORGOT_PASSWORD'
    },
    PASSWORD_RESET_SUCCESSFULL:{
        statusCode:200,
        customMessage:{
            en:"Your Password has been Successfully Changed"
        },
        type:'PASSWORD_RESET_SUCCESSFULL'
    },
    RESET_PASSWORD:{
        statusCode:200,
        customMessage:{
            en:"A reset password OTP has been sent to your registered Phone Number"
        },
        type: 'RESET_PASSWORD'
    }
};
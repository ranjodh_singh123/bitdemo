var SERVER = {
  APP_NAME: "Car-Wash",
  SECRET: "#EGameeOkCfGKHJN<uHHSh",
  SALT: 11,
  JWT_SECRET_KEY_PROVIDER: "gff%$TGMJ^rztt",
  JWT_SECRET_KEY_USER: "gff%$TGMJ^rztt",
  JWT_SECRET_KEY_ADMIN: "gff%$TGMJ^rztt",
  JWT_SECRET_KEY_ARTIST: "gff%$TGMJ^rztt",
  MAX_DISTANCE_RADIUS_TO_SEARCH: 10000,
  NOTIFICATION_KEY: "AAAAb1QjLRA:APA91bE2VUh8H-cYXOkpnWH1-SonPhB6A-WJKz-IIdRODf1-2eU9hK4GNWNJvYdtPAgpfaO7iO4ts_JevG-H8VkyObltanEn2lsq5tb5HMGkE-HMFsNp2ZYUdytDfH3E_O1JpkhsPEhu"
};

var swaggerDefaultResponseMessages = [
  { code: 200, message: "OK" },
  { code: 400, message: "Bad Request" },
  { code: 401, message: "Unauthorized" },
  { code: 404, message: "Data Not Found" },
  { code: 500, message: "Internal Server Error" }
];

var SCOPE = {
  ADMIN : "ADMIN",
  USER : "USER",
  PROVIDER : "PROVIDER",
};

var MODELS = {
  users : "users"
};

var TYPE = {
  BLOCKED : "BLOCKED",
  UNBLOCKED : "UNBLOCKED"
};

var DATABASE_CONSTANT = {
  DEVICE_TYPES : {
    IOS : "IOS",
    ANDROID : "ANDROID"
  }
};

var MAX_DISTANCE = {
    RADIUS: 0
  };

var CONNECTION = {
    ADDED: "Added",
    NOTADDED: "Not Added",
    PENDING: "Pending"
  };

var APP_CONSTANTS = {
  SERVER: SERVER,
  swaggerDefaultResponseMessages: swaggerDefaultResponseMessages,
  SCOPE: SCOPE,
  MODELS: MODELS,
  DATABASE_CONSTANT: DATABASE_CONSTANT,
  TYPE: TYPE,
  MAX_DISTANCE: MAX_DISTANCE,
  CONNECTION : CONNECTION

};

module.exports = APP_CONSTANTS;

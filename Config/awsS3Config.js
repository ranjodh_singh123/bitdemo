'use strict';
var s3BucketCredentials = {
    "bucket": "",
    "accessKeyId": "",
    "secretAccessKey": "",
    "s3URL": "",
    "folder": {
        "original": "original",
        "thumbnail": "thumbnail",
        "processed": "processed",
        "thumbnail1080": "thumbnail1080",
        "thumbnailMed": "thumbnailMed"
    }
};

module.exports = {
    // s3BucketCredentials: s3BucketCredentials
};

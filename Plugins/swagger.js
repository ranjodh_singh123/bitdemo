
'use strict';

const Inert = require('inert'),
    Vision = require('vision');
const HapiSwagger = require('hapi-swagger');

exports.plugin = {
    name: 'swagger-plugin',
    register: async (server) => {
        const swaggerOptions = {
            info: {
                title: 'Car Wash'
            },
            documentationPage: process.env.NODE_ENV !== 'production'
        };
        let swagger = [];
        if(process.env.NODE_ENV === 'production'){
        }
        else{
            swagger = [
                Inert,
                Vision,
                {
                    plugin: HapiSwagger,
                    options: swaggerOptions
                }
            ]
        }        
        await server.register(
            swagger
        );
        winston.info('Swagger Loaded');
    }
};

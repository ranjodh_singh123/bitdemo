
'use strict';

const Models = require("../Models");
const users = require("../Models/users");
const providers =require("../Models/providers");


const admins =require("../Models/admins");
const { Model } = require("mongoose");

var rn = require('random-number');

function  aggregatedata(model,group){
    return new Promise((resolve, reject) => {
        try {
            let getdata = model.aggregate(group);
            
            return resolve( getdata);
        } catch (err) {
            return reject(err);
        }

    });
}
function  aggregatedata1(model,group){
    return new Promise((resolve, reject) => {
        try {
            let getdata = model.aggregate(group);
            
            return resolve( getdata);
        } catch (err) {
            return reject(err);
        }

    });
}

function saveData(model,data) {
    return new Promise((resolve, reject) => {
        try {
            let saveData = model.create(data);
            return resolve(saveData);
        } catch (err) {
            return reject(err);
        }
    });
}

// function saveData(model,data) {
//     return new Promise((resolve, reject) => {
//         try {
//             let saveData = model.insertMany(data);
//             return resolve(saveData);
//         } catch (err) {
//             return reject(err);
//         }
//     });
//  }

function createData(model,data) {
    return new Promise((resolve, reject) => {
        try {
            let createData = model.create(data);
            return resolve(createData);
        } catch (err) {
            return reject(err);
        }
    });
 }
 
 function getData(model,query,projection,option) {
    return new Promise((resolve, reject) => {
        try {
            let findData = model.find(query,projection,option);
            return resolve(findData)
        } catch (err) {
            return reject(err);
        }
    });
  }


function getmessage(model,query,projection,option) {
    return new Promise((resolve, reject) => {
        try {
            let findData = model.find(query,projection,option).sort({createdAt: 1});
            return resolve(findData)
        } catch (err) {
            return reject(err);
        }
    });
  }

function getquestion(model,query,projection,option) {
    return new Promise((resolve, reject) => {
        try {
            let number = rn.generator({
                min:0,
                max:2,
                integer:true
              });
            let findData = model.find(query,projection,option).limit(2).skip(number(1));
            return resolve(findData);
        } catch (err) {
            return reject(err);
        }
    });
}
function getdata(model,query,projection,option) {
    return new Promise((resolve, reject) => {
        try {
            let findData = model.find(query,projection,option).sort({createdAt:-1});
            return resolve(findData);
        } catch (err) {
            return reject(err);
        }
    });
}


function getDataPages(model,query,projection,option,pageNumber) {
    return new Promise((resolve, reject) => {
        try {
            let findData = model.find(query,projection,option).limit(100).skip(pageNumber).sort({createdAt:-1});
            return resolve(findData);
        } catch (err) {
            return reject(err);
        }
    });
}


function getDataOne(model, query, projection, options) {
    return new Promise((resolve, reject) => {
        try {
            let findData = model.findOne(query, projection, options);
            return resolve(findData);
        } catch (err) {
            return reject(err);
        }
    });
}

function getUniqueData(model,keyName,query, options) {
    return new Promise((resolve, reject) => {
        try {
            let getUniqueData = model.distinct(keyName, query, options);
            return resolve(getUniqueData);
        } catch (err) {
            return reject(err);
        }
    });
}

function findAndUpdate(model, conditions, update, options) {
    return new Promise((resolve, reject) => {
        try {
            let data = model.findOneAndUpdate(conditions, update, options);
            return resolve(data);
        } catch (err) {
            return reject(err);
        }
    });
}

function update(model, conditions, update, options) {
    return new Promise((resolve, reject) => {
        try {
            let data = model.update(conditions, update, options);
            return resolve(data);
        } catch (err) {
            return reject(err);
        }
    });
}
function updateMany(model, conditions, update, options) {
    return new Promise((resolve, reject) => {
        try {
            let data = model.updateMany(conditions, update, options);
            return resolve(data);
        } catch (err) {
            return reject(err);
        }
    });
}
function remove(model, condition) {
    return new Promise((resolve, reject) => {
        try {
            let data = model.deleteMany(condition);
            return resolve(data);
        } catch (err) {
            return reject(err);
        }
    });
}

function remove1(model, condition) {
    return new Promise((resolve, reject) => {
        try {
            let data = model.deleteOne(condition);
            return resolve(data);
        } catch (err) {
            return reject(err);
        }
    });
}


function populateData(model, query, projection, options, collectionOptions) {
    return new Promise((resolve, reject) => {
        try {
            let data = model.find(query, projection, options).populate(collectionOptions).exec();
            return resolve(data);
        } catch (err) {
            return reject(err);
        }
    });
}

function populateDataAdmin(model, query, projection, options, pageNumber,collectionOptions) {
    return new Promise((resolve, reject) => {
        try {
            let data = model.find(query, projection, options).limit(100).skip(pageNumber).populate(collectionOptions).exec();
            return resolve(data);
        } catch (err) {
            return reject(err);
        }
    });
}

async function deepPopulateData(model, query, projection, options, collectionOptions,populateOptions) {

        try {
            let data = await model.find(query, projection, options).populate(collectionOptions).exec();
            // console.log("=======data===========",data[0].favourites)
            let populateData = await model.populate(data,populateOptions);
            return (populateData);
        }
        catch (err) {
            return err;
        }
}

function count(model, condition) {
    return new Promise((resolve, reject) => {
        try {
            let data = model.count(condition);
            return resolve(data);
        } catch (err) {
            return reject(err);
        }
    });
}

function aggregateData(model, group,options) {
    return new Promise((resolve, reject) => {
        try {
            let data;

            if(options !==undefined){
                data = model.aggregate(group).option(options);
            }
            else{
                data = model.aggregate(group);
            }

            return resolve(data);
        } catch (err) {
            return reject(err);
        }
    });
}

function insert(model, data, options) {
    return new Promise((resolve, reject) => {
        try {
            let data = model.collection.insert(data,options);
            return resolve(data);
        } catch (err) {
            return reject(err);
        }
    });
}
function createindex(model, data1) {
    return new Promise((resolve, reject) => {
        try {
            let data = model.collection.createIndex(data1);
            return resolve(data);
        } catch (err) {
            return reject(err);
        }
    });
}

function insertMany(model,condition, insert, options) {
    return new Promise((resolve, reject) => {
        try {
            let data = model.collection.insertMany(condition,insert,options);
            return resolve(data);
        } catch (err) {
            return reject(err);
        }
    });
}

let bulkFindAndUpdate= (bulk,query,update,options)=> {
    bulk.find(query).upsert().update(update,options);
};

let bulkFindAndUpdateOne= (bulk,query,update,options)=> {
    bulk.find(query).upsert().updateOne(update,options);
};

async function aggregateDataWithPopulate(model, group, populateOptions,options) {
    try {
        // console.log("========options==========",options)
        let aggregateData;
        if(options !==undefined){
            aggregateData = await model.aggregate(group).option(options);
        }
        else{
            aggregateData = await model.aggregate(group);
        }
        // console.log("=========aggregateData============",aggregateData.length)
        let populateData = await model.populate(aggregateData,populateOptions);
        
        
        // console.log("=======populateData===========",populateData.length)
        return populateData;
    } catch (err) {
        return err;
    }
}

module.exports = {
    getquestion:getquestion,
    createData:createData,
    aggregatedata:aggregatedata,
    getdata :getdata,
    saveData : saveData,
    getData : getData,
    getmessage:getmessage,
    getDataOne : getDataOne,
    update : update,
    updateMany:updateMany,
    remove: remove,
    getDataPages:getDataPages,
    remove1: remove1,
    insert: insert,
    insertMany: insertMany,
    getUniqueData : getUniqueData,
    createindex : createindex,
    count: count,
    populateDataAdmin:populateDataAdmin,
    findAndUpdate : findAndUpdate,
    populateData : populateData,
    aggregateData : aggregateData,
    aggregatedata1 :aggregatedata1,
    aggregateDataWithPopulate: aggregateDataWithPopulate,
    bulkFindAndUpdate : bulkFindAndUpdate,
    deepPopulateData:deepPopulateData,
    bulkFindAndUpdateOne : bulkFindAndUpdateOne
};
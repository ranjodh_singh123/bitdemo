const DAO = require("../DAOManager").queries;
const Models = require("../Models");
const TokenManager = require("../Libs/tokenManager");

const match_category = async(providerId) => {
    try {
        let match = {
              $match : {
                providerId : mongoose.Types.ObjectId(providerId),
                $or : [ { status : "Pending" } , { status : "Confirmed" }  ],
                $and : [ {logsStatus :{$ne: "Completed"}} ,{ logsStatus : {$ne : "Canceled"}} ],
                $or : [ {logsStatus : "Reached"} ,{ logsStatus :  "Started"} ],
                $or : [ {logsStatus : "Accepted"} ,{ isDeleted : false} ],
                logsStatus :{$ne: "Rejected"},
                isDeleted : false,
                isBlocked : false
              }
            }           
            return match;
    }
    catch(err) {
          throw err;
    }
}
const match_category_1 = async(providerId) => {
    try {
        let match = {
              $match : {
                providerId : mongoose.Types.ObjectId(providerId),
                $or : [ { logsStatus : "Completed" } , { logsStatus : "Canceled" } ],
                $or : [ { logsStatus : "Rejected" } , { isDeleted : false } ],
                $and :[{logsStatus : {$ne : "Accepted"}},{logsStatus :{$ne : "Reached"}}],
                logsStatus : {$ne : "Started"},
                isBlocked : false
              }
            }           
            return match;
    }
    catch(err) {
          throw err;
    }
}

const lookup_user = async() => {
    try {

      let lookup = {
            $lookup : {
               from : "users",
               localField : "user_id",
               foreignField : "_id",
               as : "user"
             }
          }
          return lookup;
    }
    catch(err) {
          throw err;
    }
}

const unwind_user = async() => {
    try {

      let unwind = {$unwind : "$user"}
          return unwind;
    }
    catch(err) {
          throw err;
    }
}

const lookup_user_product = async() => {
    try {

        let lookup = {
              $lookup : {
                from : "bookingdetails",
                localField : "_id",
                foreignField : "orderId",
                as : "productuser"
              },
            }      
            return lookup;
      }
      catch(err) {
            throw err;
      }
  }

  const unwind = async() => {
    try {

       let unwind =   { $unwind : "$productuser" }      
       return unwind;
    }
    catch(err) {
          throw err;
    }
}


const lookup_product = async() => {
    try {

    let lookup = {
          $lookup : {
              from : "products",
              localField : "productuser.productId",
              foreignField : "_id",
              as : "pro"
           },
        }      
        return lookup;
    }
    catch(err) {
          throw err;
    }
}


const lookup_sub_category = async() => {
    try {

    let lookup = {
           $lookup : {
              from : "subcategories",
              localField : "pro.subCategoryId",
              foreignField : "_id",
              as : "subcatg"
            }
        }
        return lookup;
    }
    catch(err) {
          throw err;
    }
}


const lookup_category = async() => {
    try {

    let lookup = {
          $lookup : {
               from : "categories",
               localField : "subcatg.categoryId",
               foreignField : "_id",
               as : "catg"
           }
        }
        return lookup;
    }
    catch(err) {
          throw err;
    }
}


const unwind_category = async() => {
    try {

           let unwind_category =   { $unwind : "$catg" }      
           return unwind_category;
    }
    catch(err) {
          throw err;
    }
}


const group_sub_category_product = async() => {
    try {

        let group = {
               $group : {
                "_id" : "$_id", 
                "user_id" : { $first : "$user_id"},
                "user_Name" : { $first : "$user.fullName"},
                "address" : {$first : "$address"},
                "booking_id" : { $first : "$booking_id"},
                "booking_Date" : { $first : "$date"},
                "booking_Time" : { $first : "$time"},
                "status" : { $first : "$logsStatus"},
                "category_id" : { $first : "$catg._id"},
                "category" : { $first : "$catg.name"}
               } 
            }
            return group;
    }
    catch(err) {
          throw err;
    }
}

const group_sub_category_product_1 = async() => {
    try {

        let group = {
               $group : {
                    "_id" : "$_id", 
                    "user_id" : { $first : "$user_id"},
                    "user_Name" : { $first : "$user.fullName"},
                    "address" : {$first : "$address"},
                    "booking_id" : { $first : "$booking_id"},
                    "booking_Date" : { $first : "$date"},
                    "booking_Time" : { $first : "$time"},
                    "status" : { $first : "$logsStatus"},
                    "category_id" : { $first : "$catg._id"},
                    "category" : { $first : "$catg.name"}
                } 
            }
            return group;
    }
    catch(err) {
          throw err;
    }
}


const project_data = async() => {
    try{
        let project = {
                  $project : {
                        _id : 1, 
                        user_id : 1,
                        user_Name : 1,
                        address : 1,
                        booking_id : 1,
                        booking_Date : 1,
                        booking_Time : 1,
                        status : 1,
                        category_id : 1,
                        category : 1        
                   } 
            }
            return project;
    }
    catch(err) {
          throw err;
    }
}

const sort = async() => {
    try {
        let sort = { 
               $sort : {
                booking_Date : -1 
               }
            }    
            return sort;
    }
    catch(err) {
          throw err;
    }
}

const project_data_1 = async(last, ongoing) => {
    try {
        
        let data1 = [];
        if(ongoing.length != 0){
            data1 = ongoing;
        }
        let data = [];
        if(last.length != 0){
            data = last;
        }

        let project = {
                        Ongoing_Service : data1,
                        Last_Services : data                   
            }
            return project;
    }
    catch(err) {
          throw err;
    }
}




const provider_list_order_1 = async(providerId) => {
    try {
           let match = await match_category(providerId);
           let lookup_User = await lookup_user();
           let unwind_User = await unwind_user();
           let lookup_userProduct = await lookup_user_product();
           let unwind1 = await unwind();
           let lookup_Product = await lookup_product();
           let lookup_Sub_category = await lookup_sub_category();
           let lookup_Category = await lookup_category();
           let unwind_Category = await unwind_category();
           let group = await group_sub_category_product();
           let project = await project_data();
           let sort_date = await sort();
           let query = [
                        match,
                        lookup_User,
                        unwind_User,
                        lookup_userProduct,
                        unwind1,
                        lookup_Product,
                        lookup_Sub_category,
                        lookup_Category,
                        unwind_Category,
                        group,
                        project,
                        sort_date
               ];
               let data = await DAO.aggregatedata1(Models.bookings,query);
               return data;
    }
    catch(err) {
          throw err;
    }
}


const provider_list_order = async(providerId) => {
    try {  
           let ongoing = await provider_list_order_1(providerId);
           let last = await provider_list_order_2(providerId);
           let project = await project_data_1(last,ongoing);
           return project;
    }
    catch(err) {
          throw err;
    }
}


const provider_list_order_2 = async(providerId) => {
    try { 
           let match = await match_category_1(providerId);
           let lookup_User = await lookup_user();
           let unwind_User = await unwind_user();
           let lookup_userProduct = await lookup_user_product();
           let unwind1 = await unwind();
           let lookup_Product = await lookup_product();
           let lookup_Sub_category = await lookup_sub_category();
           let lookup_Category = await lookup_category();
           let unwind_Category = await unwind_category();
           let group = await group_sub_category_product_1();
           let sort_date = await sort();
           let project = await project_data();
           let query = [
                        match,
                        lookup_User,
                        unwind_User,
                        lookup_userProduct,
                        unwind1,
                        lookup_Product,
                        lookup_Sub_category,
                        lookup_Category,
                        unwind_Category,
                        group,
                        project,
                        sort_date,
               ];
               let data = await DAO.aggregatedata(Models.bookings,query);
               return data;
    }
    catch(err) {
          throw err;
    }
}



module.exports = {
    provider_list_order_1: provider_list_order_1,
    provider_list_order_2:provider_list_order_2,
    project_data:project_data,
    provider_list_order:provider_list_order,
    lookup_user:lookup_user,
    lookup_user_product:lookup_user_product,
    unwind:unwind,
    unwind_user:unwind_user,
    group_sub_category_product:group_sub_category_product,
    group_sub_category_product_1:group_sub_category_product_1,
    match_category_1:match_category_1,
    project_data_1:project_data_1,
    lookup_sub_category:lookup_sub_category,
    lookup_category:lookup_category,
    unwind_category:unwind_category,
    lookup_product:lookup_product,
    match_category:match_category,
}
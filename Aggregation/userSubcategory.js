const DAO = require("../DAOManager").queries;
const Models = require("../Models");
const TokenManager = require("../Libs/tokenManager");

const match_category = async(categoryId) => {
    try {

        let match = {

                $match : {
                    categoryId : mongoose.Types.ObjectId(categoryId)
                }
                    
           }     
           return match;
    }
    catch(err) {
          throw err;
    }
}


const lookup_product = async() => {
    try {

    let lookup = {
            $lookup : {
                from : "products",
                let : { id : "$_id" },
                  pipeline : [ {
                             $match : {
                                  $expr : {
                                      $and : [
                                      { $eq : [ "$subCategoryId","$$id" ] }
                                      ]
                                   }
                              }
                   },
                   { 
                   $project : {_id : 1, name : 1,price : 1, picture : 1,description : 1}
                   }
                   ],
                   as : "product"          
            }
        }    
        return lookup;
    }
    catch(err) {
        throw err;
    }
}

const group_sub_category_product = async() => {
    try {

        let group = {
                    $group : {
                            "_id" : "$_id",
                            "Subcategory" : { $first : "$name" },
                            "Products" : { $first : "$product" }
                    }
            }
               
            return group;
    }
    catch(err) {
          throw err;
    }
}



const list_sub_category_product = async(categoryId) => {
    try {
     
           let match = await match_category(categoryId);
           let lookup = await lookup_product();
           let group = await group_sub_category_product();
           let query = [
                       match,
                       lookup,
                       group
               ];
               let data = await DAO.aggregatedata(Models.subCategories,query);

               return data;
    }
    catch(err) {
          throw err;
    }
}

module.exports = {
    list_sub_category_product:list_sub_category_product,
    group_sub_category_product:group_sub_category_product,
    lookup_product:lookup_product,
    match_category:match_category
}
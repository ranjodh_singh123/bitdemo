const DAO = require("../DAOManager").queries;
const Models = require("../Models");
const TokenManager = require("../Libs/tokenManager");


const geo_location_user = async(location1) => {
    try {

     let location = {
            $geoNear : {
                near : { 
                type : "Point",
                coordinates : location1
                },
                distanceField : "distance",
                maxDistance : 2000000,
                distanceMultiplier : 0.001,
                key :'location',
                spherical : true
            }
         }
         return location ;
    }
    catch(err) {  
       throw err;
    }
}

const distance_location_user = async() => {
    try {
        let project = {
                     $project : {
                           _id : 1,
                           fullName:1,
                           distance : 1,
                           address : 1
                     } 
            }
            return project;
    }
    catch(err) {
          throw err;
    }
}

const match_service_id = async(id) => {
    try {
        let match = 
                    {
                    $match : {_id:mongoose.Types.ObjectId(id)} 
           }
           return match;
    }
    catch(err) {
          throw err;
    }
}

const track_location_distance_user = async(location,id) => {
    try {
           let distance = await geo_location_user(location);
           let service_id = await match_service_id(id);
           let project  = await distance_location_user();
           let query = [
                       distance,
                       service_id,
                       project
                ];
                let checkCategories = await DAO.aggregatedata(Models.providers, query);
                return checkCategories; 
    }
    catch(err) {
          throw err;
    }
}

module.exports = {
    geo_location_user:geo_location_user,
    match_service_id:match_service_id,
    track_location_distance_user:track_location_distance_user,
    distance_location_user:distance_location_user,
}

const DAO = require("../DAOManager").queries;
const Models = require("../Models");
const TokenManager = require("../Libs/tokenManager");

const lookup_product1 = async() => {
    try {

    let lookup ={
            $lookup : {
                    from : "products",
                    let : { id : "$_id" },
                     pipeline : [
                        { $match :
                          { $expr :
                              { $and :
                                   [
                                      { $eq: [ "$subCategoryId","$$id" ] }
                                   ]
                              }
                           }
                        },
                      { $project : { _id : 1, name : 1, price : 1 } }
                      ],
                      as: "product"          
             }
        }    
        return lookup;
    }
    catch(err) {
          throw err;
    }
}

const lookup_category1 = async() => {
    try {

    let lookup = {
          $lookup : {
               from : "categories",
               localField : "categoryId",
               foreignField : "_id",
               as : "catg"
          }
        }
        return lookup;
    }
    catch(err) {
          throw err;
    }
}

const unwind_category = async() => {
    try {

       let unwind_category =   { $unwind : "$catg" }      
       return unwind_category;
    }
    catch(err) {
          throw err;
    }
}

const category_sub_category_product = async() => {
    try {
    let group = {
          $group : {
              "_id" : "$catg._id",
              "category" : { $first : "$catg.name" },
              "Subcategory" : { $addToSet :{
              "_id" : "$_id", 
              "Subcategory" : "$name",
              "Products" : "$product"
                                 }
               },
          }
        } 
        return group;
    }
    catch(err) {
          throw err;
    }
}


const categories_List = async(userId) => {
    try {
    
           let lookup_product = await lookup_product1();
           let lookup_Category = await lookup_category1();
           let unwind_Category = await unwind_category();
           let group = await category_sub_category_product();

            let query =[
                      lookup_Category,
                      unwind_Category,
                      lookup_product,
                      group           
                ];
                let checkCategories = await DAO.aggregatedata(Models.subCategories, query);         
                return checkCategories; 
    }
    catch(err) {
          throw err;
    }
}


module.exports = {
    categories_List:categories_List,
    category_sub_category_product:category_sub_category_product,
    lookup_category1:lookup_category1,
    lookup_product1:lookup_product1,
    unwind_category:unwind_category,
}

const DAO = require("../DAOManager").queries;
const Models = require("../Models");
const TokenManager = require("../Libs/tokenManager");


const geo_location = async(location1) => {
    try {

    let location = {
          $geoNear : {
               near : { 
               type : "Point",
               coordinates : location1
             },
             distanceField : "distance",
             maxDistance : 2000000,
             distanceMultiplier : 0.001,
             key : 'servicelocation',
             spherical : true
           }
       }
       return location ;
    }
    catch(err) {  
       throw err;
    }
}

const distance_location = async() => {
    try {
         let project = 
                    {
                    $project : {
                         _id : 0,
                         user_id : 1,
                         distance : 1,
                         address : 1 
                    } 
           }
           return project;
    }
    catch(err) {
          throw err;
    }
}

const match_service_id = async(id) => {
    try {
        let match = 
                    {
                    $match : {_id : mongoose.Types.ObjectId(id)} 
        }
        return match;
    }
    catch(err) {
          throw err;
    }
}

const track_location_distance= async(location,id) => {
    try {
           let distance = await geo_location(location);
           let service_id = await match_service_id(id);
           let project  = await distance_location();
           let query = [
                      distance,
                      service_id,
                      project
           ];
           let checkCategories = await DAO.aggregatedata(Models.serviceUsers, query);
           return checkCategories; 
    }
    catch(err) {
          throw err;
    }
}

module.exports = {
    geo_location:geo_location,
    match_service_id:match_service_id,
    track_location_distance:track_location_distance,
    distance_location:distance_location,
}
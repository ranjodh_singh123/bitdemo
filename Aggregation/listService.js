const DAO = require("../DAOManager").queries;
const Models = require("../Models");
const TokenManager = require("../Libs/tokenManager");



const match = async() => {
    try {    
        let date1 = new Date(Date.now()).getTime();
        console.log(date1)
        let match = {
                $match : {
                     date : {$gte :  date1}
                }
        }          
        return match;
    }
    catch(err) {
          throw err;
    }
}



const lookup_user_product = async() => {
    try {

        let lookup = {
              $lookup : {
                from : "bookingdetails",
                localField : "_id",
                foreignField : "orderId",
                as : "productuser"
              },
            }      
            return lookup;
      }
      catch(err) {
            throw err;
      }
  }

const lookup_product_id = async() => {
    try {    

    let lookup ={
        $lookup :{
              from : "products",
              localField : "productuser.productId",
              foreignField : "_id",
              as : "product"
         },
      }      
     return lookup;
    }
    catch(err) {
          throw err;
    }
}


const geo_near_location =async (location)=>{
    try {

    let location_data =      {
            $geoNear: {
                  near: { type : "Point", coordinates : location },
                  distanceField : "distance",
                  minDistance : 0,
                  maxDistance : 500000,
                  query : { status : "Pending" },
                  key : 'servicelocation',
                  spherical : true
            }
          }
          return location_data;
    }
    catch(err) {
          throw err;
    }
}


const unwind_user_Id = async() => {
    try {

    let unwind_user_id =  {$unwind : "$user_id"}      
     return unwind_user_id;
    }
    catch(err) {
          throw err;
    }
}

const unwind_user = async() => {
    try {

    let unwind_user =  {$unwind : "$user"}      
     return unwind_user;
    }
    catch(err) {
          throw err;
    }
}

const unwind_category = async() => {
    try {

    let unwind_category =   { $unwind : "$catg" }      
     return unwind_category;
    }
    catch(err) {
          throw err;
    }
}


const lookup_Category = async() => {
    try {

    let lookup ={
        $lookup :{
               from : "categories",
               localField : "product.categoryId",
               foreignField : "_id",
               as : "catg"
         }
       }
       return lookup ;
    }
    catch(err) {  
       throw err;
    }
}


const lookup_user = async() => {
    try {

    let lookup ={                                      
        $lookup : {
                 from : "users",
                 localField : "user_id",
                 foreignField : "_id",
                 as : "user"
         }
       }
       return lookup ;
    }
    catch(err) {  
       throw err;
    }
}

const match_category = async(categoryId) => {
    try {
        let match = {
              $match : {'catg._id': mongoose.Types.ObjectId(categoryId) },
            }      
            return match;
      }
      catch(err) {
            throw err;
      }
  }


const order_category_user_date_time = async() => {
    try {
    let group ={
          $group : {
           "_id" : "$_id",
           "category" : { $first : "$catg.name" },
           "userId" : { $first : "$user._id" },
           "username" : { $first : "$user.fullName" },
           "ProfilePicture" : { $first : "$user.profilePicture" },
           "date" : { $first : "$date" },
           "time" : { $first : "$time" },
           "location" : { $first : "$servicelocation.coordinates" },
           "address":{$first : "$address" }
         } 
       } 
       return group;
    }
    catch(err) {
          throw err;
    }
}



const sort = async() => {
    try {
        let sort = { 
               $sort : {
                date : -1 
               }
            }    
            return sort;
    }
    catch(err) {
          throw err;
    }
}

const list_services= async(location,categoryId) => {
    try {
           let location_data = await geo_near_location(location);
           let matchData = await match();
           let lookup_userProduct = await lookup_user_product();
           let unwind_user_id = await unwind_user_Id();
           let lookup_product_Id = await lookup_product_id();
           let lookup_category =await lookup_Category();
           let unwind_Category = await unwind_category();
           let match_data = await match_category(categoryId);
           let lookup_users = await lookup_user(); 
           let unwind_users = await unwind_user();
           let group = await order_category_user_date_time();
           let Sort_data = await sort();

            let query = [
                      location_data,
                      matchData,
                      lookup_userProduct,
                      unwind_user_id,
                      lookup_product_Id,
                      lookup_category,
                      unwind_Category,
                      match_data,
                      lookup_users,
                      unwind_users,
                      group,
                      Sort_data           
             ];
             let checkCategories =await DAO.aggregatedata1(Models.bookings, query);
             return checkCategories; 
    }
    catch(err) {
          throw err;
    }
}

module.exports = {
    match :match,
    list_services:list_services,
    match_category:match_category,
    order_category_user_date_time:order_category_user_date_time,
    lookup_user:lookup_user,
    lookup_Category:lookup_Category,
    unwind_user:unwind_user,
    lookup_user_product:lookup_user_product,
    unwind_user_Id:unwind_user_Id,
    geo_near_location:geo_near_location,
    unwind_category:unwind_category,
    lookup_product_id:lookup_product_id,
}
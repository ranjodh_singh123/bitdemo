const DAO = require("../DAOManager").queries;
const Models = require("../Models");
const TokenManager = require("../Libs/tokenManager");

const match_category = async(userId) => {
    try {
        let match = {
              $match : {
                   user_id : mongoose.Types.ObjectId(userId),
                   $or : [ { status : "Pending" } , { status : "Confirmed" } ],
                   isDeleted : false,
                   isBlocked : false
              }
           }           
           return match;
    }
    catch(err) {
          throw err;
    }
}

const lookup_user_product = async() => {
    try {

        let lookup = {
              $lookup : {
                from : "bookingdetails",
                localField : "_id",
                foreignField : "orderId",
                as : "productuser"
              },
            }      
            return lookup;
      }
      catch(err) {
            throw err;
      }
  }

const lookup_product = async() => {
    try {

        let lookup = {
              $lookup : {
                from : "products",
                localField : "productuser.productId",
                foreignField : "_id",
                as : "product"
              },
            }      
            return lookup;
      }
      catch(err) {
            throw err;
      }
  }



const lookup_sub_category = async() => {
    try {

    let lookup = {
            $lookup : {
              from : "subcategories",
              localField : "product.subCategoryId",
              foreignField : "_id",
              as : "subcatg"
            }
        }
        return lookup;
    }
    catch(err) {
          throw err;
    }
}

const lookup_user = async() => {
    try {

    let lookup = {
        $lookup:
           {
             from: "users",
             localField : "user_id",
             foreignField : "_id",
             as: "user"
           }
      }
      return lookup;
    }
    catch(err) {
          throw err;
    }
}

const unwind_user = async() => {
    try {

            let unwind_provider =   { $unwind : "$user" }      
            return unwind_provider;
    }
    catch(err) {
          throw err;
    }
}


const lookup_provider = async() => {
    try {

    let lookup = {
        $lookup:
           {
             from: "providers",
             localField : "providerId",
             foreignField : "_id",
             as: "provider"
           }
      }
      return lookup;
    }
    catch(err) {
          throw err;
    }
}

const unwind_provider = async() => {
    try {

            let unwind_provider =   { $unwind : "$provider" }      
            return unwind_provider;
    }
    catch(err) {
          throw err;
    }
}

const lookup_category = async() => {
    try {

    let lookup = {
           $lookup : {
               from : "categories",
               localField : "subcatg.categoryId",
               foreignField : "_id",
               as : "catg"
           }
        }
        return lookup;
    }
    catch(err) {
          throw err;
    }
}

const unwind_category = async() => {
    try {

            let unwind_category =   { $unwind : "$catg" }      
            return unwind_category;
    }
    catch(err) {
          throw err;
    }
}


const group_sub_category_product = async() => {

    try {

        let group = {
               $group : {
                  "_id" : "$_id",
                  "user_id" : {$first : "$user_id"},
                  "email" : {$first : "$user.email"},
                  "countrycode" : {$first : "$user.countrycode"},
                  "phoneNumber" : {$first : "$user.phoneNumber"},
                  "profilePicture" : {$first : "$user.profilePicture"},
                  "accessToken" : {$first : "$user.accessToken"},
                  "service" : {$addToSet : {
                  "order_id" : "$_id",
                  "provider_id" :  "$providerId",
                  "provider_name" : {$cond: { if: { $eq: [ "$provider.fullName", [ ] ] }, then: null, else: {$arrayElemAt:["$provider.fullName",0]} }},
                  "booking_id" :  "$booking_id",
                  "booking_Time" :  "$date",
                  "status" :  "$logsStatus",
                  "address":   "$address",
                  "category_id" : "$catg._id",
                  "category" :  "$catg.name"
                               }
                  }
                }                  
            } 
            return group;
    }
    catch(err) {
          throw err;
    }
}

const project_data = async() => {
    try {

        let project = {
               $project : { 
                    _id : 0,
                    user_id : 1,
                    email : 1,
                    countrycode : 1,
                    phoneNumber : 1,
                    profilePicture : 1,
                    accessToken : 1,
                    service : 1,
                   
               }                  
            } 
            return project;
    }
    catch(err) {
          throw err;
    }
}


const list_order = async(userId) => {
    try {  
        let match = await match_category(userId);
           let lookup_userProduct = await lookup_user_product();
           let lookup = await lookup_product();
           let lookup_Sub_category = await lookup_sub_category();
           let lookup_Category = await lookup_category();
           let unwind_Category = await unwind_category();
           let lookupProvider  =  await lookup_provider();
           let lookupUser = await lookup_user();
           let unwindUser = await unwind_user();
           let group = await group_sub_category_product();
           let project = await project_data();
           let query = [
                        match,
                        lookup_userProduct,
                        lookup,
                        lookup_Sub_category,
                        lookup_Category,
                        unwind_Category,
                        lookupProvider,
                        lookupUser,
                        unwindUser,
                        group,
                        project
               ];
               let data =await DAO.aggregatedata(Models.bookings,query);

               return data;
    }
    catch(err) {
          throw err;
    }
}





module.exports = {
    list_order:list_order,
    unwind_provider:unwind_provider,
    project_data:project_data,
    group_sub_category_product:group_sub_category_product,
    lookup_sub_category:lookup_sub_category,
    lookup_category:lookup_category,
    lookup_user_product:lookup_user_product,
    unwind_user:unwind_user,
    lookup_user:lookup_user,
    lookup_provider:lookup_provider,
    unwind_category:unwind_category,
    lookup_product:lookup_product,
    match_category:match_category,

}
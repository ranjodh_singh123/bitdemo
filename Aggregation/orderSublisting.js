const DAO = require("../DAOManager").queries;
const Models = require("../Models");
const TokenManager = require("../Libs/tokenManager");

const match_user_id = async(userId,orderId) => {
    try {

        let match = {
                    $match : {
                         user_id : mongoose.Types.ObjectId(userId),
                         _id : mongoose.Types.ObjectId(orderId),
                         isDeleted : false,
                         isBlocked : false
                    }       
           }
           return match;
    }
    catch(err) {
          throw err;
    }
}

const lookup_user_product = async() => {
    try {

        let lookup = {
              $lookup : {
                from : "bookingdetails",
                localField : "_id",
                foreignField : "orderId",
                as : "productuser"
              },
            }      
            return lookup;
      }
      catch(err) {
            throw err;
      }
  }
 
  const unwind = async() => {
    try {

       let unwind =   { $unwind : "$productuser" }      
       return unwind;
    }
    catch(err) {
          throw err;
    }
  }

 const lookup_provider = async() => {
    try {

    let lookup = {
        $lookup:
           {
             from: "providers",
             localField : "providerId",
             foreignField : "_id",
             as : "provider"
           }
      }
      return lookup;
    }
    catch(err) {
          throw err;
    }
}

const lookup_provider_rating = async() => {
    try {

    let lookup = {
        $lookup :
           {
             from : "providerratings",
             localField : "_id",
             foreignField : "orderId",
             as : "data"
           }
      }
      return lookup;
    }
    catch(err) {
          throw err;
    }
}


const lookup_product_id = async() => {
    try {

      let lookup = {
            $lookup : {
              from : "products",
              localField : "productuser.productId",
              foreignField : "_id",
              as : "product"
            },
          }      
          return lookup;
    }
    catch(err) {
          throw err;
    }
}


const lookup_sub_category = async() => {
    try {

     let lookup ={
           $lookup : {
              from : "subcategories",
              localField : "product.subCategoryId",
              foreignField : "_id",
              as : "subcatg"
            }
          }
          return lookup;
    }
    catch(err) {
          throw err;
    }
}

const unwind_product = async() => {
    try {

       let unwind_product =  { $unwind : "$product" }      
       return unwind_product;
    }
    catch(err) {
          throw err;
    }
}

const unwind_sub_category = async() => {
    try {

        let unwind_subcategory =  {$unwind : "$subcatg"}     
        return unwind_subcategory;
    }
    catch(err) {
          throw err;
    }
}

const lookup_category = async() => {
    try {

      let lookup = {
             $lookup : {
                from : "categories",
                localField : "subcatg.categoryId",
                foreignField : "_id",
                as : "catg"
             }
          }
          return lookup;
    }
    catch(err) {
          throw err;
    }
}


const unwind_category = async() => {
    try {

       let unwind_category =   { $unwind : "$catg" }      
       return unwind_category;
    }
    catch(err) {
          throw err;
    }
}


const group_booking_product_subcategory_category = async() => {
    try {
    let group = {
            $group : {
               "_id" : "$_id",
               "provider_id" : { $first : "$providerId"},
               "count" : {$first : "$data" },
               "rating" : {$avg:{$sum : "$data.rating"}},
               "ratingMsg":{$first:"$data.message"},
               "ratingId":{$first:"$data._id"},
               "provider_name" : { $first:"$provider.fullName"},
               "phoneNumber" : { $first:"$provider.phoneNumber"},
               "profilePicture" : {$first:"$provider.profilePicture"},
               "booking_id" : { $first : "$booking_id" },
               "booking_Date" : { $first : "$date" },
               "booking_Time" : { $first : "$time" },
               "status" : { $first : "$logsStatus" },
               "address" : {$first : "$address"},
               "location" : {$first : "$servicelocation.coordinates"},
               "special_instruction" : { $first :"$specialInstruction"},
               "product" : { $addToSet : { 
               "productid" :  "$product._id",
               "quentity" :  "$productuser.quentity",
               "productName" : "$product.name",
               "picture" : "$product.picture",
               "price" : "$product.price",
               "subcategory_id" : "$subcatg._id",
               "subcategory" : "$subcatg.name",
               "category_id" : "$catg._id",
               "category" : "$catg.name"   
                              }
                }
            } 
        } 
        return group;
    }
    catch(err) {
          throw err;
    }
}

const project_data = async() => {
    try {
    let project = {
            $project : {
                _id : 1,
                provider_id : 1,
                provider_name : {
                    $cond: { if: { $eq: [ "$provider_name", [ ] ] }, then: null, else: {$arrayElemAt:["$provider_name",0]} }
                  },
                  phoneNumber : {
                    $cond: { if: { $eq: [ "$phoneNumber", [ ] ] }, then: null, else: {$arrayElemAt:["$phoneNumber",0]} }
                },
                profilePicture : {
                  $cond: { if: { $eq: [ "$profilePicture", [ ] ] }, then: null, else: {$arrayElemAt:["$profilePicture",0]} }
                },
                ratings: {
                  $cond: { if: { $eq: [ "$rating", 0 ] }, then: null, else: { $round: [{ $divide: [ "$rating", {$size : "$count"} ] },1]} }
                },
                ratingMsg:{$cond: { if: { $eq: [ "$ratingMsg", [ ] ] }, then: null, else:"$ratingMsg" }}, 
                ratingId:{$cond: { if: { $eq: [ "$ratingId", [ ] ] }, then: null, else:"$ratingId" }}, 
                address : 1,
                location : 1 ,
                booking_id : 1,
                booking_Date : 1,
                booking_Time : 1,
                status : 1,
                special_instruction : 1,
                product : 1
            } 
        } 
        return project;
    }
    catch(err) {
          throw err;
    }
}

const list_sub_order = async(userId,orderId) => {
    try {
           let match = await match_user_id(userId,orderId);
           let lookup_userProduct = await lookup_user_product(); 
           let unwind1 = await unwind();
           let lookup_product = await lookup_product_id();
           let unwind_Product = await unwind_product();
           let lookup_subCategory = await lookup_sub_category();
           let unwind_subcategory = await unwind_sub_category();
           let lookup_Category = await lookup_category();
           let unwind_Category = await unwind_category();
           let lookupProvider  =  await lookup_provider();
           let lookupRating  =  await lookup_provider_rating();
           let group = await group_booking_product_subcategory_category();
           let project = await project_data();
           let query = [
                      match ,
                      lookupRating,
                      lookup_userProduct,
                      unwind1,
                      lookup_product,
                      unwind_Product,
                      lookup_subCategory,
                      unwind_subcategory,
                      lookup_Category,
                      unwind_Category,
                      lookupProvider,
                      group,
                      project
            ];
            let checkservices =await DAO.aggregatedata( Models.bookings, query );
  
            return checkservices; 
    }
    catch(err) {
          throw err;
    }
}

module.exports = {
    list_sub_order:list_sub_order,
    lookup_user_product:lookup_user_product,
    group_booking_product_subcategory_category:group_booking_product_subcategory_category,
    lookup_sub_category:lookup_sub_category,
    lookup_category:lookup_category,
    unwind_category:unwind_category,
    unwind_product:unwind_product,
    lookup_provider:lookup_provider,
    lookup_provider_rating:lookup_provider_rating,
    project_data:project_data,
    unwind_sub_category:unwind_sub_category,
    lookup_product_id:lookup_product_id,
    match_user_id:match_user_id,
    unwind:unwind,
}
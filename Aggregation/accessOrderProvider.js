const DAO = require("../DAOManager").queries;
const Models = require("../Models");
const TokenManager = require("../Libs/tokenManager");

const match_category = async(providerId) => {
    try {
        let match = {
              $match : {
                providerId : mongoose.Types.ObjectId(providerId),
                status : "Confirmed",
                isDeleted : false,
                isBlocked : false
              }
            }           
            return match;
    }
    catch(err) {
          throw err;
    }
}

const lookup_user = async() => {
    try {

      let lookup = {
            $lookup : {
               from : "users",
               localField : "user_id",
               foreignField : "_id",
               as : "user"
             }
          }
          return lookup;
    }
    catch(err) {
          throw err;
    }
}

const unwind_user = async() => {
    try {

      let unwind = {$unwind : "$user"}
          return unwind;
    }
    catch(err) {
          throw err;
    }
}

const lookup_provider = async() => {
    try {

    let lookup = {
        $lookup:
           {
             from: "providers",
             localField : "providerId",
             foreignField : "_id",
             as: "provider"
           }
      }
      return lookup;
    }
    catch(err) {
          throw err;
    }
}

const unwind_provider = async() => {
    try {

            let unwind_provider =   { $unwind : "$provider" }      
            return unwind_provider;
    }
    catch(err) {
          throw err;
    }
}

const lookup_user_product = async() => {
    try {

        let lookup = {
              $lookup : {
                from : "bookingdetails",
                localField : "_id",
                foreignField : "orderId",
                as : "productuser"
              },
            }      
            return lookup;
      }
      catch(err) {
            throw err;
      }
  }

  const unwind = async() => {
    try {

       let unwind =   { $unwind : "$productuser" }      
       return unwind;
    }
    catch(err) {
          throw err;
    }
}


const lookup_product = async() => {
    try {

    let lookup = {
          $lookup : {
              from : "products",
              localField : "productuser.productId",
              foreignField : "_id",
              as : "pro"
           },
        }      
        return lookup;
    }
    catch(err) {
          throw err;
    }
}


const lookup_sub_category = async() => {
    try {

    let lookup = {
           $lookup : {
              from : "subcategories",
              localField : "pro.subCategoryId",
              foreignField : "_id",
              as : "subcatg"
            }
        }
        return lookup;
    }
    catch(err) {
          throw err;
    }
}


const lookup_category = async() => {
    try {

    let lookup = {
          $lookup : {
               from : "categories",
               localField : "subcatg.categoryId",
               foreignField : "_id",
               as : "catg"
           }
        }
        return lookup;
    }
    catch(err) {
          throw err;
    }
}


const unwind_category = async() => {
    try {

           let unwind_category =   { $unwind : "$catg" }      
           return unwind_category;
    }
    catch(err) {
          throw err;
    }
}


const group_sub_category_product = async() => {
    try {

        let group = {
               $group : {
                "_id" : "$_id",
                "provider_id" : { $first : "$providerId"},
                "fullName" : { $first : "$provider.fullName"},
                "email" : { $first : "$provider.email"},
                "countrycode" : { $first : "$provider.countrycode"},
                "phoneNumber" : { $first : "$provider.phoneNumber"},
                "profilePicture" : { $first : "$provider.profilePicture"},
                "accessToken" : { $first : "$provider.accessToken"},
                "location" : {$first : "$provider.location"},
                "documentUploaded" : { $first : "$provider.documentUploaded"},
                "isApproval" : { $first : "$provider.isApproval"},
                "service" : {$addToSet : {
                "order_id" : "$_id", 
                "user_id" :  "$user_id",
                "user_Name" :  "$user.fullName",
                "address" :  "$address",
                "booking_id" :  "$booking_id",
                "booking_Date" :  "$date",
                "booking_Time" :  "$time",
                "status" :  "$logsStatus",
                "category_id" :  "$catg._id",
                "category" : "$catg.name"
                            }
               }
               } 
            }
            return group;
    }
    catch(err) {
          throw err;
    }
}


const project_data = async() => {
    try{
        let project = {
                  $project : {
                        _id : 0, 
                        provider_id : 1,
                        fullName : 1,
                        email : 1,
                        countrycode : 1,
                        phoneNumber : 1,
                        profilePicture : 1,
                        accessToken : 1,
                        location : 1,
                        documentUploaded : 1,
                        isApproval : 1,
                        service : 1
                   } 
            }
            return project;
    }
    catch(err) {
          throw err;
    }
}



const provider_list_order = async(providerId) => {
    try {  
        let match = await match_category(providerId);
           let lookup_User = await lookup_user();
           let unwind_User = await unwind_user();
           let looup_Provider = await lookup_provider();
           let unwind_Provider = await  unwind_provider();
           let lookup_userProduct = await lookup_user_product();
           let unwind1 = await unwind();
           let lookup_Product = await lookup_product();
           let lookup_Sub_category = await lookup_sub_category();
           let lookup_Category = await lookup_category();
           let unwind_Category = await unwind_category();
           let group = await group_sub_category_product();
           let project = await project_data();
           let query = [
                        match,
                        lookup_User,
                        unwind_User,
                        looup_Provider,
                        unwind_Provider,
                        lookup_userProduct,
                        unwind1,
                        lookup_Product,
                        lookup_Sub_category,
                        lookup_Category,
                        unwind_Category,
                        group,
                        project
               ];
               let data = await DAO.aggregatedata(Models.bookings,query);
               return data;
           
    }
    catch(err) {
          throw err;
    }
}



module.exports = {
    project_data:project_data,
    provider_list_order:provider_list_order,
    lookup_user:lookup_user,
    lookup_user_product:lookup_user_product,
    unwind:unwind,
    unwind_user:unwind_user,
    unwind_provider:unwind_provider,
    lookup_provider:lookup_provider,
    group_sub_category_product:group_sub_category_product,
    lookup_sub_category:lookup_sub_category,
    lookup_category:lookup_category,
    unwind_category:unwind_category,
    lookup_product:lookup_product,
    match_category:match_category,
}
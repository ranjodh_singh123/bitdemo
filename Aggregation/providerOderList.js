const DAO = require("../DAOManager").queries;
const Models = require("../Models");
const TokenManager = require("../Libs/tokenManager");

const match_user_id = async(orderId) => {
    try {

        let match = {
                    $match : {
                         _id : mongoose.Types.ObjectId(orderId),
                         isDeleted : false,
                         isBlocked : false
                    }
                    
            }
            return match;
    }
    catch(err) {
          throw err;
    }
}

const lookup_user_rating = async() => {
    try {

    let lookup = {
           $lookup : {
               from : "userratings",
               localField : "_id",
               foreignField : "orderId",
               as : "data"
           }
        }
        return lookup;
    }
    catch(err) {
          throw err;
    }
}



const lookup_user_product = async() => {
    try {

        let lookup = {
              $lookup : {
                from : "bookingdetails",
                localField : "_id",
                foreignField : "orderId",
                as : "productuser"
              },
            }      
            return lookup;
      }
      catch(err) {
            throw err;
      }
  }

  const unwind = async() => {
    try {

       let unwind =   { $unwind : "$productuser" }      
       return unwind;
    }
    catch(err) {
          throw err;
    }
}


const lookup_product_id = async() => {
    try {

    let lookup = {
          $lookup :{
              from : "products",
              localField : "productuser.productId",
              foreignField : "_id",
              as : "product"
           },
       }      
       return lookup;
    }
    catch(err) {
          throw err;
    }
}


const lookup_sub_category = async() => {
    try {

    let lookup = {
          $lookup : {
              from : "subcategories",
              localField : "product.subCategoryId",
              foreignField : "_id",
              as : "subcatg"
           }
        }
        return lookup;
    }
    catch(err) {
          throw err;
    }
}


const lookup_category = async() => {
    try {

    let lookup = {
           $lookup : {
               from : "categories",
               localField : "subcatg.categoryId",
               foreignField : "_id",
               as : "catg"
           }
        }
        return lookup;
    }
    catch(err) {
          throw err;
    }
}

const lookup_user = async() => {
    try {

    let lookup = {
            $lookup : {
               from : "users",
               localField : "user_id",
               foreignField : "_id",
               as : "user"
            }
        }
        return lookup;
    }
    catch(err) {
          throw err;
    }
}

const unwind_user = async() => {
    try {

            let unwind_user =   { $unwind : "$user" }      
            return unwind_user;

    }
    catch(err) {
          throw err;
    }
}

const unwind_product = async() => {
    try {

             let unwind_product =  { $unwind : "$product" }      
             return unwind_product;
    }
    catch(err) {
          throw err;
    }
}

const unwind_category = async() => {
    try {

             let unwind_category =   { $unwind : "$catg" }      
             return unwind_category;
    }
    catch(err) {
          throw err;
    }
}
const unwind_sub_category = async() => {
    try {

             let unwind_subcategory =  { $unwind : "$subcatg"}     
             return unwind_subcategory;
    }
    catch(err) {
          throw err;
    }
}


const order_booking_product_subCategory_category = async() => {
    try {
    let group = {
           $group : {
             "_id" : "$_id",
             "user_id" : { $first : "$user._id"},
             "user_name" : { $first : "$user.fullName"},
             "phoneNumber":{$first : "$user.phoneNumber"},
             "profilePicture" : { $first : "$user.profilePicture"},
             "count" : {$first : "$data" },
             "rating" : {$avg : {$sum : "$data.rating"}},
             "ratingMsg":{$first:"$data.message"},
             "ratingId":{$first:"$data._id"},
             "address" : {$first : "$address" },
             "location" : {$first : "$servicelocation.coordinates" },
             "Date" : { $first : "$date"},
             "Time" : { $first : "$time"},
             "status" : { $first : "$logsStatus"},
             "address" : {$first : "$address"},
             "location" : {$first : "$servicelocation.coordinates"},
             "specialInstruction" : { $first : "$specialInstruction"},
             "product" : { $addToSet : {
             "product_id" : "$product._id",
             "quentity" : "$productuser.quentity",
             "picture" : "$product.picture",
             "product" : "$product.name",
             "price" : "$product.price",
             "subcategory_id" : "$subcatg._id",
             "subcategory" : "$subcatg.name",
             "category_id" : "$catg._id",
             "category" : "$catg.name"
                           }       
              }
            }      
        }  
        return group;
    }
    catch(err) {
          throw err;
    }
}

const project_data = async() => {
    try {
    let project = {
            $project : {
                _id : 1,
                user_id : 1,
                user_name : 1,
                phoneNumber : 1,
                profilePicture : 1, 
                ratings:{
                    $cond: { if: { $eq: [ "$rating", 0 ] }, then: null, else: { $round : [{ $divide : [ "$rating", {$size : "$count"} ] },1]} }
                  },
                ratingMsg:{$cond: { if: { $eq: [ "$ratingMsg", [ ] ] }, then: null, else:"$ratingMsg" }}, 
                ratingId:{$cond: { if: { $eq: [ "$ratingId", [ ] ] }, then: null, else:"$ratingId" }}, 
                address : 1,
                location : 1 ,
                booking_id : 1,
                Date : 1,
                Time : 1,
                status : 1,
                specialInstruction : 1,
                product : 1
            } 
        } 
        return project;
    }
    catch(err) {
          throw err;
    }
}


const provider_order_sub_list= async(orderId) => {
    try {
           let user_id = await match_user_id(orderId);
           let lookup_User = await lookup_user();
           let unwind_User = await unwind_user();
           let lookup_userProduct = await lookup_user_product();
           let unwind1 = await unwind();
           let lookup_product_Id = await lookup_product_id();
           let unwind_Product = await unwind_product();
           let lookup_Sub_category =await lookup_sub_category();
           let unwind_Sub_category = await unwind_sub_category();
           let lookup_Category =await lookup_category();
           let unwind_Category = await unwind_category();
           let lookupRating =  await lookup_user_rating();
           let group = await order_booking_product_subCategory_category();
           let project = await project_data();

           let query = [
                      user_id,
                      lookup_User,
                      unwind_User,
                      lookup_userProduct,
                      unwind1,
                      lookup_product_Id,
                      unwind_Product,
                      lookup_Sub_category,
                      unwind_Sub_category,
                      lookup_Category,
                      unwind_Category,
                      lookupRating,
                      group,
                      project           
            ];
            let checkCategories = await DAO.aggregatedata(Models.bookings, query);
            return checkCategories; 
    }
    catch(err) {
          throw err;
    }
}

module.exports = {
    provider_order_sub_list:provider_order_sub_list,
    order_booking_product_subCategory_category:order_booking_product_subCategory_category,
    lookup_sub_category:lookup_sub_category,
    lookup_category:lookup_category,
    unwind_sub_category:unwind_sub_category,
    lookup_user_product:lookup_user_product,
    unwind:unwind,
    lookup_user:lookup_user,
    project_data:project_data,
    unwind_user:unwind_user,
    unwind_category:unwind_category,
    lookup_user_rating:lookup_user_rating,
    lookup_product_id:lookup_product_id,
    unwind_product:unwind_product,
    match_user_id:match_user_id
}
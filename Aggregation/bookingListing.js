const DAO = require("../DAOManager").queries;
const Models = require("../Models");
const TokenManager = require("../Libs/tokenManager");


const match_date = async(startdate,enddate) => {
    try {

        let match = {
                  $match : { 
                      date : { 
                          $gte : startdate ,
                           $lt : enddate 
                      }
                  }
            }
            return match;
    }
    catch(err) {
          throw err;
    }
}

const lookup_user_product = async() => {
    try {

        let lookup = {
              $lookup : {
                from : "bookingdetails",
                localField : "_id",
                foreignField : "orderId",
                as : "productuser"
              },
            }      
            return lookup;
      }
      catch(err) {
            throw err;
      }
  }

  const unwind = async() => {
    try {

            let unwind =   { $unwind : "$productuser" }     
            return unwind;
    }
    catch(err) {
          throw err;
    }
}

const lookup_product = async() => {
    try {

        let lookup = {
              $lookup : {
                from : "products",
                localField : "productuser.productId",
                foreignField : "_id",
                as : "product"
              },
            }      
            return lookup;
      }
      catch(err) {
            throw err;
      }
  }



const lookup_sub_category = async() => {
    try {

    let lookup = {
            $lookup : {
              from : "subcategories",
              localField : "product.subCategoryId",
              foreignField : "_id",
              as : "subcatg"
            }
        }
        return lookup;
    }
    catch(err) {
          throw err;
    }
}


const lookup_category = async() => {
    try {

    let lookup = {
           $lookup : {
               from : "categories",
               localField : "subcatg.categoryId",
               foreignField : "_id",
               as : "catg"
           }
        }
        return lookup;
    }
    catch(err) {
          throw err;
    }
}

const unwind_product = async() => {
    try {

            let unwind_product =   { $unwind : "$product" }     
            return unwind_product;
    }
    catch(err) {
          throw err;
    }
}

const unwind_sub_category = async() => {
    try {

            let unwind_sub_category =   { $unwind : "$subcatg" }     
            return unwind_sub_category;
    }
    catch(err) {
          throw err;
    }
}

const unwind_category = async() => {
    try {

            let unwind_category =   { $unwind : "$catg" }      
            return unwind_category;
    }
    catch(err) {
          throw err;
    }
}

const group_sub_category_product = async() => {

    try {

        let group = {
                 $group : {
                       "_id" : "$_id",
                       "booking_id" :  {$first : "$booking_id"},
                       "booking_Date" : {$first : "$date" },
                       "booking_Time" : {$first : "$time"} ,
                       "status" : {$first : "$status" },
                       "special_instruction":{$first : "$specialInstruction"},
                        "category" : { $addToSet : {
                       "category_id" : "$catg._id",
                       "category" : "$catg.name",
                       "subcategory_id" : "$subcatg._id",
                       "subcategory" : "$subcatg.name",
                       "product_id" : "$product._id",
                       "quentity" : "productuser.quntity",
                       "product" : "$product.name",
                       "price" : "$product.price",
                                     }                              
                        }
                   }
             } 
             return group;
    }
    catch(err) {
          throw err;
    }
}

const project_data = async() => {
    try {
        let project = {
               $project : {
                      _id : 1,
                     booking_id : 1,
                     booking_Time : 1 ,
                     booking_Date : 1,
                     status : 1,
                     special_instruction : 1,
                     category : 1                      
                } 
            }
            return project ;
            
    }
    catch(err) {
          throw err;
    }
}



const list_booking = async(startdate,enddate) => {
    try {
           let match = await match_date(startdate,enddate);
           let lookup_userProduct = await lookup_user_product();
           let unwind1 = await unwind();
           let lookup_Product = await lookup_product();
           let unwind_Product = await unwind_product();
           let lookup_subCategory = await lookup_sub_category();
           let unwind_subcategory = await unwind_sub_category();
           let lookup_Category = await lookup_category();
           let unwind_Category = await unwind_category();
           let group = await group_sub_category_product();
           let project = await project_data();
           let query = [
                      match ,
                      lookup_userProduct,
                      unwind1,
                      lookup_Product,
                      unwind_Product,
                      lookup_subCategory,
                      unwind_subcategory,
                      lookup_Category,
                      unwind_Category,
                      group,
                      project
            ];
            let checkservices =await DAO.aggregatedata( Models.bookings, query );
  
            return checkservices; 
    }
    catch(err) {
          throw err;
    }
}

module.exports = {
    list_booking:list_booking,
    match_date:match_date,
    lookup_user_product:lookup_user_product,
    lookup_product:lookup_product,
    unwind_product:unwind_product,
    lookup_sub_category:lookup_sub_category,
    unwind_sub_category:unwind_sub_category,
    lookup_category:lookup_category,
    unwind_category:unwind_category,
    unwind:unwind,
    group_sub_category_product:group_sub_category_product,
    project_data:project_data,
}
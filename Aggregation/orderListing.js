const DAO = require("../DAOManager").queries;
const Models = require("../Models");
const TokenManager = require("../Libs/tokenManager");

const match_category = async(userId) => {
    try {
        let match = {
              $match : {
                   user_id : mongoose.Types.ObjectId(userId),
                   $or : [ { status : "Pending" } , { status : "Confirmed" }  ],
                   $and : [ {logsStatus :{$ne: "Completed"}} ,{ logsStatus : {$ne : "Canceled"}} ],
                   $or : [ {logsStatus : "Reached"} ,{ logsStatus :  "Started"} ],
                   $or : [ {logsStatus : "Accepted"} ,{ isDeleted : false} ],
                   logsStatus :{$ne: "Rejected"},
                   isDeleted : false,
                   isBlocked : false
              }
           }           
           return match;
    }
    catch(err) {
          throw err;
    }
}

const match_category_1 = async(userId) => {
    try {
        let match = {
              $match : {
                   user_id : mongoose.Types.ObjectId(userId),
                   $or : [ { logsStatus : "Completed" } , { logsStatus : "Canceled" } ],
                   $or : [ { logsStatus : "Rejected" } , { isDeleted : false } ],
                   $and :[{logsStatus : {$ne : "Accepted"}},{logsStatus :{$ne : "Reached"}}],
                   logsStatus : {$ne : "Started"},
                   isBlocked : false
              }
           }           
           return match;
    }
    catch(err) {
          throw err;
    }
}

const lookup_user_product = async() => {
    try {

        let lookup = {
              $lookup : {
                from : "bookingdetails",
                localField : "_id",
                foreignField : "orderId",
                as : "productuser"
              },
            }      
            return lookup;
      }
      catch(err) {
            throw err;
      }
  }

const lookup_product = async() => {
    try {

        let lookup = {
              $lookup : {
                from : "products",
                localField : "productuser.productId",
                foreignField : "_id",
                as : "product"
              },
            }      
            return lookup;
      }
      catch(err) {
            throw err;
      }
  }



const lookup_sub_category = async() => {
    try {

    let lookup = {
            $lookup : {
              from : "subcategories",
              localField : "product.subCategoryId",
              foreignField : "_id",
              as : "subcatg"
            }
        }
        return lookup;
    }
    catch(err) {
          throw err;
    }
}

const lookup_provider_rating = async() => {
    try {

    let lookup = {
        $lookup :
           {
             from : "providerratings",
             localField : "_id",
             foreignField : "orderId",
             as : "data"
           }
      }
      return lookup;
    }
    catch(err) {
          throw err;
    }
}

const unwind_rating = async () => {
    try {

        let unwind = { $unwind: { path: "$data", preserveNullAndEmptyArrays: true } }
        return unwind;
    }
    catch (err) {
        throw err;
    }
}


const lookup_provider = async() => {
    try {

    let lookup = {
        $lookup:
           {
             from: "providers",
             localField : "providerId",
             foreignField : "_id",
             as: "provider"
           }
      }
      return lookup;
    }
    catch(err) {
          throw err;
    }
}

const unwind_provider = async() => {
    try {

            let unwind_provider =   { $unwind : "$provider" }      
            return unwind_provider;
    }
    catch(err) {
          throw err;
    }
}

const lookup_category = async() => {
    try {

    let lookup = {
           $lookup : {
               from : "categories",
               localField : "subcatg.categoryId",
               foreignField : "_id",
               as : "catg"
           }
        }
        return lookup;
    }
    catch(err) {
          throw err;
    }
}

const unwind_category = async() => {
    try {

            let unwind_category =   { $unwind : "$catg" }      
            return unwind_category;
    }
    catch(err) {
          throw err;
    }
}


const group_sub_category_product = async() => {

    try {

        let group = {
               $group : {
                  "_id" : "$_id",
                  "user_id" : {$first : "$user_id"},
                  "provider_id" : {$first : "$providerId"},
                  "provider_name" : {$first : "$provider.fullName"},
                  "booking_id" : {$first : "$booking_id"},
                  "booking_Time" : {$first : "$date"},
                  "status" :{$first : "$logsStatus"},
                  "address": {$first : "$address"},
                  "category_id" : {$first : "$catg._id"},
                  "category" : {$first : "$catg.name"}
                }                  
            } 
            return group;
    }
    catch(err) {
          throw err;
    }
}
const group_sub_category_product_1 = async() => {
    try {

        let group = {
               $group : { 
                    "_id" : "$_id",
                    "provider_id" : {$first : "$providerId"},
                    "provider_id" : {$first : "$providerId"},
                    "provider_name" : {$first : "$provider.fullName"},
                    "booking_id" : {$first : "$booking_id"},
                    "booking_Time" : {$first : "$date"},
                    "status" :{$first : "$logsStatus"},
                    "address": {$first : "$address"},
                    "category_id" : {$first : "$catg._id"},
                    "category" : {$first : "$catg.name"},
                    "ratingId" : {$first : "$data._id"},
               }                  
            } 
            return group;
    }
    catch(err) {
          throw err;
    }
}

const group = async() => {
    try {

        let group = {
               $group : { 
                    "_id" : "$_id",
                    "provider_id" : {$first : "$providerId"},
                    "provider_name" : {$first : "$provider.fullName"},
                    "booking_id" : {$first : "$booking_id"},
                    "booking_Time" : {$first : "$date"},
                    "status" :{$first : "$logsStatus"},
                    "address": {$first : "$address"},
                    "category_id" : {$first : "$catg._id"},
                    "category" : {$first : "$catg.name"}
               }                  
            } 
            return group;
    }
    catch(err) {
          throw err;
    }
}

const project_data = async() => {
    try {

        let project = {
               $project : { 
                   
                    _id : 1,
                    provider_id : 1,
                    provider_name : {
                        $cond: { if: { $eq: [ "$provider_name", [ ] ] }, then: null, else: {$arrayElemAt:["$provider_name",0]} }
                      },
                    booking_id : 1,
                    booking_Time : 1,
                    status : 1,
                    address : 1,
                    category_id : 1 ,
                    category : 1,
               }                  
            } 
            return project;
    }
    catch(err) {
          throw err;
    }
}

const project_data_last = async() => {
    try {

        let project = {
               $project : { 
                    _id : 1,
                    provider_id : 1,
                    provider_name : 1,
                    booking_id : 1,
                    booking_Time : 1,
                    status : 1,
                    address : 1,
                    category_id : 1 ,
                    category : 1,
                    isRated :{  $cond: { if: { $eq: ["$ratingId", null] },then: false,else: true } }, 
               }                  
            } 
            return project;
    }
    catch(err) {
          throw err;
    }
}

const sort = async() => {
    try {
        let sort = { 
               $sort : {
                booking_Time : -1 
               }
            }    
            return sort;
    }
    catch(err) {
          throw err;
    }
}

const project_data_2 = async(last, ongoing) => {
    try {
        let data1 = [];
        if(ongoing.length !=0){
            data1 = ongoing;
        }
        
        let data = [];
        if(last.length !=0){
             data = last;
        }

        let project = {
                        Ongoing_Service : data1,
                        Last_Services : data                   
            }
            return project;
    }
    catch(err) {
          throw err;
    }
}

const list_order_1 = async(userId) => {
    try {
           let match = await match_category(userId);
           let lookup_userProduct = await lookup_user_product();
           let lookup = await lookup_product();
           let lookup_Sub_category = await lookup_sub_category();
           let lookup_Category = await lookup_category();
           let unwind_Category = await unwind_category();
           let lookupProvider  =  await lookup_provider();
           let group = await group_sub_category_product();
           let project = await project_data();
           let sortData = await sort();
           let query = [
                        match,
                        lookup_userProduct,
                        lookup,
                        lookup_Sub_category,
                        lookup_Category,
                        unwind_Category,
                        lookupProvider,
                        group,
                        project,
                        sortData
               ];
               let data =await DAO.aggregatedata(Models.bookings,query);

               return data;
    }
    catch(err) {
          throw err;
    }
}

const list_order = async(userId) => {
    try {  
           let ongoing = await list_order_1(userId);
           let last = await list_order_2(userId);
           let project = await project_data_2(last, ongoing);
           
           return project;
    }
    catch(err) {
          throw err;
    }
}



const list_order_2 = async(userId) => {
    try {
           let match = await match_category_1(userId);
           let lookup_userProduct = await lookup_user_product();
           let lookup = await lookup_product();
           let lookup_Sub_category = await lookup_sub_category();
           let lookup_Category = await lookup_category();
           let unwind_Category = await unwind_category();
           let lookUpRating =  await lookup_provider_rating();
           let unWindRating = await unwind_rating();
           let lookupProvider  =  await lookup_provider();
           let unwindProvider  =  await unwind_provider();
           let group = await group_sub_category_product_1();
           let project = await project_data_last();
           let sortData = await sort();
           let query = [
                        match,
                        lookUpRating,
                        unWindRating,
                        lookup_userProduct,
                        lookup,
                        lookup_Sub_category,
                        lookup_Category,
                        unwind_Category,
                        lookupProvider,
                        unwindProvider,
                        group,
                        project,
                        sortData
               ];
               let data =await DAO.aggregatedata1(Models.bookings,query);

               return data;
    }
    catch(err) {
          throw err;
    }
}


module.exports = {
    list_order_1:list_order_1,
    list_order_2:list_order_2,
    list_order:list_order,
    group:group,
    unwind_provider:unwind_provider,
    project_data:project_data,
    group_sub_category_product:group_sub_category_product,
    group_sub_category_product_1:group_sub_category_product_1,
    lookup_sub_category:lookup_sub_category,
    lookup_category:lookup_category,
    lookup_user_product:lookup_user_product,
    lookup_provider:lookup_provider,
    unwind_category:unwind_category,
    lookup_product:lookup_product,
    match_category:match_category,
    match_category_1:match_category_1,
}
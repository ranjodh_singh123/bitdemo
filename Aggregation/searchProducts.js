const DAO = require("../DAOManager").queries;
const Models = require("../Models");
const TokenManager = require("../Libs/tokenManager");

const match_category = async(categoryId) => {
    try {

        let match = {

                $match : {
                    categoryId : mongoose.Types.ObjectId(categoryId),
                  //  name : {$regex : "car", $options :"i"}
                }
                    
           }     
           return match;
    }
    catch(err) {
          throw err;
    }
}


const lookup_product = async() => {
    try {

    let lookup = {
            $lookup : {
                from : "products",
                let : { id : "$_id" },
                  pipeline : [ {
                             $match : {
                                  $expr : {
                                      $and : [
                                          { $eq : [ "$subCategoryId","$$id" ] }
                                      ]
                                   }
                              }
                   },
                   { 
                   $project : {_id : 1, name : 1,price : 1, picture : 1,description : 1}
                   }
                   ],
                   as : "product"          
            }
        }    
        return lookup;
    }
    catch(err) {
        throw err;
    }
}
const unwind_product = async() => {
    try {

            let unwind_product =   { $unwind : "$product" }      
            return unwind_product;
    }
    catch(err) {
          throw err;
    }
}

const match_products = async(searchProduct) => {
    try {

        let match = {
                $match : {'product.name' : {$regex : searchProduct, $options :"i"}} 
           }     
           return match;
    }
    catch(err) {
          throw err;
    }
}

const group_sub_category_product = async() => {
    try {

        let group = {
                    $group : {
                            "_id" : "$_id",
                            "Subcategory" : { $first : "$name" },
                            "Products" : { $first : "$product" }
                    }
            } 
            return group;
    }
    catch(err) {
          throw err;
    }
}

const project_data = async() => {
    try {

        let project = {
                    $project : {
                            _id : 1,
                            Subcategory : 1,
                            Products : 1
                    }
            }
               
            return project;
    }
    catch(err) {
          throw err;
    }
}


const search_data = async(categoryId,searchProducts) => {
    try {
     
           let match = await match_category(categoryId);
           let lookup = await lookup_product();
           let unwindProduct = await unwind_product();
           let match_Products = await match_products(searchProducts);
           let group = await group_sub_category_product();
           let project = await project_data();
           let query = [
                       match,
                       lookup,
                       unwindProduct,
                       match_Products,
                       group,
                       project
               ];
               let data = await DAO.aggregatedata(Models.subCategories,query);

               return data;
    }
    catch(err) {
          throw err;
    }
}

module.exports = {
    search_data:search_data,
    group_sub_category_product:group_sub_category_product,
    lookup_product:lookup_product,
    project_data:project_data,
    match_category:match_category,
    unwind_product:unwind_product,
    match_products:match_products,
}
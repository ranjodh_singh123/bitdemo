module.exports = {
  
    userSubcategory : require('./userSubcategory'),
    orderListing : require ('./orderListing'),
    orderSublisting : require ('./orderSublisting'),
    trackLocationUser : require ('../Aggregation/trackLocationUser'),
    trackLocationProvider : require ('../Aggregation/trackLocationProvider'),
    categoryList : require('./categoryList'),
    listService : require('./listService'),
    providerOderList :require('../Aggregation/providerOderList'),
    providerOrderlisting : require ('../Aggregation/providerOrderlisting'),
    bookingListing : require('../Aggregation/bookingListing'),
    searchProducts : require('../Aggregation/searchProducts'),
    accessOrderUser : require('../Aggregation/accessOrderUser'),
    accessorderProvider : require('../Aggregation/accessOrderProvider')
 };